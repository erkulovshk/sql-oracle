

----------------------------------------------------------------

-- �������� ������ 1 ���� 
----------------------------------------------------------------



-- ������: ������� ��  ������� (dbms_output.put_line), 
-- ��� �������� �������� ������ � ������, ������� �� � ����������.

declare
  first_name_var varchar2(200) := '�����';
  second_name_var varchar2(200) := '���������';
  surname_name_var varchar2(200) := '����������';
-- ���� ��� ���������� ������ � ��� � ������

   client_birth_date date; -- � ��� ���������� ������� �� �������

begin
  
   select cl.birth_date_d
          into client_birth_date -- ���� ������� �� � ���� ����������  
          from tb_client cl 
         where cl.first_name_v = first_name_var
               and cl.second_name_v = second_name_var
               and cl.surname_v = surname_name_var; -- ����� ������� ������� ���� ������� " ; "

   dbms_output.put_line('���� ��������: ' ||  client_birth_date);

end; 

--���� ������� � out_put: ���� ��������: 01-OCT-92



--   dbms_output.put_line('������ ���!');
select cl.* from tb_client cl; 



----------------------------------------------------------------



-- �������� ������ 2 ���� 



----------------------------------------------------------------



-- �����!  � select ����� ���������� ������ if 
-- if else ����� ���������� ���� � ����� 
-- � if ����� ������� ������� ����������� ����� AND 
-- case �����, ����� �� ��������, ����� ����� ������� if 
-- GOTO <<�����>> �������� � ���� ����� ����. ����� ����������� ���� �������� 

/*������: �������� ���, ������� ����� ��� ������� � ����, 
� ����� ��� ��� � ����. ���������. ������� � ���� ��������� ���� �� 
��������� ��� ��������, ������� ��������� � ������ */


declare
  first_name_var varchar2(200) := '�����';
  second_name_var varchar2(200) := '���������';
  surname_name_var varchar2(200) := '����������'; 
  -- ���� 'sdad' ������� ����� ��� ���, �� ������� � exception 

 birth_date_var date := to_date('01.10.1992', 'dd.mm.rr'); -- �������� ������� IF

--  birth_date_var date := null;  > ���� ����� ��������, �� ��������� ������� ELSE
  
  result_var varchar2(200); -- � ��� ���������� ������� ��, ��� ��� ������ ������ select 
 
begin
  dbms_output.put_line('������ ����������');  

   if (birth_date_var is not null) then
      dbms_output.put_line('����������� ������� IF');  
      select cl.first_name_v
             into result_var 
             from tb_client cl 
             where cl.birth_date_d =  birth_date_var;
        
     
  else 
     dbms_output.put_line('����������� ������� ELSE');  
     
     select cl.first_name_v
             into result_var 
             from tb_client cl 
             where cl.first_name_v = first_name_var  
               and cl.second_name_v = second_name_var 
               and cl.surname_v = surname_name_var;
  
  end if;
   
   dbms_output.put_line('�������� ���������� result_var: ' || result_var );
   
  exception 
   
    when others then
       dbms_output.put('��������� ������');

end;

-- ��������� ������  when NO_DATA_FOUND then (ltkf)     when TOO_MANY_ROWS then  

-- dbms_output.put       put - ������ ������ ��������� �� ��� �� �����, � ��� ��������� � ���� �������
-- dbms_output.put_line  put_line - ������ ������ �������� ������, �.�. ������ ��������� � ����� ������





 
-- ��� �������� YY  ������ ������� ��� ���������� 

select to_date('01.10.75', 'dd.mm.yy') from dual; 
-- �����: 01.10.2075

select to_date('01.10.40', 'dd.mm.yy') from dual; 
-- �����: 01.10.2040  


-- ��� �������� RR  >  �� ����������� 
-- ���� =50 � ������, �� ����� ������� ��� 
-- ���� ������ 50, �� ����� ������� ��� (2000+)
 
select to_date('01.10.75', 'dd.mm.rr') from dual; 
-- �����: 01.10.1975     ������� ��� 

select to_date('01.10.50', 'dd.mm.rr') from dual; 
-- �����: 01.10.1950     ������� ��� 


select to_date('01.10.49', 'dd.mm.rr') from dual; 
-- �����: 01.10.2049     ������� ��� 

select to_date('01.10.40', 'dd.mm.rr') from dual; 
-- �����: 01.10.2040     ������� ��� 



/*
������: �������� ��������� ���� ����������/���������� �������
��. ����� 

        ��� 
        req_type varchar2(100) := 'insert/update'

*/


declare 

begin 
  if()
  
  else
    

end 

select * from tb_client_new;

alter table tb_client_new add inn_c char (14);

select * from tb_client_new;

insert into tb_client_new (id, name_v, birth_date_d, inn_c) 
       values (1, '������', to_date('10.02.1990', 'dd.mm.yyyy'), '22012199500365');

insert into tb_client_new (id, name_v, birth_date_d, inn_c) 
       values (2, '������', to_date('30.11.1995', 'dd.mm.yyyy'), '22012199500500');

update tb_client_new
       set id = 3,
           birth_date_d = to_date('20.05.1999', 'dd.mm.yyyy'),
           inn_c = '22012199500600'
           where name_v = 'Bekbolot,01111990,Naryn';

update tb_client_new
       set id = 4,
           birth_date_d = to_date('07.12.1998', 'dd.mm.yyyy'),
           inn_c = '22012199500700'
           where name_v = 'Azamat,05081998,Batken';

update tb_client_new
           set name_v = '��������'
         where name_v = 'Bekbolot,01111990,Naryn';  
         
update tb_client_new
           set name_v = '�������'
         where name_v = 'Maksat,04092001,Talas';           
----------------------------------------------------------------

-- �������� ������ 
----------------------------------------------------------------


/*������:�������� ��������� ���� �� �������� ��� � ��.
��� �������� ����� (� ���������� ��������� ������������ ��������).
��������� ���� ��� � ���� (��������� select � ��������),
 ���� ����� ���� ���� �������� - dbms_output.put_line("������ ��� ��� ���������� � ����, ���������� ����������!"), 
 ���� �� ����� - ���������� ������� ����� ������ � ��������� ���.

  */

 
declare
   
 front_inn_c  char(14) :=  '22012199500365';  
 -- ���, ������� ��������� � ������
 result_c char(14);
 -- tb_client_new � ��� ��� ���� ������� ���
 
begin
  dbms_output.put_line('������ ����������');  

   if (front_inn_c is not null) then
      dbms_output.put_line('����������� ������� IF');  
      select cl.inn_c
             into result_var
             from tb_client_new cl
             where cl.inn_c =  front_inn_c;
             
            elsif (result_var is not null) then 
              dbms_output.put_line('�� ����� ���: ' || result_var ' - ������ ��� ���� � ���� ������' );
              else
                  dbms_output.put_line('����������� ������� ������');
                  insert into  tb_client_new (inn_c) values (front_inn_c);
            end if;
         
    else 
     dbms_output.put_line('����������� ������� ELSE �� ������');  
     
    end if;
   
   dbms_output.put_line('�������� ���������� result_var: ' || result_var );
   
  exception 
   when others then
       dbms_output.put('��������� ������');
end;

/*
������� �� �������:
������� �� �������:
���� �� ����� ������))) ������ ���������� -> ��������� ������ �� ���������� � ��� 
-> ���� ��� �� ������ �� ��������� ������ �� ���� � ���������� �� ���������� �������, 
��� ������ ��� ���� �� ����� ���... �� ���� ��������� ������� 
������ ������ � ���� ��� �� ����������.... 

�� ���� �� �� ������ ������� ������ �� ��� ��������� �� ����,
 ����� ������������� � �������, �������� �� �� �������� ��� ���
  � �� ���������� ������� ��������� ������ ��� ������� ��������� �� �����...

*/
