
----------------------------------------------------------------

-- �������� ������ 19  ����. �������� � plpgsql

-- ��� ��� ���� �� sys ����� � ���� ������ ��� system
grant execute on dbms_crypto to system;

----------------------------------------------------------------

-- ������ ��� �������������: ����������, ����� ����� ����� �� ������ �����.


drop table temp_table;
drop sequence temp_seq;
create table temp_table
                  (
                    t_id number unique,
                    val1 varchar2(20)
                  );
create sequence temp_seq increment by 1 start with 1;

create or replace trigger temp_trig
before insert
on temp_table
   for each row
       begin
       :new.t_id := temp_seq.nextval;
   end;

declare
   ret varchar2(20);
begin

insert into temp_table (val1) values (1) returning t_id into ret;

       dbms_output.put_line('1: ' || ret); --1

update temp_table set val1 = val1 + 5 where t_id = 1 returning val1 into ret;

       dbms_output.put_line('2: ' || ret);--6

commit;

begin
       execute immediate ('truncate table anytable');
exception
       when others then
            null;
end;

update temp_table set val1 = val1 + 5 where t_id = 1 returning val1 into ret;

       dbms_output.put_line('3: ' || ret); --11

insert into temp_table (val1) values (55) returning t_id into ret;

       dbms_output.put_line('4: ' || ret); --2

rollback;

update temp_table set val1 = val1 + 5 where t_id = 1 returning val1 into ret;

       dbms_output.put_line('5: ' || ret); --11

       for srow in (select * from temp_table)
       loop

       dbms_output.put_line('6: ' || srow.t_id || ' ' || srow.val1); --1 11

       end loop;

select temp_seq.currval into ret from dual;

       dbms_output.put_line('7: ' || ret);--2

commit;

end;














----------------------------------------------------------------



















----------------------------------------------------------------

-- �������� ������ 19  ����. �������� � plpgsql

-- ��� ��� ���� �� sys ����� � ���� ������ ��� system
grant execute on dbms_crypto to system;

-------------------------------------------------------------------------------------
 
