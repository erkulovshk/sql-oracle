
----------------------------------------------------------------

-- �������� ������ 18  ����. �������� � DG. �������� �������, �������. 

-- ��� ��� ���� �� sys ����� � ���� ������ ��� system
grant execute on dbms_crypto to system;

-------------------------------------------------------------------------------------

-- �������� ���� � ������  sh_erkulov_pck

create or replace package body sh_erkulov_pck as


    -- ������� ������� ������. ��� ��������� input_str � ���������� ���������� res. ��� ���� varchar2

    function str_reverse(input_str varchar2)
        return varchar2
        is
        res     varchar2(500);

        -- ����� ������� ����� ������
            str_len number;
    begin
        str_len := length(input_str);
        -- � ����� ����� � ����� ���������� �� ������� � ���������� � res. � ����� ������
        -- ����� ��� ����� ������������ ����� ����, �� � ������� 1 ������ ���.
        loop
            res := res || substr(input_str, str_len, 1);
            str_len := str_len - 1;
            exit when str_len = 0;
        end loop;
        -- ������ � ����� res, � ������� ������������ �����.
        return res;
    end;

 -- ������� ��������� ID  ������� �� ��� ���.
--  ������� 1

        -- ��������� �� ���� ���������� v_inn ���� varchar2
    function get_cl_id_by_inn(v_inn varchar2)
        -- ���������� ���-�� ���� number
        return number
        is
        -- ������ ���������� id_cl
        id_cl number;
    begin
        select id
        into id_cl
        from TB_CLIENT_PL
        where INN_V = v_inn;

-- ��������� ����������
return id_cl;
-- ��������� ������
    exception
        when no_data_found then
            DBMS_OUTPUT.PUT_LINE('������ �� ������� ���: ' || v_inn || ' �� ������');
            return -1;
        when too_many_rows then
            DBMS_OUTPUT.PUT_LINE('������� ������ 1�� ������� �� ���: ' || v_inn);
            return -1;
        when others then
            DBMS_OUTPUT.PUT_LINE('������������� ������');
            return -1;
    end;



--  �������-����, ����� ��������� ��������� ID  ������� �� ��� ��� � �����
--  ������� ���������� id (���� ��� ��) ��� -1 (������ �� �������)

--   client_name ����� � ���� ��������� ������, ������� out
--  type_t ����� �� ����� ��� ��������, ������ ��� ����� default null. �� ��� ��� ���� ������ � ����� ���������
    function get_cl_id_by_inn (v_inn varchar2, client_name out varchar2, type_t varchar2 default null)
        return number
        is
       id_cl number;
    begin
-- ���� type_t �� ����� TEST, ����� �������� select ��� ������ � ��������� ����� � ���������� id_cl
        if (type_t != 'TEST')
        select id, NAME_V
            into id_cl
            from TB_CLIENT_PL
            where INN_V = v_inn;
        else -- ����� ���������� id = 1
            id_cl := 1;
            client_name := 'Testovoe imya';
        end if;
-- ����� ������� ���� ������� �� �������� �������, �� �� ����� ������� �� � ��������� �����������, � ���������,
-- ��� ��� �������� ���������.

    exception
        when no_data_found then
            DBMS_OUTPUT.PUT_LINE('������ �� ������� ���: ' || v_inn || ' �� ������');
            return -1;
    end;

    end;

end sh_erkulov_pck;
 





----------------------------------------------------------------































----------------------------------------------------------------

-- �������� ������ 18  ����. �������� � DG. �������� �������, �������. 

-- ��� ��� ���� �� sys ����� � ���� ������ ��� system
grant execute on dbms_crypto to system;

-------------------------------------------------------------------------------------
 
