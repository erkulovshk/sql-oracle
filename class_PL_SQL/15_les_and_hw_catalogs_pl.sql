 
----------------------------------------------------------------



-- �������� ������ 15  ����.  ��������



------------------------------------------------------------------------------------- 
-- ������������� ������������ Oracle Scheduler ����� � ������� ������� 


/* ������

� ��� ���� ����� ����� ����� �� �����, � ��� ���� ������������ ������ ������ � ������������ �� ����� �������. 

- ����� ����� ODBS �������� > ������� ������� ��� ������ Excel, ��� ����� ��� ������� ��������� 
- 

*/


-- all_directories - ��� �� �������, � ��������� �������������, �. �����-�� ���� ������� � �������� � Oracle 

--��� ���������� � �������
select * from all_directories ;  


--��� ���� ����� ������, �� ��� � �������, ����� ������������ ������� ������ ��� system
select * from system.tb_client; 


-- �������� ����� ���������� , ������� ��������� � ������ � ��������
create or replace directory  development_dir  
as 'C:\Users\sherkulov.KAZNA\Documents\GitHub\sql-oracle\class_PL_SQL\orcl_logs'; 

-- �������������� ����� ���������� 
create or replace directory  development_dir  
as 'D:\oracle_logs'; 

-- �������� ����������
DROP  directory "development_dir";


------------------------------------------------------------------------------------- 

-- �������� ����� ������� 
declare					
  file_id utl_file.file_type;
begin
          
  file_id := utl_file.fopen('DEVELOPMENT_DIR', '123', 'R');
  --utl_file.get_line('DEVELOPMENT_DIR', '123', 'R');
  
  utl_file.fclose(file_id);
end;

SELECT * FROM dba_directories;

------------------------------------------------------------------------------------- 

/*������: ������� ���� � ������ ���������� � ������� ���������, ��� ���� ������.
����� ���������, ��� ���� ������, � ���� ������, �� ������� ���.  
*/

declare
   -- ������� ����������, �. ��������� �� ���� ��� ������������ �����. 
   file_id utl_file.file_type;
   
begin
  
-- ������ ���������� development_dir, � �. ���� = D:\oracle_logs\, 
-- ������ ������� ���� � ��������� '123.txt'.  � 'R' ������, ��� ���� ����������� ������ ��� ������.
 
   file_id := utl_file.fopen('DEVELOPMENT_DIR', '123.txt', 'R');

-- ���� ���� ������, ��  ������� ���������, ��� ������  
   if utl_file.is_open(file_id) then
       dbms_output.put_line('OPEN!');
     else 
        dbms_output.put_line('CLOSE!');
    end if;

-- ��������� 60 ������ � ������� ������    
    DBMS_LOCK.sleep(1);

-- ���� ���� ������, �� ������� ������, ���� ������ - �� � ������ �� ����.     
     if utl_file.is_open(file_id) then
       dbms_output.put_line('OPEN!');
     else 
        dbms_output.put_line('CLOSE!');
    end if; 

-- ��������� 60 ������ � ������� ������      
    DBMS_LOCK.sleep(1);

-- ���� ���� ������, ����� ������� ���     
    if utl_file.is_open(file_id) then
        utl_file.fclose(file_id);
    end if; 
end;



------------------------------------------------------------------------------------- 


/*������: 

  ��������� ������. ����������� ��������� ���������� fclose_all   */

declare
  file_id utl_file.file_type;
begin
  file_id := utl_file.fopen('DEVELOPMENT_DIR', '123.txt', 'R');
  --utl_file.put_line(file_id, 'CHK put to file');
  --utl_file.fclose(file_id);
  
  --dbms_output.put_line(utl_file.get_line);
  
  if utl_file.is_open(file_id) then
    dbms_output.put_line('OPEN!');
  else 
    dbms_output.put_line('CLOSE!');  
  end if;
  
  utl_file.fclose_all(); --!!!
  
  if utl_file.is_open(file_id) then
    dbms_output.put_line('OPEN!');
  else 
    dbms_output.put_line('CLOSE!');
  end if;

end;

------------------------------------------------------------------------------------- 

--  ������ �� �����

declare
  file_id utl_file.file_type;
  v_line varchar2(32000);
begin

-- ��������� ���� � ������ ������  
  file_id := utl_file.fopen('DEVELOPMENT_DIR', '06092021.txt', 'R');

-- get_line ����� �������, � ����� � ������� ���������� ������ ��������� ������� �� �����
-- v_line ����� �� ���� ���������� ������.  file_id ������� ������� ����. 
  utl_file.get_line(file_id, v_line);

-- ������� v_line ����� ������ ������ ������ ����   '06092021.txt'
  dbms_output.put_line(v_line);
  
-- ����� ����� ������ ������ ������ ����   '06092021.txt'
  utl_file.get_line(file_id, v_line);
  dbms_output.put_line(v_line);
  
  utl_file.get_line(file_id, v_line);
  dbms_output.put_line(v_line);

  utl_file.fclose(file_id);
  
end;


------------------------------------------------------------------------------------- 
-- ��������� ��� ������ � ������� � ������������ ������� � ��������� ����� ��� ��������
-- � �����, ����� ����� ��������� ������ ����������� �� ����, �� ������ ��������� �����������. 

declare
-- ��������� ����������, �. ����� ������������ � begin ���������� �����, ������� ���������
    l_file utl_file.file_type;
    l_line varchar2(32767);
    is_end_line boolean := false;

-- ������ ���������
    procedure get_nextline(file_in  in utl_file.file_type,
                           line_out out varchar2,
                           -- eof_out �����-�� ��������, �. ��������� (out)  �� ���� boolean ���
                           eof_out  out boolean) is
    begin
      utl_file.get_line(file_in, line_out);
      eof_out := false;
    exception
      when no_data_found then
        line_out := null;
        eof_out  := true;
    end;

begin
   l_file := utl_file.fopen('DEVELOPMENT_DIR', '06092021.txt', 'R');

-- � ����� ������� ��������� � ���� � ��� ��� ��� ����������� ���������� � declare .
-- ����  �� is_end_line, ���� ��������
-- ������ �� �����, ����� ������������ is_end_line, �. ������ false �� ��������� �  declare
   loop
     get_nextline (l_file, l_line, is_end_line )
     if (not is_end_line) then 
       dbms_output.put_line(l_line);
     end if;
     
     exit when   is_end_line;
   end loop;
   
exception
-- ���� ������ ���, �� ������� ������   no_data_found
  when no_data_found then 
     dbms_output.put_line('������ � ����� �� ����������!');
-- � �� ������� ��� ����, � ������� ������ ������  
     utl_file.fclose(l_file)
end; 


------------------------------------------------------------------------------------- 

-- ������ � ���� 
-- ��� ���� �������  � ����� ���� ����� 'CHK put to file', � ��� �� ������ ������,   ������ ���
-- ���� �� ������� � ������ "W" - ���  ������, ��� ���� ����������� ��� ������ � ������ � ������ ������.


declare
  file_id utl_file.file_type;
  v_line varchar2(32000);
begin
  file_id := utl_file.fopen('DEVELOPMENT_DIR', 'client_info', 'W');
  utl_file.put_line(file_id, 'CHK put to file');

  utl_file.fclose(file_id);
  
end;

------------------------------------------------------------------------------------- 
-- ������ ������������������ ������ � ����
-- \n ������� �� ����� ������ 


declare
  file_id utl_file.file_type;
  v_line varchar2(32000);
begin
  file_id := utl_file.fopen('DEVELOPMENT_DIR', '123.txt', 'W');
  
-- putf ������� ������������������ ������, putf   ����� ������� �������� ����� � ������.
-- � ������ ���� ���� "%s" - ��� "�����". �� ��� ����� ������������� ������� ������ ����������� ������ �� �������, 
-- ���������: __ ����� ������ ������-�������� (������ ����)
-- ���������: __ ����� ������ ������-�������� (��������)
-- ��������: __ ����� 1000000 � ���� ������. 
  utl_file.putf (
              file_id, 
              '���������: %s\n���������: %s\n��������: %s\n',
              '������ ����',
              '��������',
              to_char(1000000)
              );
-- ����� ���������� �������� ��������� ��������               
  utl_file.fclose(file_id);
  
end;


------------------------------------------------------------------------------------- 
 
/*   ������:

�������� � ���� ����� ���� �������� ��������� � ������� tb_client
� ����� ������ ��� */

-- �������� ����� ���������� , ������� ��������� � ������ � ��������
create or replace directory  development_dir  
as 'C:\Users\sherkulov.KAZNA\Documents\GitHub\sql-oracle\class_PL_SQL\orcl_logs'; 




declare
  file_id utl_file.file_type;
begin

-- ��� �������� ����� � ������ ������ W, ��� ���� ����� ��� ���������� � � �������� �����
-- client_info +  ���� + .txt  - �� ��� ����� ������ ��� �������� ����...

  file_id := utl_file.fopen('DEVELOPMENT_DIR', 'client_info'
 || to_char(sysdate, 'yyyy-mm-dd') ||'.txt', 'W');

-- ����� � ����� �������� ���� � ������� ������ ������ 
  for cl in
    (select cl.name_v, cl.inn_v from tb_client_pl cl)
  loop
    --------------------------------  

-- putf ������� ������������������ ������   
    utl_file.putf (
                file_id,
                '��� �������: %s\n���: %s %s\n-----------------\n',
                cl.name_v,
                cl.inn_v
                --, result
                );
  end loop;

-- ��������� ���� � �����
  utl_file.fclose(file_id);
 
end;



------------------------------------------------------------------------------------- 

-- ����������� ������
--  ������ �������� - ��� ����� ����������, ���� ���� ����������� 
-- ��������� �������� - ��� ����� �������� �����, �. ������� � ���� � ����� ���������� 

declare
begin
  utl_file.fcopy('DEVELOPMENT_DIR', '06092021.txt', 'DEVELOPMENT_DIR', '06092021_arh.txt');
end;



------------------------------------------------------------------------------------- 

-- �������� ������

declare
begin
	utl_file.fremove ('DEVELOPMENT_DIR', '06092021_arh.txt');
end;





------------------------------------------------------------------------------------- 
-- �������������� � ����������� ������
begin
  UTL_FILE.frename('DEVELOPMENT_DIR', '06092021.txt', 'DEVELOPMENT_DIR', '06092021_arh.txt');
end;





 
------------------------------------------------------------------------------------- 


/*   ������ 
������� ��������� ���������� �������� � ����
������������ ��������� ����������� ������� � ����, �������� �� ������� ������� � ������� ����
� � ������� �� ������� �������

*/










------------------------------------------------------------------------------------- 

create or replace directory development_dir as 'D:\oracle_logs\'; -- �������� �����
 
grant read on directory development_dir to system; -- ��������� ��������








 

 

------------------------------------------------------------------------



-- �������� ������ 15  ����. ��������





--------------------------------------------------------------------------------------------------------------
