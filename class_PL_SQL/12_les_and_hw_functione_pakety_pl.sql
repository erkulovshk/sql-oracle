
----------------------------------------------------------------



-- �������� ������ 12  ����.  �������. ������




----------------------------------------------------------------


-- ��������� �������

function [�����.]���[( ��������[, ��������...] ) ]
  return ������������_���
  [�������������� ���������]
is
  [����������]
begin
  ����������� �������
[exception
  ����������� ����������]
end [���];



----------------------------------------------------------------
-- ������� ������� �������, ������� ���������� ��� number
 

create or replace function get_number (in_var varchar2)
return number
is
begin
     if (in_var = '����') then
       return 5;
     else
       return 404;
     
     end if;
     
end;


-----------------------------------------------------------------------
-- ����� ������� � select 

select get_number ('asdasdasd')
        from dual; -- �����:  404 


select get_number ('����')
        from dual; -- �����:  5 
 
-----------------------------------------------------------------------
-- ���
-- ����� ������� � ��������� ����� 

declare
   
   res number;
   
begin
   
   res := get_number('����');
    dbms_output.put_line(' ���������:  ' || res );

end; 

-- �����:  ���������:  5


  
-----------------------------------------------------------------------

-- ������� �������  � ���������� ������ � ������� ������ ������� ������ ������� �� ����


create or replace function get_number (
       in_var in out varchar2, -- in out ����� � ������� ���������� � ���������
       error_msg out varchar2    -- out ����� ��������� � ���� �����-�� ���������� � �������
       )
       
return number

is

begin
     
     in_var := 1 /0;
     
     if (in_var = '����') then
       return 5;
     else
       return 404;
     
     end if;
     
     
     
exception when others 
  then
  
  error_msg := '�������������� ������!';  
  return 0;      
     
end;


---------------------------------------------------------------------------------

--  ������� ������� � ���������� ������

declare 
  
  res number;
  res_error_msg varchar2 (200);
  var_varchar varchar2(200);


begin
  res := get_number (var_varchar, res_error_msg );
  dbms_output.put_line(' ���������:  ' || res || ' ,  ����� ������: ' || res_error_msg );

    

end;

-- �����:     ���������:  0 ,  ����� ������: �������������� ������!



-------------------------------------------------------------------------------------
 

-- ���������� ������� � ������� �� ������ ��������


create or replace function get_number (
       in_var in out varchar2, -- in out ����� � ������� ���������� � ���������
       error_msg out varchar2, -- out ����� ��������� � ���� �����-�� ���������� � �������
       var_i number            -- ���� ���� 0 ������, �� �������� � ������ ������, ���� �� �����,
                               -- �� ������ ���� �� ����� begin 
       )
       
return number

is

begin
     
     in_var := 1 /var_i;
     
     if (in_var = '����') then
       return 5;
     else
       return 404;
     
     end if;
     
     
     
exception when others 
  then
  
  error_msg := '�������������� ������!';  
  return 0;      
     
end;


---------------------------------------------------------------------------------

-- ������� ������� � ����� �����������

declare 
  
  res number;
  res_error_msg varchar2 (200);
  var_varchar varchar2(200) ;


begin
  
  
  
  res := get_number (var_varchar, res_error_msg, 5);
  -- ���� ��� � () 3-� �������� (0), �� �����:    ���������:  0. ����� ������: �������������� ������!
  -- ���� ��� � () 3-� �������� (!=0), �� �����:  ���������:  404 
     dbms_output.put_line(' ���������:  ' || res ||'.'); 
  
  
  if (res = 0) then
      dbms_output.put_line('����� ������: ' || res_error_msg );
  end if;
 
end;

 
-------------------------------------------------------------------------------------

--         � � � � � �         ---


/* ������: 

������� ��������� (add_contract), ������� �������� ������� ����������� �������� (���-�����).
������� ��������� (add_client ��� registration_client), ������� ����� ��������� �������
 �� ������ ������ ���.

������� ������� (inn_is_exsist), ������� ��������� ��� � �������, ������ ���� ��� ����. 
���� ������ ����, �� ���������� ������� � ���������� � ������� ����� �������� (���-�����)
����� ��������� add_contract. 

� ������, ����� ������� ����, ������� ������� � ������� ��������� add_client.

*/
 


-------------------------------------------------------------------------------------
/* ������� �������, ������� ����� ��������� ������ � ���� � ��������, ������ ���� ��� ����.
 � ��� ���� old_client_id out number, � ������� �� ����� id �������, ���� �� ����� ����������.

 */

create or replace  function is_exist_client (
       cl_inn varchar2,
       old_client_id out number

)

return boolean
is
    our_exception exception; -- ��������� ������_1
begin

     select id
         into old_client_id
         from tb_client cl
       where cl.inn_c = cl_inn;

       return true;


exception
  when NO_DATA_FOUND then
    return false;

  when others then
    raise our_exception; -- �������� ������_1


end;

-------------------------------------------------------------------------------------

-- ������� ���������, ������� ����� ���������/�������������� �������

create or replace procedure registration_client (
       cl_name varchar2,
       cl_inn varchar2
)  is
   

   old_client_id number;


begin

-- � if ����   ������� is_exist_client ������ true, �� ������� ����� ������  
  if (is_exist_client (cl_inn, old_client_id)) then 
    dbms_output.put_line('������ ����������. ID :  ' || old_client_id  );
  else 
     dbms_output.put_line('������ �� ���������� - ����� ���������.');
  end if;  
  
exception 
  when others then
    dbms_output.put_line ('������!');


end;




-------------------------------------------------------------------------------------

/* �������� ��������� registration_client � ��������� �����. 

*/


declare

  new_cl_name varchar2 (200) := '������ ���� ��������';
  new_cl_name varchar2 (16) := '23323232323230';


begin
  
  registration_client (new_cl_name, new_cl_inn);

end; 


























----------------------------------------------------------------



-- �������� ������  12  ����.  �������. ������



----------------------------------------------------------------

    -- �������� ������� is_exist_client

function is_exist_client(
        cl_inn varchar2,
        old_client_id out number
    )
        return boolean is
        our_exception exception;
    begin

        select id
        into old_client_id
        from tb_client_pl cl
        where cl.INN_V = cl_inn;

        return true;

    exception
        when NO_DATA_FOUND then
            return false;
        when too_many_rows then
            DBMS_OUTPUT.PUT_LINE('���������� �������� ������ 1�� � ��� ' || cl_inn);
            raise our_exception;
        when others then
            raise our_exception; -- �������� ������_1
    end;





-- �������� ������ SH_ERKULOV_PCK

create package SH_ERKULOV_PCK as
    function is_exist_client( cl_inn varchar2, old_client_id out number) return boolean;

    end SH_ERKULOV_PCK;

--- ����� ������ is_exist_client � ��������� �����. �����!!! ������� ���������� boolean ���



declare
    is_exist boolean;
    old_id number;
begin
  is_exist := SH_ERKULOV_PCK.is_exist_client('222052992001114', old_id);
  if (is_exist) then
      DBMS_OUTPUT.PUT_LINE('ID ������������� �������: '|| old_id );
  else
      DBMS_OUTPUT.PUT_LINE('��� ����� ������.');

  end if;
end;





 



