
----------------------------------------------------------------

-- �������� ������ 21 ����   JSON

-- ��� ��� ���� �� sys ����� � ���� ������ ��� system
grant execute on dbms_crypto to system;

----------------------------------------------------------------


-- ���������� � �������
create table TB_BALANCE
(
    ID        NUMBER
    constraint PK_TB_BALANCE
    references TB_CUSTOMER,
    BALANCE_N NUMBER(20, 2)
);


-- ���������� � ������� 
create table TB_RING_SESSION
(
    ID              NUMBER not null
    constraint PK_TB_RING_SESSION
            primary key,
    TIME_BEGIN_D    DATE,
    ID_CL           NUMBER
        constraint FK_TB_RING_SESSION
            references TB_CUSTOMER,
    PHONE_NUMBER_1  VARCHAR2(30),
    PHONE_NUMBER_2  VARCHAR2(30),
    TIME_DURATION_D NUMBER,
    COST_N          NUMBER,
    TYPE_OF_RING    VARCHAR2(30)
);


-- �������  

create table TB_CUSTOMER
(
    ID                NUMBER not null
        constraint PK_TB_CUSTOMER
            primary key,
    NAME_V            VARCHAR2(200),
    TYPE_OF_SERVING_C CHAR,
    COMPANY_NAME_V    VARCHAR2(200),
    MSISDN_V          VARCHAR2(20),
    PROFILE_STATUS_C  CHAR
);




------------------------------------------------------------------
 --  BALANCE_CHECK
 
 /*
     ������� �������� ������� �� ������ ��������
     -  �������� ���� ���������� ������� ������ ��� 1 ��
     -  �������� ������������� ������ ��������
     -  ����������:
            �����! ������� ������ �������: ���.��
            ������! * ����� ������ *
     */

  
create function balance_check(v_phone_number varchar2)
    return varchar2
    is
    balance_num number;
begin
    select BALANCE_N
    into balance_num
    from TB_CONTRACT_PL
    where PHONE_NUMBER_N = v_phone_number;
    dbms_output.PUT_LINE('������� ������ �������:' || balance_num);
    return ('�����');

exception
    when no_data_found then
        return ('������!������������ � ����� ������� �� ������!');
    when too_many_rows then
        return ('������!������������ ���������� �������� ������ ������!');
end balance_check;



------------------------------------------------------------------------------------------
--  BALANCE_PLUS

/*
     ������� ���������� ������� �� ������ ��������
     -  �������� ������������� ������ ��������
     -  �������� ���� ���������� ������� ������ ��� 1 ��
     -  ����������:
            �����! ������� ������ �������: ���.��
            ������! * ����� ������ *
     */


 
create function balance_plus(v_phone_number varchar2, amount number)
return varchar2
is
    balance_num number;
begin
    if (balance_check(v_phone_number)='�����') then
    select BALANCE_N
      into balance_num
      from TB_CONTRACT_PL
      where PHONE_NUMBER_N = v_phone_number;
    balance_num = balance_num+amount;
    update TB_CONTRACT_PL
      set BALANCE_N = balance_num
      where PHONE_NUMBER_N = v_phone_number;
    dbms_output.PUT_LINE('������� ������ �������'||v_phone_number||' = '|| balance_num);
    return ('�����');
    end if;
end balance_plus;


------------------------------------------------------------------------------------------
--  BALANCE_MINUS

function balance_minus(v_phone_number varchar, amount number) return varchar2;

    /*
     ������� �������� ������� �� ������ ��������
     -  �������� ���� ���������� ������� ������ ��� 1 ��
     -  �������� ������������� ������ ��������
     -  ����������:
            �����! ������� ������ �������: ���.��
            ������! * ����� ������ *
     */

    function balance_minus(v_phone_number varchar, amount number) return varchar2
        IS
        phone_num  varchar2(30);
        balance_cl NUMBER;
    BEGIN


        SELECT PHONE_NUMBER_N, BALANCE_N
        INTO phone_num,balance_cl
        from tb_contract_pl
        where PHONE_NUMBER_N = v_phone_number;

        if (balance_cl < amount) then
            return '������������ ������� �� �������';
        end if;

        UPDATE TB_CONTRACT_PL SET BALANCE_N=BALANCE_N - amount where PHONE_NUMBER_N = v_phone_number;
        COMMIT;

        select BALANCE_N
        into BALANCE_cl
        from TB_CONTRACT_PL
        where PHONE_NUMBER_N = v_phone_number;

        return ('�����! ������� ������ �������: ' || balance_cl);

    EXCEPTION
        WHEN NO_DATA_FOUND THEN return ('������! ������ ����� �� ������');
        WHEN too_many_rows THEN return ('������! ������� ������ ������ ������');
        WHEN OTHERS THEN return ('��������� ������');
    END;










----------------------------------------------------------------

 














----------------------------------------------------------------

-- �������� ������  21 ����   JSON

-- ��� ��� ���� �� sys ����� � ���� ������ ��� system
grant execute on dbms_crypto to system;

-------------------------------------------------------------------------------------
 
