

----------------------------------------------------------------


-- �������� ������ 9 ����.  ��������� 

 
----------------------------------------------------------------

-- �������� ��������� �������� 
select * from nls_session_parameters;


/* 
 
*/ 



--   ������������� ������

declare

  type list_of_cars is table of varchar2(200) index by pls_integer;
  -- type  ������ �� ������� ��� 
  -- is table ������ �� ������ ������ � ���� �������
  -- of varchar2(200) ������ ������ �������
  -- index by pls_integer ������ ������� ����� ��������, �.�. ������� �������� �����
           -- �������� �������� ���������� �����
  
  -- ����� �������, ��� � ��� ����� ���������� cars ���� list_of_cars
  cars list_of_cars; 
  
  -- ����� ���������� ���� integer 
  l_row pls_integer;

begin
  
  -- ����� ������ ������� ������� ��������� ���������
  cars(1) := 'BMW';
  cars(2) := 'Mercedes';
  cars(3) := 'AUDI';
  cars(4) := 'Lada';

  -- ���������� l_row ����������� ����� ������� ��������, �. ����� � ������� cars
  l_row := cars.first;

  while (l_row is not null) 
    loop
        dbms_output.put_line(l_row);
        dbms_output.put_line(cars(l_row));
        
        l_row := cars.next(l_row);
        
        dbms_output.put_line('---------------------------');

    end loop;

end;

/* �����:
1
BMW
---------------------------
2
Mercedes
---------------------------
3
AUDI
---------------------------
4
Lada
---------------------------


 */




-----------------------------------------------------------------------------------------

---  ��������� ������� 

declare

    type list_of_names_t is table of varchar2(100); --���������� �������������
    
    happyfamily list_of_names_t := list_of_names_t(); --�������������
    children list_of_names_t := list_of_names_t();
    parents list_of_names_t := list_of_names_t();

begin
  
-- extend(4) ��������� ����� �� ��������� ������� ��� 4 ��������� 
    happyfamily.extend(4);
    
-- �������� ��� ������ ��������    
    happyfamily(1) := '�����';
    happyfamily(2) := '������';
    happyfamily(3) := '������';
    happyfamily(4) := '����';

-- ����� extend ������ �������� �������
    children.extend;
    children(1) := '������';

    children.extend;
    children(2) := '����';

-- ����� ��� parents �� ���������� �������� �� ������ 
-- ���������� ���� �������, �� ���, ����� ��������� �������� ������ children 
--    �� ������ happyfamily  
    parents := happyfamily multiset except children;

-- ����� ������� �� ������ ������ ������ � ������ �� ����� �� ��������� parents
    for l_row in parents.first .. parents.last 
      loop
    dbms_output.put_line('��������: ' || parents(l_row));

      end loop;

end; 

/*   �����: 
��������: �����
��������: ������        */


-----------------------------------------------------------------------------------------

--  ������ ���� VARRAY

-- �� ����� ���� ������� ���� ��� ������ ������ ���� �� 
-- is varray(2) ������ ������ ������ ����� ����� ����
-- � ��� �����  ��������� ������ � �������� varchar2(100)

create type first_names_t is varray(2) of varchar2(100);  
create type child_names_t is varray(1) of varchar2(100);

-- ������� �������, ��� ������ �������� ���������� �������
create table family (
       surname varchar2(1000)
       , parent_names first_names_t
       , children_names child_names_t
       );
       
-- ���� ������ ��� ��������:  A query with objects requires OCI8 mode,
-- but OCI7 mode is used. --  Check the preference "Force OCI7 mode on OCI8"          
select  * from family  ;


declare
-- ������� ��� ���������� ���� first_names_t � child_names_t � 
--     ������� �� �������� ������� �� ��������� first_names_t()
  parents first_names_t := first_names_t();
  children child_names_t := child_names_t();

begin
-- �������� ������ �� ��� �������� ��� parents � 1 �������� ��� children 
    parents.extend(2);
    parents(1) := '����';
    parents(2) := '������';
    
    children.extend;
    children(1) := '�������';

-- ������ ����� ������� ������� � ���� �������� family, ��� � ������� parent_names 
--    ����� ������ � 2-�� ������� (parents), � � children_names 
--       ����� ������ � 1 ������ (children)
    insert into family
           (surname,parent_names,children_names)
                values ('��������',parents,children);

end;

-- � ����� � ������� �������� ������ 



-----------------------------------------------------------------------------------------


-- ��������� ��� ���������� ������

/*  ������� ��� color_tab_t, � ������� ����� ��������� � ���� �������.
 ����� � declare ������ ������ � ������ �� �������� ���������� ��� ���������. 
    */

create or replace type color_tab_t is table of varchar2(100);

declare

       type toy_rec_t is record(
            manufacturer integer,
            shipping_weight_kg number,
            domestic_colors color_tab_t,
            international_colors color_tab_t);

begin

null;

end;

-----------------------------------------------------------------------------------------

/* 
            ��������� � �������� ����������  ���������
*/
 

create type yes_no_t is table of char(1);


 


  create or replace procedure act_on_flags (flags_in in yes_no_t)
  is
  begin
         null
  end act_on_flags;


-----------------------------------------------------------------------------------------

/* 
         ��������� ��� ��������� ������� ���� ������

����� �� ������� ����� ��� dependent_birthdate_t, �. �������� ����������, 
� ����� ���� ��� ������� � ������ �� �������� (dependents_ages) ������� employees.

*/

create type dependent_birthdate_t as varray(10) of date;

  create table employees (
         id number,
         name varchar2(50),
         --������ �������...,
         dependents_ages dependent_birthdate_t
         );
  


-- ��������� ������ � �������-������ ������� employees ������ � ����������� ���� ������
insert into employees values (42, 'Zaphod Beeblebrox', ...����� >,
> dependent_birthdate_t( '12-JAN-1765', '4-JUL-1977', '22-MAR-2021'));


-----------------------------------------------------------------------------------------


declare

-- ��������� ���, ������� ����� ���������� � �����
--   ����� ��� ������� ������� tb_client (���� ��������). ����� 
--   index by pls_integer ��������, ��� �� ����� �� �������� ���������� � ��������� ���������
  type client_list_type is table of tb_client%rowtype
                                  index by pls_integer;
-- ��������� ����������, � �. ����� ��� ��������� ���   client_list_type
  client_list client_list_type;

-- ����� ����� ��������� ����������, � �. ����� ��� ������� � ���� ������� tb_client
  client_row tb_client%rowtype;

-- ��������� ���������� ���� integer  
  l_row pls_integer;



begin

-- ��������� ������� � ���������� client_row
    client_row.id := 3000;
    client_row.first_name_v := '����� �������� ������������)))';

-- � ������  client_list ����� ����������  client_row, ��� ������ ����� 1 ������  
    client_list(1) := client_row;

-- �������� ���������� client_row � ����� ��������� � ��� 2-�� �������,
--          �� ������ ��� ����� ������� �� 1 ������, ��� ��� �������� �� �����
    client_row := null;
    
    client_row.id := 3001;
    client_row.first_name_v := '���� ��������';

-- � ������  client_list ������ ��������(2) ����� ����������  client_row,
--       ��� ������ ����� 1 ������  
    client_list(2) := client_row;

-- ���������� l_row ����������� ������ ������� �������� ������� client_list, �. ����� 1
    l_row := client_list.first;
    
    while (l_row is not null)
       loop
       dbms_output.put_line('id = ' || client_list(l_row).id);
       dbms_output.put_line('��� = ' || client_list(l_row).first_name_v);
       dbms_output.put_line('��� = ' || client_list(l_row).inn_c);
       
       client_list.delete(l_row); --(!) ������� ������ ������� ������� � ������� client_list
       
       l_row := client_list.next(l_row);
       dbms_output.put_line('-------------111--------------');
       end loop;
    
-- 1  ����������� ������ ������� ������ �������           
    select *
      into client_list(1)
      from tb_client cl 
      where rownum <2;   
       
    client_list.delete(1); -- ������� ������ �������   

--2  ������� ������� ������  (for cl in), ������ ���� � ������ client_list ���������� ���� �������� � 
     -- � ������� tb_client, � � ������� � �������� ������� ���������� cl.id, ������� ���������.
     -- ��� ��� ��, ��� ��� ����� 
    for cl in (select * from tb_client  where rownum > 15) loop
        client_list(cl.id) := cl;
    end loop;


-- ����� ���������� l_row ����������� ������ ������� �������� ������� client_list, �. ����� 1    
    l_row := client_list.first;
    
        while (l_row is not null)
       loop
       dbms_output.put_line('id = ' || client_list(l_row).id);
       dbms_output.put_line('��� = ' || client_list(l_row).first_name_v);
       dbms_output.put_line('��� = ' || client_list(l_row).inn_c);
       
       client_list.delete(l_row); --(!) ������� ������ ������� ������� � ������� client_list
       
       l_row := client_list.next(l_row);
       dbms_output.put_line('-------------111--------------');
       end loop;
        
-- 3  bulk collect ���� ����������� ��� ������ ��� ������� �������� � �������� � ���������� ������
           -- ����� ���������� � ����� ������ 10 ������� ( where rownum > 11)
     select * 
        bulk collect into client_list
        from tb_client
        where rownum > 11;
      
     l_row := client_list.first;
         while (l_row is not null)
       loop
       dbms_output.put_line('id = ' || client_list(l_row).id);
       dbms_output.put_line('��� = ' || client_list(l_row).first_name_v);
       dbms_output.put_line('��� = ' || client_list(l_row).inn_c);
       
       client_list.delete(l_row); --(!) ������� ������ ������� ������� � ������� client_list
       
       l_row := client_list.next(l_row);
       dbms_output.put_line('-------------111--------------');
       end loop;        

end;


/*   �����:
    id = 3000
    ��� = ����� �������� ������������)))
    ��� = 
    -------------111--------------
    id = 3001
    ��� = ���� ��������
    ��� = 
    -------------111--------------
    
    � ���� � �������� ��������� � ������ � ������ ������ ������, �� � ����� ������� �������
                 */



---------------------------------------------------------------------------------------------

/*    ��� ��������  */

declare
   
-- ����� ������� ��� � ���� �������, � �. ����� ��� ������� ������� �������� � �������-�� ������                 
    type client_list_type_i is table of tb_client%rowtype
                                  index by pls_integer;

-- ����� ������� ��� � ���� ������, � ������ ������ ����� ���������� cl_type_var
--   � ����� client_list_type_i, �. �� �� ����� �������
    type cl_i_rec_type is record of (
         cl_type_var client_list_type_i
         );
    
    

begin
  

end;



---------------------------------------------------------------------------------------------

/*    ������: 

1. �������� �������� � id 1, 5, 10
2. ��� ������� ������� �������� ������
3. ��������� � ����� ������ ������� �� ��
4. ���� ������ �� �����������: ������� ��������� -> ��������� ������� �� �������

*/ 

declare
-- ������� ������ �� ������� 

   client1 tb_client%rowtype;
   client2 tb_client%rowtype;
   client3 tb_client%rowtype;
begin

-- ����� ������� ������ (for cl in () ) ����� ���� ������ �� ���������� ��������.   
  for cl in (select *
               from tb_client
              where id in (1, 3, 5))
-- ������ ����� ��� ������� ������� (��� ������ ���������� client1.. 2..3)
--  ���� �� id ������, �� ����� ������ ������ �� �������� ������� (cl)
-- �����! � ���������� ������� � if �������  client3, ����� ��� ��������� �� ������� 
--  ������� � id 1 (�����), ����� �������� ��������: 
--  * � ������ ������� ������� ���������� id ������� � where � ����� � ���� ������ ������ ����� �������
--  * ����� � if, ������� ����������� ������ ������ ���� �������, ������� ����� ������ ������ �������.
-- * ������� � �����, � if  client3 ����������� ������ ������� � id 1 (����� )          
  loop
      if (client3.id is null) then
            client3 := cl;
      elsif (client2.id is null) then
            client2 := cl;
      elsif (client1.id is null) then
            client1 := cl;
      end if;
  end loop;
 
  dbms_output.put_line('������1 = ' || client1.first_name_v);
  dbms_output.put_line('������2 = ' || client2.first_name_v);
  dbms_output.put_line('������3 = ' || client3.first_name_v);
 
end;

select * from tb_client; 

/*  �����: 
     ������1 = �����    (���� � ������� - ��� ������ � id 1 >> �� ��� ����� (����� ����))
     ������2 = �������
     ������3 = �����
                    */

 





-----------------------------------------------------------------------------------------



-- �������� ������  9




----------------------------------------------------------------

/* 
 
*/
  

 






