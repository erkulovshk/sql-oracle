

----------------------------------------------------------------


-- �������� ������ 7 ����   ���� .  ���� ������ ����. + ��������� Data Data grid

 
----------------------------------------------------------------

-- �������� ��������� �������� 
select * from nls_session_parameters;






/* 
��������� ������� ���� � �������

CURRENT_DATE (�����) � DATE

CURRENT_TIMESTAMP (�����) - TIMESTAMP WITH TIME ZONE

LOCALTIMESTAMP (�����) - TIMESTAMP

SYSDATE (������ ���� ������) - DATE

SYSTIMESTAMP (������ ���� ������) - TIMESTAMP WITH TIME
ZONE
*/ 



declare 

begin
  
   dbms_output.put_line('CURRENT_DATE ' || CURRENT_DATE);
   dbms_output.put_line('CURRENT_TIMESTAMP ' || CURRENT_TIMESTAMP);
   dbms_output.put_line('SYSDATE ' || SYSDATE);
   dbms_output.put_line('SYSTIMESTAMP ' || SYSTIMESTAMP);


end; 

-- �����:
-- CURRENT_DATE 15-AUG-22
-- CURRENT_TIMESTAMP 15-AUG-22 01.51.54.950000000 PM +06:00
-- SYSDATE 15-AUG-22
-- SYSTIMESTAMP 15-AUG-22 01.49.41.431000000 PM +06:00



-----------------------------------------------------------------------------------------

-- ���� ������ INTERVAL


declare

  start_date timestamp;
  end_date timestamp;
  service_interval interval year to month;
  years_of_service number;
  months_of_service number;
  date1 date := to_date('01.01.2020','dd.mm.yyyy');
  date2 date := to_date('01.03.2020','dd.mm.yyyy');
  interval1 interval day to second;
  day_interval number;

begin

  start_date := to_timestamp('29-12-1988', 'dd-mm-yyyy');
  end_date := to_timestamp('15-11-1995', 'dd-mm-yyyy');
  service_interval := (end_date - start_date) year to month;
  dbms_output.put_line('service_interval = ' || service_interval);
  years_of_service := extract(year from service_interval);
  months_of_service := extract(month from service_interval);
  dbms_output.put_line('����� ��� � �������: ' || years_of_service || ' ��� � ' ||
  months_of_service || ' �������');
  dbms_output.put_line('---------------------------------------');

interval1 := (date2 - date1) day to second;

dbms_output.put_line('interval1 = ' || interval1);

day_interval := extract(day from interval1);

dbms_output.put_line('����� ����: ' || day_interval);

end;

/* �����: 

service_interval = +06-11
����� ��� � �������: 6 ��� � 11 �������
---------------------------------------
interval1 = +60 00:00:00.000000
����� ����: 60


*/



-----------------------------------------------------------------------------------------
 

 -- �������������� ����� � ����

declare

 date_d date;

begin

  dbms_output.put_line('������� ��������������');
  date_d := to_date('01.01.20', 'dd.mm.rr');
  dbms_output.put_line('date_d = ' || date_d || ' .'); -- CHR(10)); --nls(!)

  dbms_output.put_line('������� to_date');
  date_d := to_date('01.01.2020','dd.mm.yyyy');

  dbms_output.put_line('date_d = ' || date_d || CHR(10));
  dbms_output.put_line('�� ��������');

  date_d := to_date('01.01.2020 18:30:00','dd.mm.yyyy HH24:MI:SS');
  dbms_output.put_line('date_d = ' || date_d);
  dbms_output.put_line('to_char date_d = ' || to_char(date_d, 'dd.mm.yyyy HH24:MI:SS') || CHR(10));
  dbms_output.put_line('rr');

  date_d := to_date('01.01.20','dd.mm.rr');
  dbms_output.put_line('to_char date_d = ' || to_char(date_d, 'dd.mm.yyyy HH24:MI:SS'));

  date_d := to_date('01.01.80','dd.mm.rr');
  dbms_output.put_line('to_char date_d = ' || to_char(date_d, 'dd.mm.yyyy HH24:MI:SS'));

  date_d := to_date('01.01.80','dd.mm.yy');
  dbms_output.put_line('to_char date_d = ' || to_char(date_d, 'dd.mm.yyyy HH24:MI:SS'));
  -- yy ��� ������� �������� 
  -- rr ��� ������� ��������

end;

/* �����:

������� ��������������
date_d = 01-JAN-20 .
������� to_date
date_d = 01-JAN-20

�� ��������
date_d = 01-JAN-20
to_char date_d = 01.01.2020 18:30:00

rr
to_char date_d = 01.01.2020 00:00:00
to_char date_d = 01.01.1980 00:00:00
to_char date_d = 01.01.2080 00:00:00

*/



-----------------------------------------------------------------------------------------
-- ������� CAST 


declare

  date1 date;

begin

  date1 := cast('24.02.2009' as date);
  dbms_output.put_line(date1);

end;


-- �� ��������, �������� ������� � ���� 
--   dbms_output.put_line('date_d = ' || date_d || CHR(10));  --nls(!)



-----------------------------------------------------------------------------------------
-- ������� EXTRACT 


begin

   if extract(month from sysdate) = 11 then
     dbms_output.put_line('������ ������');

   else
     dbms_output.put_line('������ �� ������');
   end if;

end;

-- �����: ������ �� ������


-----------------------------------------------------------------------------------------
-- ������� ��� ������ � �����/��������


-- ������: �������� ��������� ���� ������ last day
-- ��� ����� ����������� � ������� ��� ������� ���� �������� � ��������� ���� ������ 


begin

   dbms_output.put_line(LAST_DAY(sysdate));
     
end;

-- ������� ��������.  ��� ���� ������:  

-- round  ������ ���������� ����, ��� ��� ��� ������ ��� �� ������ �������� ���.
-- ���� mm ��������, �� ������� ������ ���� ������ 
-- ���� yy ��������, �� ������� ��������� ���, ��� ��� �������� ���� ��� ������. 

begin
   dbms_output.put_line( sysdate); -- 16-AUG-22
   dbms_output.put_line(ROUND(sysdate)); --17-AUG-22
   dbms_output.put_line(ROUND(sysdate, 'mm'));-- 01-SEP-22
   dbms_output.put_line(ROUND(sysdate, 'yy')); -- 01-JAN-23
   dbms_output.put_line(TRUNC(sysdate, 'yyyy')); -- 01-JAN-22
     
   
end;


-----------------------------------------------------------------------------------------

/*   ������ 
����������� ��������� ����
- ������ �� ���� �������� � ��������� ���������� ���������� ���� ��� (���� �������� = 
������ ���, ���� ��� � - ������ ����� ���: 2, ���� � - �� 1)
� ������ �� ������������ - ������ ��������� �� ������:
'��� 123456789 ������ ������� ����� �� ������������� ��������� �������'


---
��� ����� ���� �� �����... 

���� ��� ��, �� ���� ������ �� ���� ���������� �������, ������� ������ �������� � �� ���
��������� ������� �� ������� �� �������� ����. �����. 
� � ���� ������, ����� ��� �������� ���������, ��� ����� � ���������� ������, �� ������ ���������:
'�������� ����.����� ��������/�� ��������'. 
���� �������� �������� - �������. 

(!) ���� ���������� � ������� ���, ���������� ��������� "���������� � ������� 
����� ���."

*/

select * from tb_client; 

-- ��� ������ ���������� � LOOP  

declare 
begin 
 
  for cl in ( select * from tb_client
    ) loop
    dbms_output.put_line(cl.first_name_v || ' - ' || cl.birth_date_d ); 
   end loop;

end;
-- �����: ����� - 03-JUN-94  � �.�. � ����� ����. 



-- ����� ������� 
select * from tb_client;

select rowid, cl.* from tb_client cl;

declare 
  true_gender_id char(1);
  check_inn_mask varchar2(10);


begin 
   
   for cl in ( select * from tb_client
    ) loop
   
  -- ����� � if �� ������� ���������� ������ ������ ��� ����� ����������� ����� ��� 
   if (cl.gender_id = 2) then 
     true_gender_id := 1;
   else
     true_gender_id := 2; 
   end if;  
   
   -- ����� �� � ����� ����� �������� ������ ������ � �� ������� � ���������� ����.
   check_inn_mask := true_gender_id
                   || to_char(cl.birth_date_d, 'ddmmyyyy');
   -- ����� � if ���������� ����� ����� ���������� � ��� ������� (������ 9 ��������), 
   -- ��� �������� �� ��������� ��� �����
   if (check_inn_mask != substr(cl.INN_C, 1, 9)) then  
      dbms_output.put_line('� ������� ' || cl.first_name_v
                              || ' �������� ���!');
    end if;
    
    end loop;

end;

-- �����: � ������� ����� �������� ���!
-- ��� ���� � ������� 2 ������� �� 1 :) ������� � ���� �� ������ ��� 







-----------------------------------------------------------------------------------------



-- �������� ������  7 ���� ���� 

----------------------------------------------------------------

/* 
 � �������� �� ��������� ��������� ��������� ������� (�-� ���������� �������)


�������� ��������� ���� ������������� �������� ����� ���������� ����������
  (������ ����������) � �������, ���� �������� ��������� ������ 1 - ����� � ����,
  �� ������� ��������� � �������(�������� ���� � output). ������ ���������� ���:
 
  ������ ���� ��������
    �������� ������ �������� +996555123456
        ��������1 = 1,5 ������
        ��������2 = 1 �����
        ��������3 = 1,3 ������
        � �.�. ....
    �������� ������ �������� +996555654321
        ��������1 = 1,5 ������
        ��������2 = 1 �����
        ��������3 = 1,3 ������
        � �.�. ....
  ������� ����� ���������
      �������� ������ �������� +996555123456
        ��������1 = 1,5 ������
        ��������2 = 1 �����
        ��������3 = 1,3 ������
        � �.�. ....
    �������� ������ �������� +996555654321
        ��������1 = 1,5 ������
        ��������2 = 1 �����
        ��������3 = 1,3 ������
        � �.�. ....

*/
  
select cn.* from tb_contract_pl cn ; 

select t.* from tb_tarif_pl t ; 

select cl.* from tb_client_pl cl ; 

select op.* from tb_operation_pl op ; 

select op.* from tb_document_pl op ; 

select op.* from tb_document_type_pl op ; 

select rowid, op.* from tb_operation_pl op;




DECLARE

  oper_interval number; -- ���������� ��� �������� ���������� ������� ����� ������������

BEGIN
    
  for cl_operation in
     ( select 
             -- ������� ���������� ���������� ��� ������� �������
             row_number() over(partition by client_id order by client_id) cnt_i, 
             -- ������� ���������� ���������� �� ������� ���������
             row_number() over(partition by contract_id order by contract_id) contract_i, 
             client_id,
             name_v,
             phone_number_n,
             contract_id,
             operation_date, 
             -- ������������ ���� ��������� ��������
             lead(operation_date) over(partition by client_id order by client_id) as next_operation_date
             
          from tb_contract_pl cct
          join tb_client cl on cl.id = cct.client_id
          join tb_operation_pl op on op.contract_id = cct.id
          where type_id = 1 --��� ��������: ���������� �����
      )
  loop
    if(cl_operation.next_operation_date is not null)--���� ���� ��������� �������� 
       then
         
         --���� ��� ����� ������� �������� ������, �� ������� ��� �������
         if(cl_operation.cnt_i = 1)
           then dbms_output.put_line(cl_operation.name_v);
         end if;
         
         --���� �� ����� ������ �������� �������� ������, �� ������� �����
         if(cl_operation.contract_i = 1)
           then dbms_output.put_line('�������� ������ �������� '||cl_operation.phone_number_n);
         end if;
      
      oper_interval := round(months_between(cl_operation.next_operation_date, cl_operation.operation_date), 2);
         
         --���� ������� �� ������� ������ ������
         if(oper_interval > 1) 
            then dbms_output.put_line( '�������� ' 
                                  || cl_operation.cnt_i
                                  || ' - ���������� ������� ����� �����������: '
                                  || oper_interval);
        
        else dbms_output.put_line(' �������� '
                                  ||cl_operation.cnt_i
                                  ||' - ���������� ���� ����� �����������: '
                                  ||(cl_operation.next_operation_date - cl_operation.operation_date));
        end if;
    
    end if;
    
  end loop;

END;






















