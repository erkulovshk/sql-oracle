
----------------------------------------------------------------

-- �������� ������ 17  ����. ����������

-- ��� ��� ���� �� sys ����� � ���� ������ ��� system
grant execute on dbms_crypto to system;

-------------------------------------------------------------------------------------

-- ������ ��������� ��� �������, ������ � ���������� ������� ���������� 

-- ����������. ������� � ��������� ����� ����������� ����� l_in_val

declare
 
 l_key    varchar2(2000) := '1234567890123456';
 l_in_val varchar2(2000) := 'Super important text';
-- � l_mod ����� ��������� ����� ���������� (�������� aes128), ���������� (pad_pkcs5) � ��������� (chain_cbc) 
 l_mod    number := dbms_crypto.encrypt_aes128 + dbms_crypto.chain_cbc +
                    dbms_crypto.pad_pkcs5;
 l_enc    raw(2000);

begin
-- �� ������ dbms_crypto �������� ����� encrypt (�����������). ��� ���� � () � ������  utl_i18n ����� ����� 
-- string_to_raw (������ � ��� row) � ����� ����� � ������� �������� ��� ��� ����� l_in_val, � �������, ��� 
-- � ������ ������ ��������� ����� 'AL32UTF8' - UTF8 
 l_enc := dbms_crypto.encrypt(utl_i18n.string_to_raw(l_in_val, 'AL32UTF8'),
                              l_mod,
-- ����� ����� (,) ����������� ����� �� �������� ��� ����������    l_enc                            
                              utl_i18n.string_to_raw(l_key, 'AL32UTF8'));
 dbms_output.put_line('Encrypted=' || l_enc);
end;

-- �����: Encrypted = B959D91C33FE35EC45CE6D0FFC082904502F51D54E456663C55D2FFFF3179957
-- ��� � ���� ��� ������������� ����� 


----------------------------------------------------

-- ���������� ������ 

declare
--  ���� l_key ��� ��  1234567890123456. 
 l_key    varchar2(2000) := '1234567890123456';
 
-- � l_in_val ����� ������������� ����. hextoraw � ����� ������ ���������� ��� ������������� ��������
l_in_val  raw(2000) := hextoraw('B959D91C33FE35EC45CE6D0FFC082904502F51D54E456663C55D2FFFF3179957');

-- ����� ��� �� ������, ��� � ����� 
 l_mod    number := dbms_crypto.encrypt_aes128 + dbms_crypto.chain_cbc +
                    dbms_crypto.pad_pkcs5;
 l_dec    raw(2000);

begin

-- �� ������ dbms_crypto ����� �����  decrypt � �������� � ���� � (): 
-- ���������� l_in_val, ��� ����� ������������� ����. ����� ���������� ����� ���������� l_mod
-- � � ����� string_to_raw � ������ utl_i18n �������� ��� ����, � �������, ����� �� ��� ������ � ������� UTF8 
 l_dec := dbms_crypto.decrypt(l_in_val,
                              l_mod,
                              utl_i18n.string_to_raw(l_key, 'AL32UTF8'));
-- ������� ���� ����������, ������ � ������ utl_i18n ����� raw_to_char, ������ ��� l_dec ���� ���� raw 
 dbms_output.put_line('Decrypted = ' || utl_i18n.raw_to_char(l_dec));
end;

-- �����: Decrypted = Super important text


--------------------------------------------------------------------------------------
-- ��� ���� ������ ��������� ������

DECLARE
    l_key      VARCHAR2 (2000) := '1234567890123456';
    l_in_val   VARCHAR2 (2000) := 'Confidential Data';
    l_mod      NUMBER
       :=   DBMS_CRYPTO.encrypt_aes128
          + DBMS_CRYPTO.chain_cbc
          + DBMS_CRYPTO.pad_pkcs5;
    l_enc      RAW (2000);
BEGIN
    l_enc :=       DBMS_CRYPTO.encrypt (utl_i18n.string_to_raw (l_in_val, 'AL32UTF8'),   l_mod,
                            utl_i18n.string_to_raw (l_key, 'AL32UTF8')
                           );
    DBMS_OUTPUT.put_line ('Encrypted=' || l_enc);

    END;

--------------------------------------------------------------------------------------

/* ����, ������ ���������� � ��������. 
��������:
    - �������� ������� TB_CARD_PASS � ����� CARD_PASS VARCHAR2(2000)  �  CARD_NUM  VARCHAR2(20).
    - ����� ������� �������, ����� ������� ���� ������ ����� select.
        ����� ������� ������ (��� ������ � ����, �� � �����)
    - ����� ������� ������� � ������� encrypt. Select ������ ������������� �����. 
    - ����� ������� ������� � ������ ���������� ������������� ����.
    - ����� ������� ������� ������� � ����� �������� �������, ����� ������� �������������� �����
    - ����� ������ ��������, ����� ����� �������� ������ ���������,  � ��������� ����������� ������� 
     var_encrypt  �� ���������� ����������� ������. 
*/

 

-- ������� �������� TB_CARD_PASS
create table TB_CARD_PASS
(
    CARD_NUM  VARCHAR2(20),
    CARD_PASS VARCHAR2(2000);
);

-- ��������� � ��� ������

insert into TB_CARD_PASS values ('4169585312344321', '1234');
insert into TB_CARD_PASS values ('4169585312344322', '4321');
insert into TB_CARD_PASS values ('4169585312344323', '5555');
insert into TB_CARD_PASS values ('4169585312344324', '6666');
insert into TB_CARD_PASS values ('4169585312344325', '9876');

--  �������� ������ � TB_CARD_PASS

select * from TB_CARD_PASS;

  

--------------------------------------------------------------------------
-- �������� ������� �� ���������� 

-- ��� ��������� �����-�� �������� � ���� ������ � �� ���� ������ �� �� �����, ��� �� ����� ���� ��������� �����
-- � ���������� ���������� l_enc  ���� raw 

create function var_encrypt (v_var varchar2)
return raw
is
    l_key varchar2(2000) := '1234567890123456';
    l_mod number := dbms_crypto.encrypt_aes128 + dbms_crypto.chain_cbc +
                                dbms_crypto.pad_pkcs5;
    l_enc raw(2000);
begin
    l_enc := dbms_crypto.encrypt(utl_i18n.string_to_raw(v_var, 'AL32UTF8'),
                                  l_mod, utl_i18n.string_to_raw(l_key, 'AL32UTF8'));
     

    return l_enc;
end;

--------------------------------------------------------------------------------------
-- �������� ������� �� ���������� 

-- ��� ��������� �����-�� �������� � ���� ������ � ���������� ���������� l_dec, 
-- ������� � ��� �������� ���� varchar2, ������ raw - ������.     

create or replace function var_decrypt(v_var varchar2)
return varchar2
is
      l_key varchar2(2000) := '1234567890123456';
      l_mod number := dbms_crypto.encrypt_aes128 + dbms_crypto.chain_cbc +
                         dbms_crypto.pad_pkcs5;
      l_dec raw(2000);
begin
      l_dec := dbms_crypto.decrypt(v_var,
                         l_mod,
                         utl_i18n.string_to_raw(l_key, 'AL32UTF8'));
      return utl_i18n.raw_to_char(l_dec);
end;

 

--------------------------------------------------------------------------

-- ������� ������ card_pass_tg  �� ������� tb_card_pass, � �. ����� �������  var_encrypt
-- ������� ����� ������������ ������
create or replace trigger card_pass_tg
       before insert
       on TB_CARD_PASS
       for each row
begin
       :new.CARD_PASS := var_encrypt(:new.CARD_PASS);
end;

--------------------------------------------------------------------------


--  ������� ��� ������ � TB_CARD_PASS
delete from TB_CARD_PASS;

-- ��������� ����� ������ � TB_CARD_PASS

insert into TB_CARD_PASS values ('4169585312344321', '1234');
insert into TB_CARD_PASS values ('4169585312344322', '4321');
insert into TB_CARD_PASS values ('4169585312344323', '5555');
insert into TB_CARD_PASS values ('4169585312344324', '6666');
insert into TB_CARD_PASS values ('4169585312344325', '9876');

--  �������� ������ � TB_CARD_PASS �� ������� ������� ������������� ������
select * from TB_CARD_PASS;


-- �������� ������ ������� + � ������������� ���� 
select tb.*,
       var_decrypt(tb.card_pass)
       from TB_CARD_PASS tb ;





-----------------------------------------------------------------------------------------

-- �������� ������  17  ����. ����������


--------------------------------------------------------------------------------------------

/*
����������� ���������� ��� �������, ���������� ����� ��������, �� ����� ������ �� ����������, 
����� �� ������ ���������� ����.

�������� 2 ������� �������� � ���������� (������ �� �����) - �� ������, 
��� ��� ������������� ������ dbms_crypto ���������� ������ ������������ system grant 
�� ���������� ������� ������� ������ (� ����� � ������� ����� �������� ��� �����������). 

�������� ���� ��� ������� � ������� �������� (� ����� �� ������ ������), � ���������� ����������� ���.

*/

select * from tb_client_pl;

--------------------------------------------------------------------------------------------
-- �������� ������� �� ���������� 

-- ��� ��������� �����-�� �������� � ���� ������ (���� �� ����� ��� ������� ����� ������)
-- � ���������� ���������� l_enc  ���� raw 

create function func_encrypt_char (v_var varchar2)
return raw
is
    l_key varchar2(2000) := '1234567890123456';
    l_mod number := dbms_crypto.encrypt_aes128 + dbms_crypto.chain_cbc +
                                dbms_crypto.pad_pkcs5;
    l_enc raw(2000);
begin
    l_enc := dbms_crypto.encrypt(utl_i18n.string_to_raw(v_var, 'AL32UTF8'),
                                  l_mod, utl_i18n.string_to_raw(l_key, 'AL32UTF8'));
     

    return l_enc;
end;


--------------------------------------------------------------------------------------
-- �������� ������� �� ���������� 

-- ��� ��������� �����-�� �������� � ���� ������ � ���������� ���������� l_dec, 
-- ������� � ��� �������� ���� varchar2, ������ raw - ������.     

create or replace function func_decrypt_char(v_var varchar2)
return varchar2
is
      l_key varchar2(2000) := '1234567890123456';
      l_mod number := dbms_crypto.encrypt_aes128 + dbms_crypto.chain_cbc +
                         dbms_crypto.pad_pkcs5;
      l_dec raw(2000);
begin
      l_dec := dbms_crypto.decrypt(v_var,
                         l_mod,
                         utl_i18n.string_to_raw(l_key, 'AL32UTF8'));
      return utl_i18n.raw_to_char(l_dec);
end;

--------------------------------------------------------------------------

-- ������� ������ clname_tg  �� ������� tb_card_pass, � �. ����� �������  var_encrypt
-- ������� ����� ������������ ������
create or replace trigger clname_tg
       before insert
       on tb_client_pl 
       for each row
begin
       :new.name_v := func_encrypt_char(:new.name_v);
end;

--------------------------------------------------------------------------
-- ��������� ������ �������
insert into tb_client_pl (id, name_v) values (10,'���� �������������');

-- �������� ������ ������� �� �������: ���������� ��� ���?
select *
      from tb_client_pl c 
      where c.id = 10;
-- �����: name_v =
--  B23B6F729D9C968480DC8DDB923FEC49C91B7F2B83DAAE4359B3D858780A31D4B9C6DB2F885D017E5E600C42D089E6CF


-- �������� ��������������� ����� 
select c.name_v,
       func_decrypt_char (c.name_v) as decrypt
     from tb_client_pl c
     where c.id = 10;
-- �����: decrypt = ���� �������������


---------------------------------------------------------------------------
-- ������ ����, ����� �������� ��� ����� ������� �� ������������� �����. 

-- ����� ����� ����������� ������ ������� � id 10, ����� �� ������, ��� ��� ��� ��� �����������
delete from tb_client_pl c where  c.id = 10;

-- ������ ��� ����� �� ������ 
select * from tb_client_pl;


------     ������ ���������� ����� 

declare
   var_id number;
   max_var_id number;
   char_name varchar2(2000);
begin
  var_id :=1;
-- max id �����, ����� ���������� ����, ����� var_id > max_var_id
  select max(cl.id)
         into max_var_id
         from tb_client_pl cl;   
    loop 
-- ������ ������� ��� ������� � id = 1  � ���������� char_name   
    select cl.name_v
        into char_name
        from tb_client_pl  cl 
        where cl.id = var_id;
-- ��������� ����������, ������� ������������� ��� ���� ��, ����������� �������� ������ ������ ����� var_id                           
    update tb_client_pl c1
           set c1.name_v = func_encrypt_char(char_name)
           where c1.id = var_id;                                          

-- �������  var_id �� 1
    var_id := var_id + 1;  
    exit when var_id = max_var_id + 1;
end loop;            
exception when others then
   dbms_output.put_line ('��������� �������������� ������!');
end;

------- �������� �������������� ����� + ������������� ���

select cl.id,
       cl.name_v as encrypt_name,
       func_decrypt_char(cl.name_v) as decrypt_name
     from tb_client_pl  cl

