

----------------------------------------------------------------

-- �������� ������ 5 ����  ������   ������ � �������

 
----------------------------------------------------------------


/*
������: �������� ����� ���������� ������� � ���������� � ������� ����� ����������.
��������
- ���� ������ �� ����������, �� ������ � exceptione
- ���� ������ ����������, �� ��� ������, �� �������� ���� � nvl � �����:
   '�� ������ ������ � id ___ '
- ���� ������ ����������, �� �������� ��� ����� � �����:
  '������ ��������� � ������: ___'
 
��� ����: 
� ��������  � id (61, 100) ���� ������
� ������� � id (4) ����� ������ ������. ��� ��� ��������, �� �������.

*/ 


-- ����� ������������� ��� ���� �������, � �. ����� �������
   select cl.*, ad.*
          --nvl(ad.city_v, '0') 
          -- ���� 1-� ������� ������, ��� �������� 0
          -- ���� ������ �������� ����, �� �� � ��������
          from tb_client cl
          left join tb_address ad on ad.client_id = cl.id
          where cl.id = 4;
          
declare 
   var_id number := 61;
   var_city_v varchar2(500);
   pi constant number default 3.14; -- �������� ��������� 

begin
  

   select nvl(ad.city_v, '0')
          into var_city_v 
          from tb_client cl
          left join tb_address ad on ad.client_id = cl.id
          where cl.id = var_id;
  
  if (var_city_v = '0') then 
     raise_application_error (-20200 , '�� ������ ����� � ������� � id ' || var_id );
  -- ���� select ������ '0', �� � ���� var_city_v = 0, ����� � ��� ���� ���� ����� ������
  else 
    dbms_output.put_line('������ ��������� � ������: ' || var_city_v );
  end if;
  
  exception
     
     when NO_DATA_FOUND then
       dbms_output.put_line( '������ �� ������ � ��');
     when others then 
       if ( sqlcode = -20200) then  
        dbms_output.put_line ( sqlerrm );
       else  dbms_output.put_line ( '���� ������' );
       end if;
end;
 
-- exception when TOO_MANY_ROWS then raise  to_many_addres;
--when to_many_addres then dbms_output.put_line 
--         ('����� �� ���������� ID � ������� ����� 1 ������ �������');   



-----------------------------------------------------------------------------

-- NLS_SESSION_PARAMETERS - ��� ���? 

select *
from NLS_SESSION_PARAMETERS
where parameter = 'NLS_LENGTH_SEMANTICS';



-----------------------------------------------------------------------------
-- �������������

declare 
   var_id number := 61;
   var_city_v varchar2(500);
   pi constant number default 3.14;
   var_test varchar2(200) := ' ''���� ������'' '; 
   -- ���� ��� '', ������ ' � �������� ������
   -- �����:  var_test:   '���� ������' 
 
   
begin
  dbms_output.put_line('var_test:  ' || var_test );

end; 

-- �����:  var_test:   '���� ������' 



-----------------------------------------------------------------------------------------
-- ������������ �������
-- chr(0) �����, ����� ������ ���� �� �������� � �������, ����� � ����� ������ �� ���� ������
-- _(������), � � ������ ������ �� ��� ��� null

declare 
  
  
begin
  dbms_output.put_line('ASCII(''T''): ' || ASCII('T'));
  dbms_output.put_line('��� : ' || chr(0));

end; 

-- ����� 1:   ASCII('T'): 84  - ��� � ���� ���������� ������������� ����� �
-- ����� 2:  ��� :   - ����� � ������ ����� ���� ������


-----------------------------------------------------------------------------------------


-- �������������� ����� 
 -- INITCAP ������ ������ ����� ��������� 

declare 
  
begin
  dbms_output.put_line( INITCAP('����, ���'));
 
end; 
-- �����: ����, ���
 

-----------------------------------------------------------------------------------------


-- ������: ���������� ��� ����� � ������� 

 
declare
  names varchar2(200) := '���������,����,����,������,�����,������';
  i  number := 1;
  result varchar2(200);

begin
      
    -- result := substr (names, 1, instr(names, ',') -1 );
    -- ��� ��� �� ������ ������ ������ �������: ���������: ��������� 
  loop
    if (instr(names, ',') = 0) then
      result := names;
    else  
      result := substr(names, 1, instr(names, ',') - 1);
    end if;
   
    dbms_output.put_line('���������: ' || result);
    names := substr(names, instr(names, ',') + 1);

    exit when instr(names, ',') = 0;
  end loop;
 
  dbms_output.put_line('���������: ' ||names);

end;



-----------------------------------------------------------------------------------------

-- ������� / ������  ��� ���� ���� ������ 
declare

   names varchar2(500) := '���������,����,����,������,�����,������';
   comma_location number := 0;

begin

   loop

   comma_location := instr(names, ',', comma_location + 1);
   exit when comma_location = 0;
   dbms_output.put_line(comma_location);

   end loop;

end;



-----------------------------------------------------------------------------------------
 
  





----------------------------------------------------------------


-- �������� ������ 4 ����  exception   ����������  




----------------------------------------------------------------

/* 
  �������� ��������� ���� ���������� ����� ��������:
    - �������� �� ���� ������� � ��������� ���� name_v 
    (������ ������� � ������ � �����, ������ ������ ".", ������ ����� � ������� �����)
    
    ��������� ���� ���: ������ ������� � ������ � � �����, ������ ����� � �����
        (!) ��������� ���� �� ��� ����� ����� ��������� � ����, ���� ���� - 
        � ������ output ������� ��������� "������������ ���: " || inn_v
*/
  
select cl.* from tb_client_new cl ; 
select rowid, cl.* from tb_client_new cl;




DECLARE
       inn_counter number := 0;
BEGIN
  --  replace �������� ����� �� �������, trim �������� �������
  -- ������� INITCAP ����������� ������ ����� ������� ����� � ������  � ��������� 
  
  update tb_client_new set name_v = initcap(trim(replace(name_v, '.', '')));

  for client in --� ����� ��������� ��� ������� �������
      (select * from tb_client_new)
  loop
      update tb_client_new set inn_c = (trim(trim('.' from inn_c)))
          where id = client.id;
  
  --�������, ���� �� � ���� ������ � ������ id, �� � ��� �� ��� 
  --  and id != client.id �������� ��������� ��������� ����� � ����� �����
      select count(inn_c)
         into inn_counter
         from tb_client_new
         where inn_c = client.inn_c
            and id != client.id;

      if(inn_counter > 0)--���� �����, �� ������� ��������� 
         then dbms_output.put_line('������������ ���: ' || client.inn_c||' � ������� � id: '|| client.id);
      end if;
  end loop;

END;






