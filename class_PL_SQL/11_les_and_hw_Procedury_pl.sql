
----------------------------------------------------------------



-- �������� ������ 11  ����. ��������� 


--  ���������

----------------------------------------------------------------

 
-- ������ ������ ���������, ������� ����� ��� ���������� ������� 
-- ��� ��� �� �������
select system.balance_plus from dual; 


-- �������� - ��� � ���� ���-����� 
select rowid, c.* from tb_contract_pl c;

tb_contract_pl 
tb_tarif_pl

select * 
       from tb_contract_pl c
       join tb_tarif_pl t on t.id = c.tarif_id
       
  

----------------------------------------------------------------

/* ������: ������� ��������� �� ���������� ������� ������, ��������, ��� � ��� ���� 
    ������ ��� �������    (������ ��� �������)      */
    
��� ������� ��� ����: 

- ������� ��������� � ������ var_inn, var_amount, var_result
-- �����! 
out varchar2  - ����� ������ ��� ������ ������������, �� ������� ����
����� '��' ��� �������� ������ ���������, � ����� ������ ��� 
������ ��������� ������  var_result ������ ������ ���������� (procedure_result), ������� �������� 
� ������ ���������. 
� ������ ��������� �� ����� ��������� ����� '��' ��� �������� �������� � ������� 
��������������� ����� ��� ���������� �������.  


- � is ������� ����� ��� contr_info_type � ���� ������ (���� ������� � ������ cl_id, contr_id
    balance )
>   � ����� ��� �� � is ������� ���������� contr_info ���� 

     
- ������ begin � select �������� id �������, id ��������� ���, � ������ ��������� (��� �����).
�����! �  select � where �� �������� ������ ����, ��� ��� ������������� var_inn
�����! > � �������� ������ ���� state_id = 1, �.�. ���-����� ������� 
�����! > � ����� ������ ���� ������ 

- ��� ��� �� �������� � select �� ������� � ���������� ������� contr_info

- ����� � begin ������� update tb_contract_pl (������� �������� ����� ������) , ��� 
 > id ��������� ����� ����� id ��������� ������ ������� (������ � ��� ���� � contr_info )


--- ����� �������� � ������ ���� ������� --- 
create or replace procedure balanse_plus_by_inn (
          var_inn varchar2,
          var_amount number,
          var_result out varchar2
          )
-- ���, ��� ������� �� �������� ������ IS, �������� ���� ���������. IS ��� declare
is
          type contr_info_type is record (
               cl_id number,
               contr_id number,
               balance number
          );       
          contr_info contr_info_type;
begin
     select cl.id,
           c.id,
           c.balance_n
      into contr_info
      from tb_client_pl cl
      join tb_contract_pl c on c.client_id = cl.id
     where cl.inn_v = var_inn
           and c.state_id = 1 and rownum < 2; -- ��� ���������, ����� ����� 1 �����, ���� ������ ������� 
           -- � �������

    update tb_contract_pl
       set balance_n = var_amount + balance_n
     where id = contr_info.contr_id;
     
     var_result := 'OK';
     
     -- commit;  �� �������� �������, ��� ������ ���� ���� ������, ��� ����� ���������� 
     -- �����������. 
   
exception when NO_DATA_FOUND then
               dbms_output.put_line('������ �� �������!');
               var_result := 'ERROR';
          when others then
               dbms_output.put_line('������ ������! �� ������� ��������� ������!');
end balanse_plus_by_inn;

-- ��������� ������� 

--------------------------------------------------------------------------------  

-- �������� ��������� � ��������� �������������� � ���� ��� 
-- �����: ������ �� �������!    ERROR
 
declare
  procedure_result varchar2(200);
begin
 
  balanse_plus_by_inn(
    '55555'
    , 1000
    , procedure_result
  );
 
  if (procedure_result = 'OK') then
    dbms_output.put_line('OK');    
  elsif (procedure_result = 'ERROR') then
    dbms_output.put_line('ERROR');
  else
    dbms_output.put_line('�������� procedure_result = ' || procedure_result);
  end if;  
end;

/* �������� ��������� � ��������� ������������ � ���� ��� 222052992001155, �� ���������
 � ������� c id 3   ������� ����� ���������. � ���� ���� �������� � id 5, ��� ������ 3333
������ ��� ������ ���� = 4333

           */


declare
  procedure_result varchar2(200);
begin
 
  balanse_plus_by_inn(
    '222052992001111'
    , 1000
    , procedure_result
  );
 
  if (procedure_result = 'OK') then
    dbms_output.put_line('OK');    
  elsif (procedure_result = 'ERROR') then
    dbms_output.put_line('ERROR');
  else
    dbms_output.put_line('�������� procedure_result = ' || procedure_result);
  end if;  
end;
-- �����:   OK 


select rowid, cl.* from tb_contract_pl cl;
tb_client_pl
select * from tb_client_pl;




update tb_contract_pl
       set balance_n = 500
     where id = 2;









----------------------------------------------------------------







----------------------------------------------------------------



-- �������� ������ 11  ����. ���������. 



----------------------------------------------------------------


 
