
----------------------------------------------------------------

-- �������� ������ 8 ����  ������. ���������� ������. ���������� ��������. 

-- ������ �� ������ �������.    ������ �� ������ �������  

-- ������, ������������ �������������

----------------------------------------------------------------

declare 
 var_first_name_v  varchar2(200);
 var_inn varchar2(20);
begin
  
    select cl.first_name_v, cl.inn_c
           into var_first_name_v, var_inn -- ����� � 2 ���������� ����� �������� ��� ������
           from tB_client cl 
          where cl.id = 1;
          
          -- ������� ��, ��� ��������
          dbms_output.put_line(var_first_name_v || ' * ' || var_inn ); 
          


end; 

-- �����: ����� * 10110199210000  
 
----------------------------------------------------------------
  

/*   ������ �� ����, ��� ��� ��������� � ������, �������.
     
 ����, ������ �� ������ �������   */ 

declare 
 tb_client_row
         tb_client%rowtype;
 -- rowtype �������, ��� ��� ����� ��� ���� ����� ������� ���������
 -- ������ ��� ���������� ����� ����������, � �. ���� ��� ������ � tb_cl
begin
  
    select *
           into tb_client_row 
           from tb_client cl 
          where cl.id = 1;
          
          dbms_output.put_line(tb_client_row.first_name_v || ' * ' || tb_client_row.inn_c);
          dbms_output.put_line(tb_client_row.first_name_v || ' : id = ' || tb_client_row.id);  
          
end; 

-- �����:
-- ����� * 10110199210000   
-- ����� : id = 1




-----------------------------------------------------------------------------------------
 
  
/*    
      ������ �� ������ �������   */ 

declare

      cursor my_books_cur is
             select *
                    from books
                    where author like '%����� ���%';

        one_sf_book my_books_cur%rowtype;

begin
end;
 

-----------------------------------------------------------------------------------------
 
  
/*  ������� ����������� ������ */ 

begin

    for book_rec in (select * from books) 
      loop
        calculate_total_sales(book_rec);
      end loop;

end;



----------------------------------------------------------------
 
 
--������, ������������ �������������

declare

       type book_info_rt is record 
            (
             author books.author%type,
             category varchar2(100),
             total_page_count number);
             
       steven_as_author book_info_rt;
begin
end;

 
----------------------------------------------------------------
  

/*  ������: ������� ������� (id, ���, ���) � ����� ��� ���������� 
    � ������� � id = 4 ���� 2 ������ 
    ����� ��� ������� ������ ������������ ��������. 
    ����� ���� �������� ������ ������ (����������). ��������� �� �������� 
    1-� ����������. � ����� �������� ������ ���������� � ����������� ������� �� ����������.
 */ 

declare 
-- ����� �� ����������� ��� ���
  type cl_info_type is record 
            (
             id tb_client.id%type,
             first_name varchar2(200),
             inn char(16),
             city  varchar2(200)
              );
-- �����! id number ����� �������� �� id tb_client.id%type
-- ����� ���� ������������ �������� ����������, �� ���������� ��� �� ������� id

-- ����� �� ���������� ���� ���������� cl_info_rec � cl_info_rec_copy (������� �����)
-- ����� ������ cl_info_rec ����� ��� cl_info_type
   cl_info_rec cl_info_type;
   cl_info_rec_copy cl_info_type;
       
begin
  select cl.id,
           cl.first_name_v,
           cl.inn_c,
           ad.city_v
           into cl_info_rec 
           from tb_client cl
           join tb_address ad on ad.client_id = cl.id 
          where cl.id = 4 and rownum < 2;
--  and rownum < 2  ��� ������ ������� ������ ������ �� ���� �����     
          
          dbms_output.put_line(cl_info_rec.first_name ||
           ' --- ' || cl_info_rec.inn || ' --- ' || cl_info_rec.city );
           
--����� ����������� � �� ���������� ���������� ������ ������, �.�. 1-� ���������� cl_info_rec
     cl_info_rec_copy :=  cl_info_rec;     
     dbms_output.put_line ( 'cl_info_rec_copy.inn = ' ||  cl_info_rec_copy.inn);

-- ����� �������� ������, ���� ����� ����, ��� �� ���� �������� ������     
     cl_info_rec := null;

-- �� ������ ��� ��� ������ �� ��������� 
     dbms_output.put_line('cl_info_rec = ' || cl_info_rec.city);
end; 

/*    �����: 
      ����� --- 13110199210003   --- ������
      cl_info_rec_copy.inn = 13110199210003  
      cl_info_rec =                                   */






-----------------------------------------------------------------------------------------





select * from tb_address for update; 
-- for update ����������  ��� ����� ������ ������ � ������ �������
-- ���� 1 ������ �� �������, 2 ������ �� ������ �������.
 


 select rownum, cl.*
           from tb_client cl
           join tb_address ad on ad.client_id = cl.id 
          where cl.id = 4;
-- rownum ����������� ������ ������ ������




-----------------------------------------------------------------------------------------

/*   ������ ������ � �������� ����, �� �� ����� ��������� ����� ��������� 
���������������� ������� ������ ���������� �� ������ ��������.             */ 

declare 
 
  type cl_info_type is record 
            (
             id tb_client.id%type,
             first_name varchar2(200),
             inn char(16),
             city  varchar2(200)
              );
 
   cl_info_rec cl_info_type;
   cl_info_rec_copy cl_info_type;
       
begin
  
    select cl.id,
           cl.first_name_v,
           cl.inn_c,
           ad.city_v
           into cl_info_rec 
           from tb_client cl
           join tb_address ad on ad.client_id = cl.id 
          where cl.id = 4 and rownum < 2; 
          
 
       
     cl_info_rec_copy :=  cl_info_rec;
     -- ����� ������ ��������� �� ��������� ������ ������ begin
     -- if (cl_info_rec_copy != cl_info_rec )      
            
end; 

 

-----------------------------------------------------------------------------------------

--������� �������� ������ � �������

/*  ����� ��  ������ � client_row �������� ������ ������� � id 100.
     

*/

declare

  client_row tb_client%rowtype;
  max_id number := 0;

begin

    select *
           into client_row
           from tb_client cl
           where cl.id = 100;
-- ������� �� �����  ������� ��� 
    dbms_output.put_line('client_row.first_name_v = ' || client_row.first_name_v );

-- ������ ������� ��� ������� ������ ������, ����� ����������� � ���� ������ ' _1'
    client_row.first_name_v := client_row.first_name_v || '_1';

-- ������� ����� ��� ������� �� ����� ������
    dbms_output.put_line('client_row.first_name_v = ' || client_row.first_name_v );

-- �������� ������������ id �� ����� ������� �������� � ��������� � ���� + 1,
-- ����� ������ ��������� ����� id ������ ������� 
    select max(cl.id)
           into max_id
           from tb_client cl;
                client_row.id := max_id + 1;
                
--��������� � ������������ ������� ����� ������ �� ������� ������� (��� ��� �� �������� �� ��� + '_1')                 
    insert into tb_client (id, first_name_v) values (client_row.id, client_row.first_name_v);
    
     -- commit; ����� ���������� ���� ���������. 
     
exception when others then
  rollback;

end;

-- �����: � ������� ��������� 101 ������ � ������ ������_1, ������ ��� 100 ������� ����� ������ 

select * from tb_client;





-----------------------------------------------------------------------------------------
-- ������� ������ � ������� ������

declare

  client_row tb_client%rowtype;
  max_id number := 0;

begin

    select *
           into client_row
           from tb_client cl
           where cl.id = 100;
 
    dbms_output.put_line('client_row.first_name_v = ' || client_row.first_name_v );
 
    client_row.first_name_v := client_row.first_name_v || '_2';
 
    dbms_output.put_line('client_row.first_name_v = ' || client_row.first_name_v );
 
    select max(cl.id)
           into max_id
           from tb_client cl;
           
    client_row.id := max_id + 1;

    -- ����� ������� ������� ���� ������ � ������������ ������� �� ������� �������                         
    insert into tb_client   values client_row;
    
    -- �������� ����� ������� � id 100 � ����� id 101 � ������ = ��� + '_2'
     
exception when others then
  rollback;

end;
 
select * from tb_client;







-----------------------------------------------------------------------------------------

-- ��������� �������

    if client_row100.id = client_row101.id
       and client_row100.name_v = client_row101.name_v --� �.�., ����������� ��� ����
    then
       dbms_output.put_line('������ �����');
    else
       dbms_output.put_line('������ �� �����');
    
    end if;



-----------------------------------------------------------------------------------------

-- ���������� ������������

  create or replace trigger tg_tb_client_bi
     before insert on tb_client
     for each row

begin
    if :new.id is null then
       :new.id := sq_tb_client.nextval;
    end if;

  --:old.id --��� ������ ����������

end;


-----------------------------------------------------------------------------------------

-- �������� ������  8 ���� ������

----------------------------------------------------------------

/* 
 ���������� ��������� ���� ����������� ��� ������� (�� �����) ��������� ������.
����� ������������������:
- �������� ������������ ID
- � ����� ������ �� 0 �� ������������� �������� ID, ���� ������ ����
        - ���������� ��� � ������ output (� ����� ��������� ������� �� �������
        � ������!)
*/
  

 



















