
----------------------------------------------------------------



-- �������� ������ 10  ����. ������� 


--  ������, ��� ��� ��������� �����, ������� ����������/��������� �� �����-�� ������

----------------------------------------------------------------



-- ������: ������� ������ ��� ��_��������, ������� ������ � � ����� �������� ��� ����� �������� �����
--         ���������� '1' � ����� �����.   


declare
-- ������� ������ ��� ������� tb_client, �������(left) ������ 
  cursor client_cur is
    select c.id,
           c.first_name_v,
           ad.city_v,
           adt.name_v
      from tb_client c
      left join tb_address ad on ad.client_id = c.id
      left join tb_address_type adt on adt.id = ad.type_id;

begin
-- ������ ������� ������ client_rec, ����� ������� � ����� 
-- ��������� ���� ������� ��������, ����� ���������� '1' � ����� ������� 
  for client_rec in client_cur loop
    update tb_client
       set first_name_v = first_name_v || '1'
     where id = client_rec.id;

-- sql ����� ��������� ���� �� ����� ���� ������, � ������ ������    if sql%found �������,
-- ��� ���� ������� ���������� ���� ���������, �� ����� ������� out_put, ��������� ���� 
    if sql%found then
      dbms_output.put_line('�������� ��� cl � id = ' || client_rec.id);
    end if;
  
  end loop;

end;
select * from tb_client
/*
�����:  �������� ��� cl � id = 8
�������� ��� cl � id = 1
�������� ��� cl � id = 2
�������� ��� cl � id = 2
�������� ��� cl � id = 3
�������� ��� cl � id = 3
�������� ��� cl � id = 4  */



----------------------------------------------------------------
-- ������: �������, ������� ������. ��������� ������� � ������� ������� 


declare 

  cursor client_cur is
    select c.id,
           c.first_name_v,
           ad.city_v,
           adt.name_v
      from tb_client c
      left join tb_address ad on ad.client_id = c.id
      left join tb_address_type adt on adt.id = ad.type_id;
      
-- ������� ������, � �. ����� ����� ��������� �������.
   client_rec client_cur%rowtype;

begin
      open client_cur;
  
-- fetch ��� ��������� ������, �� ����� ��� ���������, ���� ������� ������
-- fetch ������ ���� ������ �����������, ����� ��� ��������, ���� ����� ������������
      fetch client_cur into client_rec;

      if ( client_cur%ISOPEN) then 
        close client_cur;
      end if;
      
     dbms_output.put_line('C ������ ��������� ����� ������ � �������: ' ||  client_rec.id);
     dbms_output.put_line('C ������ ��������� ����� ������ � �������: ' ||  client_rec.first_name_v);     
 
end;

/*    �����:
        C ������ ��������� ����� ������ � �������: 8
        C ������ ��������� ����� ������ � �������: ������    */


----------------------------------------------------------------

/*      ������: �������� ��������, ����� ������ �������� � ������. � ����� 
        �� ������ ������ ������� � �������     */
declare 

  cursor client_cur is
    select c.id,
           c.first_name_v,
           ad.city_v,
           adt.name_v
      from tb_client c
      left join tb_address ad on ad.client_id = c.id
      left join tb_address_type adt on adt.id = ad.type_id
      where c.id in (1, 4, 5);
      
    client_rec client_cur%rowtype;

begin
      open client_cur;
   
      fetch client_cur into client_rec;
         dbms_output.put_line('�� ���� ' || client_rec.first_name_v || ' id: ' || client_rec.id);

-- ��������� ������� ������ ������, ���� ����� �������� (client_cur%FOUND)
--      ���� �����-�� ������ ������ client_cur (�� ��� 3) � ����� ��������� ������ ������
-- �  client_rec � ������ ��� ������� � �������. ����� ��������� ������ �� �������� ��� ����, ����:
   
       loop
         dbms_output.put_line('����: ' || client_rec.first_name_v || ' id: ' || client_rec.id ); 
         fetch client_cur into client_rec;
         
         -- �����, ��� ������ fetch �� ������ ������        
         exit when client_cur%NOTFOUND;
        end loop;
        
/*  ������ ��� ����, ��� ��������� ������ ��� ���� ���������: 
      while (client_cur%FOUND) loop
         fetch client_cur into client_rec;
         dbms_output.put_line('����: ' || client_rec.first_name_v || ' id: ' || client_rec.id );
       end loop;   */   
      
       close client_cur;
       open client_cur;

      if ( client_cur%ISOPEN) then 
          dbms_output.put_line('������� ������');
          close client_cur;
      end if;
end;

/*  �����:   
      �� ���� ����� id: 4
      ����: ����� id: 5
      ����: ����� id: 1
      ����: ����� id: 4
      ����: ����� id: 5
      ������� ������
       */
     
 
----------------------------------------------------------------

   
/*
    ����� ���
    1. �������� � ��������� ���� ��������
    2. �������� �� ���� ��������, ���������� �� ������ ��� - ���
    3. ������ � �� = 3 ����� �������� ���, ���������: �DDMMYYYY12345678
    4. ��������� � ����
    5. ���������� ������ others => rollback
    
����: bulk collect ���� ����������� ��� ������ ��� ������� �������� � �������� � ���������� ������
 ����� ���������� � ����� ������ 10 ������� ( where rownum > 11)

� ������� (id =3) � ������ ��� ��� : 2_21_10_1992_10002  
����� ������� ���� ��� ��������� ��: 2_19_08_2022_10002   �.�. ��������� ����, ������ �� 
*/
declare
    type client_list_type is table of tb_client%rowtype;
    client_list client_list_type;
begin
    select * bulk collect into client_list
           from tb_client;

--  in client_list.first .. client_list.last  
--  '..'  ���������� ������� ������ � 'cl' - ��� ������ ����� 

    for cl in client_list.first .. client_list.last loop
      dbms_output.put_line(client_list(cl).first_name_v || ' - '  || client_list(cl).INN_C);
     
      if (client_list(cl).id = 3) then
        dbms_output.put_line('�������� ���');
        update tb_client
           set INN_C = substr(client_list(cl).INN_C, 1, 1)
               || to_char(sysdate, 'ddmmyyyy')
               || substr(client_list(cl).INN_C, 10)
         where id = client_list(cl).id;
      end if;
    end loop;
end;

select * from tb_client cl where cl.id = 3;













----------------------------------------------------------------






----------------------------------------------------------------



-- �������� ������ 10  ����. �������. 



----------------------------------------------------------------


 
