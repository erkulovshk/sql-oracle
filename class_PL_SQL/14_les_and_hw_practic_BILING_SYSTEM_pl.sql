 
----------------------------------------------------------------



-- �������� ������ 14  ����. BILING SYSTEM



------------------------------------------------------------------------------------- 


/* ������

���������� ���� �������� (��� � ���) ��� ��������,
����� ������ ������ � ����, ������ ��� � �� ��� ���������
������� ��� ������ �������. 
***** ������ ����� � ����� 

�������� �� ���� ���������� (���� ���-�����), ���� ��� 
��������� ������, � ���� ����� ��������� - ���������� ���
������� ��� ������ ( ����� �� �����). 
***** ������ ����� � ����� 

�������� ����� �������� (���������� ����������), ��������� ���������� ��
����� �������� � ����, ��������� �������� �� ��, � ���� ��� ��
1. ����������� �������� � ������� (������ ����� ��������)
2. �������� ����� �������� � ���� ���������� 
***** ������ ����� � ����� 

---------------------------------------------------------------------

- ������ �� ����� 14 

������ �������� ������ �� 22 ����. ����� ������ ����������
�� �������� 1.22 ��� � �������� 5.4 �� �������.
(��������� � ������� � �������). 
  
*/

 
select * from tb_client_pl;
select * from tb_contract_pl;
select * from tb_tarif_pl;

declare
 begin    
  
  
end;



-----------------------------------------------------------------------
  
-- ������ ��� balance_info, ������� ���� ������������ ������ ������� get_balance_info

create or replace type balance_info is object (
       message_v varchar2(200),
       second_n number,
       sms_n number,
       mb_n number
);

---------------------------------------------------------------------------------

-- ������ ������� get_contract_by_imsi, ������� ���������� ��� ������ ��������� 
--  ��������� imsi_n ��������� ( ��� �� ���������� �����)
  
create or replace function get_contract_by_imsi (
       var_imsi number
)
return tb_contract_pl%rowtype
is
       var_contr tb_contract_pl%rowtype;
begin

   select *
     into var_contr
     from tb_contract_pl c
    where c.imsi_n = var_imsi;  


    return var_contr;

exception
  when others then
       dbms_output.put_line('������ ��� ��������� ���������!');
end;



-------------------------------------------------------------------------------

--������ ������� get_tarif_by_id, ������� ��������� �� ���� id ������
--  � ���������� ��� ������ �� ����� ������ � ���������� var_tarif, ������� �������� ���
--  ��� ���� ������� tb_tarif_pl 

create or replace function get_tarif_by_id (var_id number)
return tb_tarif_pl%rowtype
is

-- �������� tb_tarif_pl
       var_tarif tb_tarif_pl%rowtype; 

begin

-- �� var_id ��� ������ �����, ��������� ��� id, ����� ���� �� ������ � ���� � ���������� var_tarif

       select *
         into var_tarif
         from tb_tarif_pl t
        where t.id = var_id;

-- ����� ������ �������: ������� var_tarif. 
        return var_tarif;

exception
        when others then
          dbms_output.put_line('������ ��� ��������� ������!');
end;


------------------------------------------------------------------------
/*
������ ������� GET_BALANCE_INFO, ������� ����� ���������� ���������� ����� ���� ���������� 
balance_info_var. � ����� � ������ ��������� ����� ��� �����  �������  ������������ ����������
balance_info_var ��� ���� ������ ������� GET_BALANCE_INFO.
*/

� ���, GET_BALANCE_INFO ��������� � �������� ���������  var_imsi (���������� ����� ���-�����)  
� ���������� ���������� balance_info_var, �. �������� ��� BALANCE_INFO (� ����� ������ ����
 ��� ������� ���������). 
�� ��������� �� balance_info_var ���������� � ������� balance_info � �������� ���������. 

����� ������ ���������� ���� ����� ��������� ������� � balance_info_var. 



create or replace function get_balance_info (
       var_imsi number
)
return balance_info
is
       var_contr tb_contract_pl%rowtype; 
       var_tarif tb_tarif_pl%rowtype; 
       
       balance_info_var balance_info
                        := balance_info(null, null, null, null);
begin

-- ������� ����� var_imsi �������� �� tb_contract_pl ���� � ���������� var_contr
       var_contr := get_contract_by_imsi(var_imsi);
       dbms_output.put_line('ID ��������: ' || var_contr.id);

-- �� ���� tarif_id, ������� �� ����� � ����������� ��������� ����, ������ ����� 
-- � ���� � ���������� var_tarif 
       var_tarif := get_tarif_by_id(var_contr.tarif_id);  
       dbms_output.put_line(var_tarif.name);

--������ �� � ��������� message_v � ���������� balance_info_var
       balance_info_var.message_v := 'OK';
       
/* ���� ����� � ����. ������, �� is_subscription_fee �����  = 1, 
����� ��������� ���, ���, mb ����� '-1'.  */ 
       case var_tarif.is_subscription_fee
         when 1 then
            balance_info_var.second_n := -1;
            balance_info_var.sms_n := -1;
            balance_info_var.mb_n := -1;
        
           
/* ���� ����� �� �����, �� is_subscription_fee ����� = 0, ����� �� ������ �������� else.
� �����, ��� ������� ������ ������, �. ��������� ����������� ������� ������ �������, ����:
1 - ����� ����� ������� ������� �� ��� ��������� var_contr.balance_n
2 - ��������� ������ �� ��������� 1 ������ �� ��� ������ var_tarif.minute_price � �������� ���-�� ����� 
3 - �������� ���-�� ���������� ����� �� 60 � ������� ���-�� ������. 
� ����� ��� ����� ����� � ��������� ���, mb, ��� ������ ������ ����� �� ��������� ���� �����. 

  */
        else 
           balance_info_var.second_n :=
             var_contr.balance_n / var_tarif.minute_price * 60;
           balance_info_var.sms_n :=
             var_contr.balance_n / var_tarif.sms_price;
           balance_info_var.mb_n :=  
             var_contr.balance_n / var_tarif.mb_price;
       end case;
-- ����� ������� ���� ������� ����������   balance_info_var � ��������� �� �� ������ ������ �*
 

    return balance_info_var;  

exception when others then
    return null;
end;

select * from tb_client_pl;
select * from tb_contract_pl;
select * from tb_tarif_pl;




-----------------------------------------------------------------------
 

/* ����� � ��������� ����� ������� ���� get_balance_info
res  - ��� ���� ����� ����������, �. ���� balance_info (��� ��� ���������� ������, ������ ����)
� ������ ��� ��������� res ���������, ��� �������. 
����� res ���������� � ���������� ������ ������� get_balance_info, ���� ����� ������ 
�������� imsi_n � ������ �� ���-���� �� �������  tb_contract_pl. 
����� ������� ������ ��� ��� ��������, �������� ��� ������ � ������ ������ �� ������������ ��� 
�������. 
  
*/

declare
         res balance_info := balance_info(null, null, null, null);
begin
  res := get_balance_info(123456);
 
  dbms_output.put_line('message_v = ' || res.message_v);
  dbms_output.put_line('second = ' || res.second_n);
  dbms_output.put_line('sms = ' || res.sms_n);
  dbms_output.put_line('mb = ' || res.mb_n);  
 
-- �� ���� ����� ����� ���� ������� ������� �������� �������, � ����� ����� �������� ������

end;


/* �����, ��� var_imsi = 500600: 
    ID ��������: 5
    ����� � ����. ������
    message_v = OK
    second = -1
    sms = -1
    mb = -1 
   
 
   �����, ��� var_imsi = 123456: 
    ID ��������: 1
    ����� �� �����
    message_v = OK
    second = 181500
    sms = 605
    mb = 302.5                  */













------------------------------------------------------------------------



-- �������� ������ 14  ����. BILING SYSTEM




--------------------------------------------------------------------------------------------------------------
