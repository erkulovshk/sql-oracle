 
----------------------------------------------------------------



-- �������� ������ 16  ����. XML type.   c Oracle 9 ���� ��� XML



------------------------------------------------------------------------------------- 
 
select * from tb_client_pl;
select * from tb_contract_pl;
select * from tb_tarif_pl;


-- ������ XML �����, ����� ��� ������� � ��������, ��� ������� �����.
-- �� � ��������� ������ �������� �����.


<clientInfo>
  <name> ������ ���� �������� </name>
  <inn> 222052992001111 </inn>
  <addressList>
    <address>
      <type> ����  </type>
      <city> ������ <city>
    </address>
        <address>
      <type> ��  </type>
      <city> ������ <city>
    </address>
  <addressList>
</clientInfo>

-- ���� �����, �� ��� ������ json. ����� ���� ������ �������� ����� {}, ������ ���� �������� ����� []  
 {
  "clientInfo": {
    "name": " ������ ���� �������� ",
    "inn": " 222052992001111 ",
    "addressList": {
      "address": [
        {
          "type": " ����  ",
          "city": " ������ "
        },
        {
          "type": " ��  ",
          "city": " ������ "
        }
      ]
    }
  },
  "#omit-xml-declaration": "yes"
}




--------------------------------------------------------------------------------------------------------------


/* ������
 ������� �������� ������� � ������ ��� id � ������� ���� xmltype
 � �������� ������

*/

create table test_table_xml (
       fall_id number,
       fall xmltype
);

select * from test_table_xml;

-- ��� �� �������.  ���� ������ '' ��������� <../>
insert into test_table_xml (3, 'fsfsdfsdfdsfds');


-- ��� ���� �� ��������, � ������ ������ ���� �� 
insert into test_table_xml (3, '<jjjjj/>');



--------------------------------------------------------------------------------------------------------------
-- ������� ���� xml (  ����������� ������ xml )  � ������� test_table_xml


insert into test_table_xml
values
      (6,
      -- ������� ��� xmltype � ��� ������� createxml � ������� ������� xml ������ 
       xmltype.createxml('<?xml version="1.0"?>
       <fall>
         <name>Munising Falls</name>
         <county>Alger</county>
         <state>MI</state>
         <url>
           http://michiganwaterfalls.com/munising_falls/munising_falls.html
         </url>
       </fall>')
   );




------------------------------------------------------------------------------------------------------

/* ��� ������� XML-������ �� ������� ������������ ������ ������� XMLType. 
����� existsNode, ���������� � ��������� �������, ��������� �������������
 � XML-��������� ��������� ����. 
 
�  test_table_xml � ������� fall (� ���� ��� XML) ������� ����� getStringVal
� �������� �������� ���������� � ���� ������.   
� extract ������ ��� ������ ������ ��������. � ����� ������ ���������� ���� url. 
                                  */

select f.fall_id 
       --,f.fall.extract('/fall/url')
       , f.fall.getStringVal()
  from test_table_xml f
 where f.fall.existsnode('/fall/url') > 0;


------------------------------------------------------------------------------------------------------
/*   �������, � XML-������� ����� �������� � � PL/SQL. �.�. � ��������� �����. */

declare

-- ������� ���������� ���� XML � 2-� ���������� ���� char
  fall xmltype;
  url  varchar2(100);

begin
  --������� xml-������ ��  ����� ������� test_table_xml � ����� ��� ���� � ���������� fall
  select f.fall
         into fall
         from test_table_xml f
         where f.fall_id = 1;
  
  
  -- ����� � ���������� fall �� ����������� � ������� extract ������ ������ ��������
  -- �� �����,  � ����� ������� getstringval ������ ��� ��� �������� � ���� ������.
  -- � �����, ���� ����� �� ������� � url. 
  url := fall.extract('/fall/url/text()').getstringval;
  dbms_output.put_line(url);
end;


-------------------------------------------------------------------------------------------------
-- ������� �� ������ � xml ������

-- xmlroot ��������� � ��������� ��������� �������� xml � ������ ���� XML
xmlroot(xml_info, xml_version)


-- xmlelement - ��� ��� ������, �� ��������� �� ���� ��� �������� � ��� ��������, �� ����� ����
-- ���� 10 ��. 

xmlelement(name, value);


-------------------------------------------------------------------------------------------------



/*  � ��� ���� �������� � ������� XML. �� ������ ����� ������ ������ ������, 
������� �������� � XML � ������ ��� ������.

xmlroot - ����� � ��� ������ ��������� �� ����� ��� xmlelement � � () ����������� ��� ���������.  
� ��� ������ ��������� �� ��������� ������ XML.

����� ��� ������ � ���������� � xmlelement �� ����������� ��� � �������� (� �������� �� �������� 
������ ������� �������� tb_client_pl )

select - ������ � ����� ���� ������� xmlstring, ������� ����� ��������� ��� � ���� �������� �������. 

*/

select xmlroot(xmlelement("example",
                            xmlelement("name", d.name_v),
                            xmlelement("birthDate",
                                       to_char(d.birthdate_d, 'dd.mm.yyyy'))
                            ),
                 version '1.0') as xmlstring
    from tb_client_pl d;




-------------------------------------------------------------------------------------------------
/* ������: � ��������� ����� ������� ������ ������ � ������� ��������, ��������� �� � ������� XML
 � ���������� ���� XML, � ����� �� ���� ���������� �������� ������ � ���� ������ �
  �������� ��� ������ � ������ ������ ���� varchar2. 
����� ������� ���������� ���� ���������� � ���� ������ */

declare
  xml_x xmltype;  
  var_v varchar2(1000);
  
begin
   
  select xmlroot(xmlelement("example",
                            xmlelement("name", d.name_v),
                            xmlelement("birthDate",
                                       to_char(d.birthdate_d, 'dd.mm.yyyy'))
                            ),
                 version '1.0') as xmlstring
    into xml_x
    from tb_client_pl d
    where d.id = 4;
  
  
 -- ����������� var_v � ���������� ������ � ����������� � ����� ��������, �������
 -- �� ����� �� ����� �� ������� �������� � �������� � ���������� xml_x
  var_v := xml_x.extract('/example/name/text()').getstringval;
  dbms_output.put_line(var_v);
end;



--------------------------------------------------------------------------------------------------------------

/*������ 
����������� ��������� ���������� � xml ���� ��� �������� � �������� ������ ����� 
� ��������� ������� ��������� ��������� ����������: 
����� ��������, ����� ���-�����, ��� �������, ��� �������.

�����! ���� ���������, ��� � ������ ������� ����� ���� ��������� ������� ���������.

1.  ���� ���� � ���� ������ 
2.  ��� ������� � ���� ����

*/

-- ���� ��� ���� select ��������� � ��������� XML ������ ��������� 

select c.phone_number_n,
       c.imsi_n,
       cl.name_v,
       cl.inn_v
   from tb_contract_pl c
   join tb_client_pl cl on cl.id = c.client_id;
   
------------------------ 

/* � ���, ������� ��������� client_xml_generate (��������� ID �������):  
xmlelement - ��� ��� ������, ������ ����-��������, � ��� ����� ������ ���� ����� ��������. 
 
*/ 

create or replace procedure client_xml_generate (cl_id number)

is
       one_file utl_file.file_type;
-- ��� ���������� ������� ��� ��� ������� � ����� �����������, ��������� 
-- ��� ������ �����, �������� �. ����� ���������� ���� ������� i, ������ ��� ��� ���������� ����� � ������       
       i number := 1;
begin
         -- � ����� ������ ���� ���� ����� select
      for cl in ( select xmlroot
                         (xmlelement(
                            "client_info",
                               xmlelement("phone_number", c.phone_number_n),
                               xmlelement("imsi", c.imsi_n),
                               xmlelement("name", cl.name_v),
                               xmlelement("inn", cl.inn_v)
                                   ),
                           version '1.0'
                     -- ������������ ������� �� select ���� ������, ����� �� ��� �����
                     -- ���������� � ����� ��������� ������� cl 
                              ).getStringVal() as xml_info, cl.inn_v as inn
                      
                      from tb_contract_pl c
                      join tb_client_pl cl on cl.id = c.client_id
                      where cl.id = cl_id
                   )
/* ����� ����� one_file ������������ �������� ���������  ��� ���������� ����� ( 'w' - ��� ����� ��������,
���� ����� ���, � �� ������ ��������). � ��� ���� ����� ��������� � �������� ����� � i, ����� ������ �����
���� � �������, � �. ���� ��� ���������.
����� � ������  utl_file ������� ����� put (��������� �� ���� one_file � cl.xml_info - ���
XML � ���� ������� ������, �� ���� ������)
*/
   loop
-- ����� ��������� ����, � ���� ��� ���, �� ������� ��������� � ������������ ������ � ���� (����� 'w').
-- � ��� ����   one_file ��������� ��������� �� ���� ��� ����, ��� ������� � ����     
       one_file := utl_file.fopen('DEVELOPMENT_DIR', cl.inn || '_' || i || '.xml', 'w');
       
--�����! ����� ����� ���������  put ���� 1-� ��������� ���� � ��� ���, � �. ����� ��� �� 2-�� ���������,
-- ��� �������� cl.xml_info ��� ��� � �������  varchar2, ������ ��� �� ��� �������� �� getStringVal()
       utl_file.put(one_file, cl.xml_info);
       
-- ������ ����� ����� ������� ������� �������������� �������� ����
       utl_file.fclose(one_file);

       
       i := i + 1;
   
   end loop;

exception when others then null;
end;



--------------------------------------------------------------------
-- ��������� CLIENT_XML_GENERATE ��� ���������

create or replace procedure client_xml_generate (cl_id number)
is
       one_file utl_file.file_type;    
       i number := 1;
begin
      for cl in ( select xmlroot
                         (xmlelement(
                            "client_info",
                               xmlelement("phone_number", c.phone_number_n),
                               xmlelement("imsi", c.imsi_n),
                               xmlelement("name", cl.name_v),
                               xmlelement("inn", cl.inn_v)
                                   ),
                           version '1.0'
                              ).getStringVal() as xml_info, cl.inn_v as inn
                      
                      from tb_contract_pl c
                      join tb_client_pl cl on cl.id = c.client_id
                      where cl.id = cl_id
                   )
   loop   
       one_file := utl_file.fopen('DEVELOPMENT_DIR', cl.inn || '_' || i || '.xml', 'w');
       utl_file.put(one_file, cl.xml_info);
       utl_file.fclose(one_file);
       i := i + 1;  
   end loop;
exception when others then null;
end;





--------------------------------------------------------------------------------------------------------------



-- �������� ������ 16  ����. XML type





--------------------------------------------------------------------------------------------------------------
