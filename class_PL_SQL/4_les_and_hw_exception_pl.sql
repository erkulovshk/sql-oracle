

----------------------------------------------------------------

-- �������� ������ 4 ���� 

 
----------------------------------------------------------------



/* ������: �������� ���, �������� � ���������� � ������� ��� ���������� */

declare
   var_name varchar2(200);

begin 
   select cl.first_name_v
        into var_name
        from tb_client  cl
        where cl.id = 1;

        dbms_output.put_line ('var_name = ' || var_name );
        
exception when others then null;
end;

-- �����: var_name = �����



-----------------------------------------------------------------------------------------
 

/* ������: �������� ��� �������, ����� ����� id, ��������� � ����������
� ����� ���������� ��� ���� ������:  N0_DATA_FOUND, TO_MANY_ROWS, others

*/

select * from tb_client;

declare
   var_id number := 1;
   var_name varchar2(200);

begin
  dbms_output.put_line ('������ ���������');
  
  -- var_id := 1 / 0;

   select cl.first_name_v
        into var_name
        from tb_client  cl ;
        --where cl.id = var_id;

        dbms_output.put_line ('var_name = ' || var_name );
        
exception
  when NO_DATA_FOUND then dbms_output.put_line ('N0_DATA_FOUND');
  when TOO_MANY_ROWS then dbms_output.put_line ('TO_MANY_ROWS');
  when others then dbms_output.put_line ('������');
end;

-- ���� � declare  var_id number = 1, �� �����: ������ ��������� � var_name = �����  

-- ���� � declare  var_id number = null, �� �����: ������ ��������� � N0_DATA_FOUND    

-- ���� � select ������������  where, �� �����: ������ ���������  TO_MANY_ROWS

-- ���� � declare �������� var_id := 1 / 0, �� �� ����� ����������� � others � �����: ������


 
/* ������ ������ ���������� ����� ����� ���� � ������ ����� ���������. 

  ������: ���������� ����� ������� �� ��� � ������ ���������� ��� � ����.
*/

select * from tb_client_new;


declare
   var_id number := 1;
   var_name varchar2(200);

begin
  dbms_output.put_line ('������ ��������� 1 ');
  
   select cl.name_v
        into var_name
        from tb_client_new  cl 
        where cl.id = var_id;
        
   dbms_output.put_line ('var_name = ' || var_name );
/******************************************************/
        declare 
             var_inn char(14) := '22012144400500' ;
             new_name  varchar2(200);
             
        begin
          select cl.name_v
             into new_name
             from tb_client_new cl
             where cl.inn_c = var_inn;
             
             dbms_output.put_line ('new_name = ' || new_name);      
        exception 
          when NO_DATA_FOUND then dbms_output.put_line
             ('���������� ����: �� ����� �������');
        
        end;

/******************************************************/
           

exception
   
  when NO_DATA_FOUND then dbms_output.put_line ('N0_DATA_FOUND');
  when TOO_MANY_ROWS then dbms_output.put_line ('TO_MANY_ROWS');
  when others then dbms_output.put_line ('������');
 
end;


/*
����� ��������: 

������ ��������� 1 
var_name = ������
new_name = ������


����� ��������� ������. 

* ���� ��� �������� � var_inn �� ��������������,  �� �� ������� N0_DATA_FOUND
* ���� �� ���������� ���� �������� ��������� ������:
 exception 
          when NO_DATA_FOUND then dbms_output.put_line
             ('���������� ����: �� ����� �������');
             
�� ����� �����: 
������ ��������� 1 
var_name = ������
���������� ����: �� ����� �������

* ���� ���:
 dbms_output.put_line ('new_name = ' || new_name);

���� �������� ������ ������ ���������� ���������� �����, ��� ��� ������ ������ ����������
 ���������� ����� � ���� ��������� ���������� new_name. 
� var_name ����� �������� ������ ��������� �����, ��� ��� var_name �������� � ������������ �����. 

*/

/****************************************************/

-- delete from Tb_Client_New where name_v = '�������';
-- delete from Tb_Client_New where name_v = 'Tima,04�092001,Osh';
  

-----------------------------------------------------------------------------------------

 
/*   ���������� ����������� ����������. 
������: �������� � ������� ����������� ����������
*/

select * from tb_client_new;


declare
   var_id number := 1;
   var_name varchar2(200);
   inn_not_found exception; -- �����. ����. ����.
   id_not_found exception; 

begin
  dbms_output.put_line ('������ ��������� 1 ');
  
   select cl.name_v
        into var_name
        from tb_client_new  cl 
        where cl.id = var_id;
        
   dbms_output.put_line ('var_name = ' || var_name );

/** begin ����� 1 ****************************************************/
        declare 
             var_inn char(14) := '22012199500700' ;
             new_name  varchar2(200);
                          
        begin
          select cl.name_v
             into new_name
             from tb_client_new cl
             where cl.inn_c = var_inn;
             
             dbms_output.put_line ('new_name = ' || new_name);      
        exception 
          when NO_DATA_FOUND then raise inn_not_found;
             
        
        end;
/* end *****************************************************/

/** begin ����� 2 ****************************************************/
        declare 
            
             new_name  varchar2(200);
                       
        begin
          select cl.name_v
             into new_name
             from tb_client_new cl
             where cl.id = 98989898;
             
             dbms_output.put_line ('new_name = ' || new_name);      
        exception 
          when NO_DATA_FOUND then raise id_not_found;
        end;
/* end *****************************************************/           

exception
   
  when NO_DATA_FOUND then dbms_output.put_line ('N0_DATA_FOUND');
  when TOO_MANY_ROWS then dbms_output.put_line ('TO_MANY_ROWS');
  when inn_not_found 
    then dbms_output.put_line ('������ ��� ������ ��� �� ���������� ����� 1');
  when id_not_found 
    then dbms_output.put_line ('������ ��� ������ id �� ���������� ����� 2');  
  when others then dbms_output.put_line ('������');
 
end;

/*
�����:
������ ��������� 1 
var_name = ������
new_name = Azamat,05081998,Batken
������ ��� ������ id �� ���������� ����� 2

- ������� ���������� ��������� ��� ������ ������ ������ ����� � begin
- ����� ������ �������, � � ����� � exc ������� ���� ��� ��� ���� ������� ������.
*/


-----------------------------------------------------------------------------------------


/*
 ���������� ������� ������
 sqlcode - ���������� ��� ������ ���������� ����������
 sqlmsg - ���������� ��������� �� ������ ��� ��������� ���� ������
*/

declare
 var_name varchar2(200);
 var_id number := 12121212;
begin 
    select cl.name_v
        into var_name
        from tb_client_new  cl 
        where cl.id =  var_id;
        
exception when NO_DATA_FOUND then
    --dbms_output.put_line ('sqlcode' || sqlcode || ', sqlerrm : ' || sqlerrm );
    dbms_output.put_line ('��� ������ ������� �� id:
     ' || var_id || ' ������ �� �������');
end;




/*
�����: 
sqlcode-1476, sqlerrm : ORA-01476: divisor is equal to zero

���� ������������ � �������� ��� �������, �� �����:
��� ������ ������� �� id:
     12121212������ �� �������
     
*/


-----------------------------------------------------------------------------------------


/*
 ������ ������ � ���������� ����� case 
*/

declare
 var_name varchar2(200);
 var_id number := 12121212;
begin 
    select cl.name_v
        into var_name
        from tb_client_new  cl 
        where cl.id =  var_id;
        
exception when others then
        case   sqlcode
          when -1843 then 'sdsds'
          when -1843 then 'sdsds'
          when -1843 then 'sdsds'
          when -1843 then 'sdsds'
         end;
            

end;

-----------------------------------------------------------------------------------------


/*
��������� RAISE_APPLICATION_ERROR

��� ������������� ����������, ������������� ��� ����������, Oracle
������������� ��������� RAISE_APPLICATION_ERROR. �� ������������ �����
�������� RAISE (������� ���� ����� ������������ �������������
 ��� ���������� ���� ����������� ����������) ����������� � ���,
  ��� ��� ��������� ������� � ����������� ��������� �� ������.
*/
 



----------------------------------------------------------------
/*
DBMS_UTILITY.FORMAT_CALL_STACK - ������� ���������� �����������������
������ �� ������ ������� � ���������� PL/SQL. ������������ ���������� �������
�� �������������� ���������� ������; ��� ����� ���������� ��� �����������
���������� ������ ����.
*/

begin
  dbms_utility.format_call_stack();
end; 






----------------------------------------------------------------

/*
 ��������� EXCEPTION_INIT
 ��������� �������������, ����������� � �������� ������
EXCEPTION, � ���������� ����� ������.


*/


----------------------------------------------------------------

/*
 OR ����� ��������� ������ ���������� � ����� �����


*/

declare
 var_name varchar2(200);
 var_id number := 145415212;
begin 
    select cl.name_v
        into var_name
        from tb_client_new  cl 
        where cl.id =  var_id or cl.id =  4545454;
          dbms_output.put_line ('var_name = ' || var_name);
exception when NO_DATA_FOUND or TOO_MANY_ROWS then 
            dbms_output.put_line ('NO_DATA_FOUND ��� TOO_MANY_ROWS');
   
end;

 
-- ���� OR  
-- �����:  var_name = ������
-- ���� var_id number := ��������������� id 
-- �� �����:  NO_DATA_FOUND ��� TOO_MANY_ROWS
 

---------------------------------------------------------------
-- �������� RAISE_APPLICATION_ERROR 

-- ������ ������� � output ���� ������, ������������ �� ��� ������

declare 

begin
  
  if (1 = 1) then 
     raise_application_error (-20000 , '���� ������');
  
  end if;
  
  exception 
    when others then 
      if (sqlcode = -20000) then 
        dbms_output.put_line (sqlerrm);
        end if;
  
end;
-- �����: ORA-20000: ���� ������



----------------------------------------------------------------
-- ������ ������� � output ���� ������, ������������ �� ����� ������

declare 

begin
  
  if (1 = 1) then 
     raise_application_error (-20000 , '���� ������');
  
  end if;
  
  exception 
    when others then 
      if (sqlerrm = 'ORA-20000: ���� ������') then -- sqlerrm ���� ��������� ���������.
        dbms_output.put_line ('��� �� ������, ������� �� ����������');
        end if;
  
end;
-- �����: ��� �� ������, ������� �� ����������


-----------------------------------------------------------------------------

--������� ����� instr ��������� ������� � if
declare 

begin
  
  if (1 = 1) then 
     raise_application_error (-20000 , '���� ������');
  
  end if;
  
  exception 
    when others then 
      if (instr ( sqlerrm, '���� ������') > 0) then  
        dbms_output.put_line ('��� �� ������, ������� �� ����������');
        end if;
  
end;
--( sqlerrm, '���� ������') > 0) ����� ������� ���� ������ ���� ��������������, 
-- �� ����� ����� ������ 0, � ����� ���� if ��������. ������� ������ 0 ����� ���������� 
-- � ����� ����� � ������ ������ �  sqlerrm ���������� ���� ��������������
 


-----------------------------------------------------------------------------

/*
������: �������� ����� ���������� ������� � ���������� � ������� ����� ����������.
��������
- ���� ������ �� ����������, �� ������ � exceptione
- ���� ������ ����������, �� ��� ������, �� �������� ���� � nvl � �����:
   '�� ������ ������ � id ___ '
- ���� ������ ����������, �� �������� ��� ����� � �����:
  '������ ��������� � ������: ___'
 
��� ����: 
� ��������  � id (61, 100) ���� ������
� ������� � id (4) ����� ������ ������. ��� ��� ��������, �� �������.

*/ 


-- ����� ������������� ��� ���� �������, � �. ����� �������
   select cl.*, ad.*
          --nvl(ad.city_v, '0') 
          -- ���� 1-� ������� ������, ��� �������� 0
          -- ���� ������ �������� ����, �� �� � ��������
          from tb_client cl
          left join tb_address ad on ad.client_id = cl.id
          where cl.id = 4;
          
declare 
   var_id number := 61;
   var_city_v varchar2(500);
-- to_many_addres exception;

begin
   
   select nvl(ad.city_v, '0')
          into var_city_v 
          from tb_client cl
          left join tb_address ad on ad.client_id = cl.id
          where cl.id = var_id;
  
  if (var_city_v = '0') then 
     raise_application_error (-20200 , '�� ������ ����� � ������� � id ' || var_id );
  -- ���� select ������ '0', �� � ���� var_city_v = 0, ����� � ��� ���� ���� ����� ������
  else 
    dbms_output.put_line('������ ��������� � ������: ' || var_city_v );
  end if;
  
  exception
     
     when NO_DATA_FOUND then
       dbms_output.put_line( '������ �� ������ � ��');
     when others then 
       if ( sqlcode = -20200) then  
        dbms_output.put_line ( sqlerrm );
       else  dbms_output.put_line ( '���� ������' );
       end if;
  
end;
 
-- exception when TOO_MANY_ROWS then raise  to_many_addres;
--when to_many_addres then dbms_output.put_line 
--         ('����� �� ���������� ID � ������� ����� 1 ������ �������');   


-----------------------------------------------------------------------------





-- �������� ������ 4 ����  exception   ����������  






----------------------------------------------------------------

/* 
 1. �������� 2 ����������� ���������� ��� ������ �������
  �� ��� (NO_DATA_FOUND, TOO_MANY_ROWS), � ������ output ������� 
���������� �������� ������ (�������� ��� ������������). 
��� ��������� ���������� �������� ��� "�������������� ������"

*/
 select * from tb_client_new;
select rowid, cl.* from tb_client_new cl

declare
 var_name varchar2(200);
 var_inn char(14) := '20012199500700' ; --22012199500500 --22012199500700

begin 
  dbms_output.put_line ('����� begin');
    select cl.name_v
        into var_name
        from tb_client_new  cl 
        where cl.inn_c =  var_inn;
        
          
exception 
  when NO_DATA_FOUND
          then dbms_output.put_line ('������� � ���: ' || var_inn || ' ��� � ����. 
               ������� ������ ��� �������');
  when TOO_MANY_ROWS
          then dbms_output.put_line ('�� ����������  ���: ' || var_inn || '   � ����  
               ��������� �������.
               �������� ������ �� ������ ������ ������� �� ����� ���');  
  when others then dbms_output.put_line ('�������������� ������');
   
end;

/*
 2. ��� ������ ������� �� ������� (where name like '%������%') 
 �������� ���������� �������� ����� 1� ������ � ������� �������
  RAISE (��������� ��� ���������� � ���������� � ������ exception).
*/
select * from tb_client_new;
select rowid, cl.* from tb_client_new cl;

select * 
       from tb_client_new cl
       where cl.name_v like '%�%';

declare
       var_name varchar2(200);
       to_many_name exception; 

begin
  dbms_output.put_line ('����� ��������� begin'); 
  
  declare
  
  
  begin 
    dbms_output.put_line ('����� ���������� begin ');
      select cl.name_v
          into var_name
          from tb_client_new  cl 
          where cl.name_v like '%�%';
      exception when TOO_MANY_ROWS then raise  to_many_name;
  end;
  
exception
   when to_many_name then dbms_output.put_line 
         ('����� �� ��������� ������ ������ ����� 1 ������');        
end;

/*
3. ��� ������� ���������� �� �������, �������� ������ ����� ��� ������ ���� 
(���������� � ��� = null), � ������� ������� RAISE_APPLICATION_ERROR ��������� 
����������, �������� ��� � ������ Exception � ���������� �������� 
��� ������������ �������� ������ � output
*/


select * from tb_client_new;
select rowid, cl.* from tb_client_new cl;


-- ������� 1 �� ������ 
declare
  new_inn  char(14); -- := '22012144400500' ;
begin
  dbms_output.put_line ('����� ��������� begin'); 
  
  declare
  begin
      dbms_output.put_line ('����� ���������� begin ');
      insert into tb_client_new (inn_c) values (new_inn);
  end;    
  
    
exception
  when others then
    raise_application_error (-20100,'������� ����� �������� ��������');
end;     
 
-- � ���� ������� �� ������� inn  �������� ���������� � ���� not_null.
-- �� � ���������, ��� ������� ������� �������� �������� �� ���� ������� ������ � out_put 
 -- ���� ������� ������ ������ �� ����� �������� 


-- �� ���� � ������� 2 


declare 

begin
  
  if (1 = 1) then 
     raise_application_error (-20000 , '���� ������');
  
  end if;
  
  exception 
    when others then 
      if (sqlcode = -20000) then 
        dbms_output.put_line (sqlerrm);
        end if;
  
end;
-- �����: ORA-20000: ���� ������

-----------------------------------------------------------------------------------

-- ������� 2 ������

select * from tb_client_new;
select rowid, cl.* from tb_client_new cl;

declare

  new_inn  char(14); -- := '22012144400500' ;

begin
 
    if (new_inn is  null) then
      dbms_output.put_line ('�����   if'); 
      
      raise_application_error (-20100,'������� ����� �������� ��������');
    
    else
      dbms_output.put_line ('�����   else'); 
      insert into tb_client_new (inn_c) values (new_inn);
    
    
    end if;
 
      
exception
   when others then 
    if (sqlcode = -20100) then 
       dbms_output.put_line (sqlerrm);
       end if;
 
end;     

-----------------------------------------------------------------------------------



















