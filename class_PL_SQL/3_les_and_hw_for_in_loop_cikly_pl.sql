

----------------------------------------------------------------

-- �������� ������ 1 ���� 


-- ����� for... in   ;   ��������� ����� for .. in () ; 
-- ���� for in �������� ���� ���� ������ 
-- while �������� ���� ������ � ������ ����-�� ��������� ( �������� ���� id > 10) 
----------------------------------------------------------------

/* ������: ������� ����� �� 1 �� 20 � output � ������� I = 1 � �.�. */

declare

begin 
 
   for i in 1 .. 20
     loop
       dbms_output.put_line ('i = ' || i);
     end loop;  
end; 
 
/* �����: i = 1  i = 2   i = 3  i = 4   i = 5
i = 6
i = 7
i = 8
i = 9
i = 10
i = 11
i = 12
i = 13
i = 14
i = 15
i = 16
i = 17
i = 18
i = 19
i = 20    */

/*
����� ��������� ����� �� ������� ������, ���� �������� � ���������� ������ 
� ������ tb � �� ������������. 

������: ������� ��� - N, �� - ///    
����� �� �� �������� tb empl �������� ������ � �������� tb_project_employees
� ����� empl - ��� ������, ������� ��������� �� ������ ����� ������� 
*/

declare
begin 
  for empl in (
    select  e.name_v, e.salary 
    from tb_project_employees e 
    )
    loop
      dbms_output.put_line ('name = ' || empl.name_v);
      dbms_output.put_line ('salary = ' || empl.salary);
      dbms_output.put_line ('--------------------------------');
    end loop;

end; 
select   e.* from tb_project_employees e;

/* �����:
name = Win
salary = 11111
--------------------------------
name = Ulan
salary = 7200.123
--------------------------------
*/

-----------------------------------------------------------------------------------------
declare
       date_now date := to_date('15.08.2021', 'dd.mm.yyyy');
begin 
       while (date_now < to_date('01.09.2021', 'dd.mm.yyyy'))
-- ���� date_now ������ ��� 1 ���� 2021, �������� ��� ����
         loop 
           dbms_output.put_line ('date_now = ' || date_now);
-- ��� ������ ���������� ������� 15 ��� 2021, � ����� ����� ���������� 1
           date_now := date_now + 1;
         end loop;
         
   end;
  
/* �����:
date_now = 15-AUG-21
date_now = 16-AUG-21
....
date_now = 30-AUG-21
date_now = 31-AUG-21    */


----------------------------------------------------------------------------------------
/* �� ����� ����: ���� �������� ���������� empl (���������� ������ ������� empl )
� �������  empl �� ������ - �� ��������� ����� ������ ����������, � �� �������, 
������ ��� �� �������� �� ������

*/

declare

empl varchar2(200) := '�������� ��������'; 

begin 
  for empl in (
    select  e.name_v, e.salary 
    from tb_project_employees e 
    )
    loop
      dbms_output.put_line ('name = ' || empl.name_v);
      dbms_output.put_line ('salary = ' || empl.salary);
      dbms_output.put_line ('--------------------------------');
    end loop;
    
    dbms_output.put_line('empl  = ' || empl);

end; 

 
/* �����:
 ....
--------------------------------
name = Tom
salary = 5213
--------------------------------
empl  = �������� ��������

*/

----------------------------------------------------------------------------------------
/*���� ������ ���� '12321312321321434234'

1. ������� � ������ output ������ ������ �� �����������,
����� ���������� ���:

1
2
3
2
*/

--substr
--length
select substr('12321312321321434234', 1, 1) as str,
       length('12321312321321434234') as str_len
  from dual;

-- ������� 

declare 
   str varchar2(200) := '12321312321321434234';
   n number := 1; 
   length_str number; 
   

begin
  length_str := length (str);
  
  loop 
     dbms_output.put_line(substr(str, n, 1));
     exit when n = length_str;
     n := n + 1; 
     
     end loop;   

end;

/*
�����:
1
2
3
2
1
...      */


-- ����������� �� ������ substr
select substr('����', 1, 1) from dual
--����� � 

select substr('����', 2, 1) from dual
--����� �

select substr('����', 3, 1) from dual
--����� �  

/********************************************************
������ � �������������� ��������:
���� ������ ���� '1235213123521321434234'
 ������� ���� � �������� ����� � ������. 
����������� ������ �� ���� �������� ������ (���������� �� � out_put ��� � ������)
� � ������, ���� ���������� ������ �5� - (!) ����� �����, ������� ����������:
� ������ ���������� ������ 5. 
����� � ����� ������� ���������� ����� 5, ������� �����������

*/


declare
       str varchar2(200) := '1232131253213521434234';
       n number := 1;
       length_str number;
       result_str char(1) := '';
       count5 number := 0;
begin
       length_str := length(str);
       
       loop
                result_str := substr(str, n, 1);
                if (result_str = '5') then
                   dbms_output.put_line('������ � ��������!!!');
                   count5 := count5 + 1;
                end if;
               
                dbms_output.put_line(result_str);
                exit when n = length_str;
                n := n + 1;
       end loop;

     dbms_output.put_line('count5 = ' || count5);
       if (count5 is not null) then
         dbms_output.put_line('���������� �������� "5": ' || count5);
       end if;
end;

/*  �����:

1
...
������ � ��������!!!
5
3
...
������ � ��������!!!
5
...
4
count5 = 2
���������� �������� "5": 2

*/





----------------------------------------------------------------


-- �������� ������ 3 ���� 



----------------------------------------------------------------

/* 
�������� 10 �������� 


� ����� (�� ��������� �������) ������� ����� ���� �������� � ����:


��� ������� �1: ������ ���� ��������
��� ������� �2: ������ ���� ��������
��� ������� �3: ������� ����� ���������
....

�� ������������ ������: ������� ���� ����� ��������� �������
*/

-- ������� ����� ������ 

select cl.* from tb_client cl; 

  
declare
   n number := 1; 
begin 
  for client in (
    select  cl.first_name_v, cl.second_name_v, cl.surname_v
    from tb_client cl
    )
    loop
      dbms_output.put_line ('��� ������� �' || n || ': ' || client.second_name_v
      || ' ' ||client.first_name_v || ' ' ||client.surname_v  );
      n := n +1; 
      
    end loop;

end; 

/* �����:
�������! :) 

��� ������� �1: ��������� ����� ����������
��� ������� �2: ��������� ����� �����������
��� ������� �3: ������� ������� ����������

*/




/* ������� �� �������:
      � ������� �� ������ �������� "������� �� ��������� �������".
      ��������������� ������� ����������� ����� "loop ... end loop" 
      ������ ������� �� ������ ��� �� ��������� ������ �� ��... 
      ���������� ������������ � �������� ����� ������ ��� 
      ����� ���������� ������� � � ������� ������ �� �����.*/

-- ������� ��� ������� �� ������ 

declare
  i number:=1;
  --���������� ��� �������� ���������� ����� � �������
  number_rows number;
  name_cl varchar2(100);

begin
  select count(name_v)
    into number_rows 
    from tb_client_pl cl;
  
--������ ������� �� i-��� ������,��������� ���������� � ���������� name_cl
   loop
      select distinct 
        '��� ������� �'||i||' : '|| last_value(name_v) over()
        into name_cl
        from tb_client
        where ROWNUM < =i;
      dbms_output.put_line(name_cl);
      i:=i+1;
      exit when i > number_rows; 
   end loop;
end;






