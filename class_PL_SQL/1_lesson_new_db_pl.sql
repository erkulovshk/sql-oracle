-- ���� ������ ��� ������ � ����������� 

/* ������ �������� 
   tg_tb_pl_document_bi
�  
   TG_TB_TARIF_BI_PL
   
*/


select * from tb_country_pl  for update   ;

select rowid, c.* from tb_tarif_pl c;
 
insert into tb_country_pl (id, name_v) values (null, '����������');
insert into tb_country_pl (id, name_v) values (null, '���������');
insert into tb_country_pl (id, name_v) values (null, '������');

insert into tb_client_pl values (
       1,
       '������ ���� ��������',
       to_date('01.01.2000','dd.mm.yyyy'),
       1,
       'M',
       '222052992001111',
       null,
       '�������',
       '1234@gmail.com',
       sysdate,
       sysdate
);

insert into tb_client_pl values (
       2,
       '������ ���� ��������',
       to_date('01.01.2000','dd.mm.yyyy'),
       1,
       'M',
       '222052992001111',
       null,
       '�������',
       '1234@gmail.com',
       sysdate,
       sysdate
);

insert into tb_client_pl values (
       3,
       '������� ����� ���������',
       to_date('01.01.2000','dd.mm.yyyy'),
       1,
       'M',
       '222052992001111',
       null,
       '�������',
       '1234@gmail.com',
       sysdate,
       sysdate
);

insert into tb_client_pl values (
       4,
       '������ ����� ��������',
       to_date('01.01.2000','dd.mm.yyyy'),
       1,
       'M',
       '222052992001111',
       null,
       '�������',
       '1234@gmail.com',
       sysdate,
       sysdate
);

select rowid, c.* from tb_tarif_pl c;

insert into tb_tarif_pl values (1, '����� � ����. ������', 1, 500, 30, 1,5,10);
insert into tb_tarif_pl values (2, '����� �� �����', 0, 500, 30, 2, 10, 20);


insert into tb_contract_pl (id,
                            client_id,
                            imsi_n,
                            phone_number_n,
                            tarif_id,
                            balance_n,
                            state_id)
values (1, 1, 12234356345541332423, '+996555123456', 1, 1050, 1);

insert into tb_contract_pl (id,
                            client_id,
                            imsi_n,
                            phone_number_n,
                            tarif_id,
                            balance_n,
                            state_id)
values (2, 2, 12231343562452341332423, '+996555123457', 1, 200, 1);

insert into tb_contract_pl (id,
                            client_id,
                            imsi_n,
                            phone_number_n,
                            tarif_id,
                            balance_n,
                            state_id)
values (3, 3, 122313325612111332423, '+996555123458', 1, 111, 1);

insert into tb_contract_pl (id,
                            client_id,
                            imsi_n,
                            phone_number_n,
                            tarif_id,
                            balance_n,
                            state_id)
values (4, 4, 12231324122541332423, '+996555123459', 1, 222, 1);


insert into tb_contract_pl (id,
                            client_id,
                            imsi_n,
                            phone_number_n,
                            tarif_id,
                            balance_n,
                            state_id)
values (5, 3, 12231324122541332423, '+996555123450', 1, 333, 1);

insert into tb_operation_type_pl (id, name_v) values (null, '�������� �������');
insert into tb_operation_type_pl (id, name_v) values (null, '���������� �������');
insert into tb_operation_type_pl (id, name_v) values (null, '�������� �������');
insert into tb_operation_type_pl (id, name_v) values (null, '�������� ���������');



-- �������� ������ ��� ������ PL/SQL

ALTER TABLE tb_client_pl DROP CONSTRAINT FK_tb_client_tb_country;
ALTER TABLE tb_client_pl DROP CONSTRAINT FK_tb_client_tb_document;
ALTER TABLE tb_address_pl DROP CONSTRAINT FK_tb_address__address_type;
ALTER TABLE tb_address_pl DROP CONSTRAINT FK_tb_address_tb_country;
ALTER TABLE tb_address_pl DROP CONSTRAINT FK_tb_address_tb_client;
ALTER TABLE tb_contract_pl DROP CONSTRAINT FK_tb_contract_tb_client;
ALTER TABLE tb_contract_pl DROP CONSTRAINT FK_tb_contract_tb_tarif;
ALTER TABLE tb_document_pl DROP CONSTRAINT FK_tb_document_document_type;
ALTER TABLE tb_operation_pl DROP CONSTRAINT FK_tb_operation_tb_contract;
ALTER TABLE tb_operation_pl DROP CONSTRAINT FK_tb_operation_peration_type;
ALTER TABLE tb_client_history_pl DROP CONSTRAINT FK_lient_history_tb_client;
DROP TABLE tb_client_pl PURGE;
DROP SEQUENCE SQ_tb_client;
DROP TABLE tb_address_pl PURGE;
DROP SEQUENCE SQ_tb_address;
DROP TABLE tb_contract_pl PURGE;
DROP SEQUENCE SQ_tb_contract;
DROP TABLE tb_country_pl PURGE;
DROP SEQUENCE SQ_tb_country;
DROP TABLE tb_document_pl PURGE;
DROP SEQUENCE SQ_tb_document;
DROP TABLE tb_operation_pl PURGE;
DROP SEQUENCE SQ_tb_operation;
DROP TABLE tb_document_type_pl PURGE;
DROP SEQUENCE SQ_tb_document_type;
DROP TABLE tb_address_type_pl PURGE;
DROP SEQUENCE SQ_tb_address_type;
DROP TABLE tb_client_history_pl PURGE;
DROP SEQUENCE SQ_tb_client_history;
DROP TABLE tb_tarif_pl PURGE;
DROP SEQUENCE SQ_tb_tarif;
DROP TABLE tb_operation_type_pl PURGE;
DROP SEQUENCE SQ_tb_operation_type;


-------------------------------------------------------------------------------
--            tb_client_pl
-------------------------------------------------------------------------------

CREATE TABLE tb_client_pl (
    id                              NUMBER
  , name_v                          VARCHAR2(200)
  , birthdate_d                     DATE
  , birth_place_id                  NUMBER
  , sex_c                           CHAR(1)
  , inn_v                           VARCHAR2(200)
  , documet_id                      NUMBER
  , occupation_v                    VARCHAR2(200)
  , email_v                         VARCHAR2(200)
  , create_date_d                   DATE
  , update_date_d                   DATE
  , CONSTRAINT PK_tb_client_pl PRIMARY KEY ( id )
);


CREATE SEQUENCE SQ_tb_client_pl;

CREATE OR REPLACE TRIGGER TG_tb_client_BI
    BEFORE INSERT ON tb_client_pl
    FOR EACH ROW
BEGIN
    if :NEW.id is NULL then
        :NEW.id := SQ_tb_client_pl.nextVal;
    end if;
END;




-------------------------------------------------------------------------------
--            tb_address_pl
-------------------------------------------------------------------------------

CREATE TABLE tb_address_pl (
    id                              NUMBER
  , type_id                         NUMBER
  , country_id                      NUMBER
  , city_v                          VARCHAR2(200)
  , street_v                        NUMBER
  , house_v                         VARCHAR2(200)
  , flat_v                          VARCHAR2(200)
  , client_id                       NUMBER
  , CONSTRAINT PK_tb_address_pl PRIMARY KEY ( id )
);


CREATE SEQUENCE SQ_tb_address_pl;

CREATE OR REPLACE TRIGGER TG_tb_address_BI_pl
    BEFORE INSERT ON tb_address_pl
    FOR EACH ROW
BEGIN
    if :NEW.id is NULL then
        :NEW.id := SQ_tb_address_pl.nextVal;
    end if;
END;




-------------------------------------------------------------------------------
--            tb_contract_pl
-------------------------------------------------------------------------------
select * from tb_contract_pl;

CREATE TABLE tb_contract_pl (
    id                              NUMBER
  , client_id                       NUMBER
  , imsi_n                          NUMBER
  , phone_number_n                  VARCHAR2(200)
  , tarif_id                        NUMBER
  , balance_n                       NUMBER
  , state_id                        NUMBER
  , CONSTRAINT PK_tb_contract_pl PRIMARY KEY ( id )
);


-- CREATE SEQUENCE SQ_tb_contract_pl;

CREATE OR REPLACE TRIGGER TG_tb_contract_BI_pl
    BEFORE INSERT ON tb_contract_pl
    FOR EACH ROW
BEGIN
    if :NEW.id is NULL then
        :NEW.id := SQ_tb_contract_pl.nextVal;
    end if;
END;




-------------------------------------------------------------------------------
--            tb_country_pl
-------------------------------------------------------------------------------

CREATE TABLE tb_country_pl (
    id                              NUMBER
  , name_v                          VARCHAR2(200)
  , CONSTRAINT PK_tb_country_pl PRIMARY KEY ( id )
);


CREATE SEQUENCE SQ_tb_country_pl;

CREATE OR REPLACE TRIGGER TG_tb_country_BI_pl
    BEFORE INSERT ON tb_country_pl
    FOR EACH ROW
BEGIN
    if :NEW.id is NULL then
        :NEW.id := SQ_tb_country_pl.nextVal;
    end if;
END;




-------------------------------------------------------------------------------
--            tb_document_pl
-------------------------------------------------------------------------------

CREATE TABLE tb_document_pl (
    id                              NUMBER
  , type_id                         NUMBER
  , seria_v                         VARCHAR2(200)
  , number_n                        INT
  , start_date_d                    DATE
  , end_date_d                      DATE
  , CONSTRAINT PK_tb_document_pl PRIMARY KEY ( id )
);


CREATE SEQUENCE SQ_tb_document_pl;

CREATE OR REPLACE TRIGGER TG_tb_document_BI_pl
    BEFORE INSERT ON tb_document_pl
    FOR EACH ROW
BEGIN
    if :NEW.id is NULL then
        :NEW.id := SQ_tb_document_pl.nextVal;
    end if;
END;




-------------------------------------------------------------------------------
--            tb_operation_pl
-------------------------------------------------------------------------------

CREATE TABLE tb_operation_pl (
    id                              NUMBER
  , contract_id                     NUMBER
  , type_id                         NUMBER
  , description_v                   VARCHAR2(200)
  , amount_n                        NUMBER
  , CONSTRAINT PK_tb_operation_pl PRIMARY KEY ( id )
);


CREATE SEQUENCE SQ_tb_operation_pl;

CREATE OR REPLACE TRIGGER TG_tb_operation_BI_pl
    BEFORE INSERT ON tb_operation_pl
    FOR EACH ROW
BEGIN
    if :NEW.id is NULL then
        :NEW.id := SQ_tb_operation_pl.nextVal;
    end if;
END;




-------------------------------------------------------------------------------
--            tb_document_type_pl
-------------------------------------------------------------------------------

CREATE TABLE tb_document_type_pl (
    id                              NUMBER
  , name_v                          VARCHAR2(200)
  , CONSTRAINT PK_tb_document_type_pl PRIMARY KEY ( id )
);


CREATE SEQUENCE SQ_tb_document_type_pl;

CREATE OR REPLACE TRIGGER TG_tb_document_type_BI_pl
    BEFORE INSERT ON tb_document_type_pl
    FOR EACH ROW
BEGIN
    if :NEW.id is NULL then
        :NEW.id := SQ_tb_document_type_pl.nextVal;
    end if;
END;




-------------------------------------------------------------------------------
--            tb_address_type_pl
-------------------------------------------------------------------------------

CREATE TABLE tb_address_type_pl (
    id                              NUMBER
  , name_v                          NUMBER
  , CONSTRAINT PK_tb_address_type_pl PRIMARY KEY ( id )
);


CREATE SEQUENCE SQ_tb_address_type_pl;

CREATE OR REPLACE TRIGGER TG_tb_address_type_BI_pl
    BEFORE INSERT ON tb_address_type_pl
    FOR EACH ROW
BEGIN
    if :NEW.id is NULL then
        :NEW.id := SQ_tb_address_type_pl.nextVal;
    end if;
END;




-------------------------------------------------------------------------------
--            tb_client_history_pl
-------------------------------------------------------------------------------

/*CREATE TABLE tb_client_history_pl (
    id                              NUMBER
  , client_id                       NUMBER
  , date_d                          DATE
  , changes_v                       VARCHAR2(200)
  , CONSTRAINT PK_tb_client_history PRIMARY KEY ( id )
);


CREATE SEQUENCE SQ_tb_client_history;

CREATE OR REPLACE TRIGGER TG_tb_client_history_BI
    BEFORE INSERT ON tb_client_history_pl
    FOR EACH ROW
BEGIN
    if :NEW.id is NULL then
        :NEW.id := SQ_tb_client_history.nextVal;
    end if;
END;*/




-------------------------------------------------------------------------------
--            tb_tarif_pl
-------------------------------------------------------------------------------

CREATE TABLE tb_tarif_pl (
    id                              NUMBER
  , name                            VARCHAR2(200)
  , is_subscription_fee             CHAR(1)
  , subscription_fee_value          NUMBER
  , subscription_fee_days           NUMBER
  , minute_price                    NUMBER
  , sms_price                       NUMBER
  , mb_price                        NUMBER
  , CONSTRAINT PK_tb_tarif_pl PRIMARY KEY ( id )
);


CREATE SEQUENCE SQ_tb_tarif_pl;

CREATE OR REPLACE TRIGGER TG_tb_tarif_BI_pl
    BEFORE INSERT ON tb_tarif_pl
    FOR EACH ROW
BEGIN
    if :NEW.id is NULL then
        :NEW.id := SQ_tb_tarif_pl.nextVal;
    end if;
END;




-------------------------------------------------------------------------------
--            tb_operation_type_pl
-------------------------------------------------------------------------------

CREATE TABLE tb_operation_type_pl (
    id                              NUMBER
  , name_v                          VARCHAR2(200)
  , CONSTRAINT PK_tb_operation_type_pl PRIMARY KEY ( id )
);


CREATE SEQUENCE SQ_tb_operation_type_pl;

CREATE OR REPLACE TRIGGER TG_tb_operation_type_BI_pl
    BEFORE INSERT ON tb_operation_type_pl
    FOR EACH ROW
BEGIN
    if :NEW.id is NULL then
        :NEW.id := SQ_tb_operation_type_pl.nextVal;
    end if;
END;




-------------------------------------------------------------------------------

ALTER TABLE tb_client_pl ADD CONSTRAINT FK_tb_client_tb_country_pl FOREIGN KEY ( birth_place_id ) REFERENCES tb_country_pl ( id );
ALTER TABLE tb_client_pl ADD CONSTRAINT FK_tb_client_tb_document_pl FOREIGN KEY ( documet_id ) REFERENCES tb_document_pl ( id );
ALTER TABLE tb_address_pl ADD CONSTRAINT FK_tb_address__address_type_pl FOREIGN KEY ( type_id ) REFERENCES tb_address_type_pl ( id );
ALTER TABLE tb_address_pl ADD CONSTRAINT FK_tb_address_tb_country_pl FOREIGN KEY ( country_id ) REFERENCES tb_country_pl ( id );
ALTER TABLE tb_address_pl ADD CONSTRAINT FK_tb_address_tb_client_pl FOREIGN KEY ( client_id ) REFERENCES tb_client_pl ( id );
ALTER TABLE tb_contract_pl ADD CONSTRAINT FK_tb_contract_tb_client_pl FOREIGN KEY ( client_id ) REFERENCES tb_client_pl ( id );
ALTER TABLE tb_contract_pl ADD CONSTRAINT FK_tb_contract_tb_tarif_pl FOREIGN KEY ( tarif_id ) REFERENCES tb_tarif_pl ( id );
ALTER TABLE tb_document_pl ADD CONSTRAINT FK_tb_doc_doc_type_pl FOREIGN KEY ( type_id ) REFERENCES tb_document_type_pl ( id );
ALTER TABLE tb_operation_pl ADD CONSTRAINT FK_tb_operation_tb_contract_pl FOREIGN KEY ( contract_id ) REFERENCES tb_contract_pl ( id );
ALTER TABLE tb_operation_pl ADD CONSTRAINT FK_tb_op_peration_type_pl FOREIGN KEY ( type_id ) REFERENCES tb_operation_type_pl ( id );
--ALTER TABLE tb_client_history_pl ADD CONSTRAINT FK_lient_history_tb_client_pl FOREIGN KEY ( client_id ) REFERENCES tb_client_pl ( id );




 



--   �� ������ ������� 
-------------------------------------------------------------------------------

-- ������ �������� ������� �������, ����� ������� 

/*
alter table tb_client drop constraint fk_tb_client_tb_country;
alter table tb_client drop constraint fk_tb_client_tb_document;
alter table tb_document drop constraint fk_tb_document_document_type;
alter table tb_address drop constraint fk_tb_address__address_type;
SELECT * FROM  tb_address drop constraint fk_tb_address_tb_country;
alter table tb_address drop constraint fk_tb_address_tb_client;
alter table tb_client_history drop constraint fk_lient_history_tb_client;
SELECT * FROM tb_client purge;
drop sequence sq_tb_client;
SELECT * FROM  tb_country purge;
drop sequence sq_tb_country;
drop table tb_document purge;
drop sequence sq_tb_document;
drop table tb_document_type purge;
drop sequence sq_tb_document_type;
SELECT * FROM tb_address purge;
drop sequence sq_tb_address;
SELECT * FROM tb_address_type purge;
drop sequence sq_tb_address_type;
drop table tb_client_history purge;
drop sequence sq_tb_client_history;
-- */



--   �� ������ ������� 
-------------------------------------------------------------------------------
--            tb_pl_client
-------------------------------------------------------------------------------



create table tb_pl_client (
    id                              number,
    name_v                          varchar2(500),
    birthdate_d                     date,
    birth_place_id                  number,
   sex_c                           varchar2(500),
   inn_v                           varchar2(500),
   documet_id                      number,
   occupation_v                    varchar2(500),
   email_v                         varchar2(500),
   create_date_d                   date,
   update_date_d                   date,
   constraint pk_tb_pl_client primary key ( id )
);

select cl.* from tb_pl_client cl; 

create sequence sq_tb_pl_client;

create or replace trigger tg_tb_pl_client_bi
    before insert on tb_pl_client
    for each row
begin
    if :new.id is null then
        :new.id := sq_tb_pl_client.nextval;
    end if;
end;


show errors;

-------------------------------------------------------------------------------
--            tb_pl_country
-------------------------------------------------------------------------------

create table tb_pl_country (
    id                              number
  , name_v                          varchar2(500)
  , constraint pk_tb_pl_country primary key ( id )
);


create sequence sq_tb_pl_country;

create or replace trigger tg_tb_pl_country_bi
    before insert on tb_pl_country
    for each row
begin
    if :new.id is null then
        :new.id := sq_tb_pl_country.nextval;
    end if;
end;
 

show errors;

-------------------------------------------------------------------------------
--            tb_pl_document
-------------------------------------------------------------------------------

create table tb_pl_document (
    id                              number
  , type_id                         number
  , seria_v                         varchar2(500)
  , number_n                        int
  , start_date_d                    date
  , end_date_d                      date
  , constraint pk_tb_pl_document primary key ( id )
);


create sequence sq_tb_pl_document;

create or replace trigger tg_tb_pl_document_bi
    before insert on tb_pl_document
    for each row
begin
    if :new.id is null then
        :new.id := sq_tb_pl_document.nextval;
    end if;
end;


show errors;

-------------------------------------------------------------------------------
--            tb_pl_document_type
-------------------------------------------------------------------------------

create table tb_pl_document_type (
    id                              number
  , name_v                          varchar2(500)
  , constraint pk_tb_pl_document_type primary key ( id )
);


create sequence sq_tb_pl_document_type;

create or replace trigger tg_tb_pl_document_type_bi
    before insert on tb_pl_document_type
    for each row
begin
    if :new.id is null then
        :new.id := sq_tb_pl_document_type.nextval;
    end if;
end;


show errors;

-------------------------------------------------------------------------------
--            tb_pl_address
-------------------------------------------------------------------------------

create table tb_pl_address (
    id                              number
  , type_id                         number
  , country_id                      number
  , city_v                          varchar2(500)
  , street_v                        number
  , house_v                         varchar2(500)
  , flat_v                          varchar2(500)
  , client_id                       number
  , constraint pk_tb_pl_address primary key ( id )
);


create sequence sq_tb_pl_address;

create or replace trigger tg_tb_pl_address_bi
    before insert on tb_pl_address
    for each row
begin
    if :new.id is null then
        :new.id := sq_tb_pl_address.nextval;
    end if;
end;


show errors;

-------------------------------------------------------------------------------
--            tb_pl_address_type
-------------------------------------------------------------------------------

create table tb_pl_address_type (
    id                              number
  , name_v                          number
  , constraint pk_tb_pl_address_type primary key ( id )
);

create sequence sq_tb_pl_address_type;

create or replace trigger tg_tb_pl_address_type_bi
    before insert on tb_pl_address_type
    for each row
begin
    if :new.id is null then
        :new.id := sq_tb_pl_address_type.nextval;
    end if;
end;


show errors;

-------------------------------------------------------------------------------
--            tb_pl_client_history
-------------------------------------------------------------------------------

create table tb_pl_client_history (
    id                              number
  , client_id                       number
  , date_d                          date
  , changes_v                       varchar2(500)
  , constraint pk_tb_pl_client_history primary key ( id )
);


create sequence sq_tb_pl_client_history;

create or replace trigger tg_tb_pl_client_history_bi
    before insert on tb_pl_client_history
    for each row
begin
    if :new.id is null then
        :new.id := sq_tb_pl_client_history.nextval;
    end if;
end;


show errors;

-------------------------------------------------------------------------------

alter table tb_pl_client add constraint fk_tb_pl_client_tb_country foreign key ( birth_place_id ) references tb_pl_country ( id );
alter table tb_pl_client add constraint fk_tb_pl_client_tb_document foreign key ( documet_id ) references tb_pl_document ( id );
alter table tb_pl_document add constraint fk_tb_pl_document_document_type foreign key ( type_id ) references tb_pl_document_type ( id );
alter table tb_pl_address add constraint fk_tb_pl_address__address_type foreign key ( type_id ) references tb_pl_address_type ( id );
alter table tb_pl_address add constraint fk_tb_pl_address_tb_country foreign key ( country_id ) references tb_pl_country ( id );
alter table tb_pl_address add constraint fk_tb_pl_address_tb_client foreign key ( client_id ) references tb_pl_client ( id );
alter table tb_pl_client_history add constraint fk_pl_client_history_tb_client foreign key ( client_id ) references tb_pl_client ( id );
