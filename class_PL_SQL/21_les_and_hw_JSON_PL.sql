
----------------------------------------------------------------

-- �������� ������ 21 ����   JSON

-- ��� ��� ���� �� sys ����� � ���� ������ ��� system
grant execute on dbms_crypto to system;

----------------------------------------------------------------



CREATE TABLE json_documents (
  id    number,
  data  CLOB,
  CONSTRAINT json_documents_pk PRIMARY KEY (id),
  CONSTRAINT json_documents_json_chk CHECK (data IS JSON)
);


select *
from json_documents;

-- ��� ��� �� ���������, ������ ��� ����� JSON ������.
insert into json_documents values (1, 'wrong_text');



-- ��� ��� ��� ���������
insert into json_documents values (1, '{
    "clientInfo":{
        "inn": "2220519900000000",
        "name": "������ ���� ��������",
        "birthDate": "22.05.1990"
    }
}');

select *
from json_documents;

-- ��� �������� ������ ������ � JSON �������
-- �������� ����������� ��� �� ����� ���, � ����� ��� ������� ���������� � ���� �����
-- �������� �������, ������� data, ������ ������ clientInfo, ���� "inn", �������� ����� '2220519900000000'



-- ������ � ������

DECLARE
    -- ��������� ���������� � ���� ������� JSON
  l_obj JSON_OBJECT_T;

BEGIN
    -- ����� �� ����� ��������� ���������� �����-�� �������� ���� JSON
    -- ��� ����� ��� �������� ����� NEW ������������ � ���������� := NEW JSON_OBJECT_T();
  l_obj := JSON_OBJECT_T();

    --��� ������ ������ JSON ���� �������� � to_string � ��� ��� ��������
    -- �� ���� ���������� � ��� ������
  DBMS_OUTPUT.put_line('l_obj.TO_STRING = ' || l_obj.TO_STRING);

    -- ����� �� ����� � ���������� JSON �������� � ������ ����� ���������� l_obj
  l_obj := JSON_OBJECT_T('{ "employee_no":9999 }');
  DBMS_OUTPUT.put_line('l_obj.TO_STRING = ' || l_obj.TO_STRING);

END;


--  PUT   

declare
    l_obj json_object_t;
begin

-- ����� ��� ������ ����������� ������� � �������
    l_obj := json_object_t('{ "inn":"222051990111111"}');
    dbms_output.put_line('l_obj = ' || l_obj.to_string);

-- ����� PUT ��������� ������ ������� ��� ���� ���� (�������) � ������ NAME � ��������� ����
-- ����� ������ �����
-- �����! PUT ������ ��� �������� � �� ��������� ����������� ������
    l_obj.put('name', '����');
    dbms_output.put_line('l_obj = ' || l_obj.to_string);
-- ��� ���������� �� �� ������� � ����� �����, � � ����� ��� ���� "name":"����" �� �������
l_obj.put('name', '����');
    dbms_output.put_line('l_obj = ' || l_obj.to_string);


    end;



-- ��������


declare
    l_obj json_object_t;
begin

-- ����� ��� ������ ����������� ������� � �������
    l_obj := json_object_t('{ "inn":"222051990111111"}');
    dbms_output.put_line('l_obj = ' || l_obj.to_string);

-- ����� PUT ��������� ������ ������� ��� ���� ���� (�������) � ������ NAME � ��������� ����
-- ����� ������ �����
-- �����! PUT ������ ��� �������� � �� ��������� ����������� ������
    l_obj.put('name', '����');
    dbms_output.put_line('l_obj = ' || l_obj.to_string);

-- ��� ���������� �� �� ������� � ����� �����, � � ����� ��� ���� "name":"����" �� �������
    l_obj.put('name', '����');
    dbms_output.put_line('l_obj = ' || l_obj.to_string);

-- rename_key - ����� ������� �������� ����� 'name' �� 'client_name'
    l_obj.rename_key('name', 'client_name');
    dbms_output.put_line('rename_key(''name'', ''client_name'') l_obj = ' || l_obj.to_string);

-- ������ ������� "client_name":"����"  �� ��� ����� 'client_name'
    l_obj.remove('client_name');
    dbms_output.put_line('l_obj = ' || l_obj.to_string);


-- put -  �������� ����  'address', � ��� ��������� ����� ������ ������ JSON
-- � ����� �����
    l_obj.put('address', json_object_t('{"city":"������"}'));
    dbms_output.put_line('address l_obj = ' || l_obj.to_string);

-- ����� ����� remove ������ ������� ����� � ��� ���������
    l_obj.remove('address');
    dbms_output.put_line('l_obj = ' || l_obj.to_string);

-- ��������   ��������� ��������� ����� json_array_t  - ��� ������ ��������
-- ������ ��������, ��� � ���� ��� ����� ������� � ����� 'phone'
-- ���� ������ ������������, �� ������ ������� (��� ��� 1, ��� 2) ����� ���_1 � ���� �������
-- JSON ��������, ����� � ���� ����� ����� ���� ���� � ������� � �.�. � ����� ����.
    l_obj.put('phone', json_array_t('["+996558123456","+995555654321","+996555123456"]'));
    dbms_output.put_line(' ''phone'' l_obj = ' || l_obj.to_string);

-- put_null  �������� ��������, ������� ���� ��  ����� 'phone'
    l_obj.put_null('phone');
    dbms_output.put_line('l_obj = ' || l_obj.to_string);

-- l_obj.get ���������� �������� � ����� ��� � ������� l_obj
    DBMS_OUTPUT.PUT_LINE(l_obj.get('inn').to_string);

end;











----------------------------------------------------------------

 














----------------------------------------------------------------

-- �������� ������  21 ����   JSON

-- ��� ��� ���� �� sys ����� � ���� ������ ��� system
grant execute on dbms_crypto to system;

-------------------------------------------------------------------------------------
 
