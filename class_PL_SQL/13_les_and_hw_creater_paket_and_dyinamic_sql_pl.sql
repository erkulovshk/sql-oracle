 
----------------------------------------------------------------



-- �������� ������ 13  ����. ������������ SQL



------------------------------------------------------------------------------------- 


/* ������

� ����������� �� ���� ������������ (������������/���.
������/�����) � �������� ������ ����� �������� � ������� ��
��������.

������������ ����� ���, ���� ��������, ���.

���. ������: �� ���� ��� � ������������ + ������ ��������

����� �� ���� � ID 

+ 
���� �������� �������� ������� ��������� ���������� ������� � ������������� �������. 
*/

 
select * from tb_client_pl;
select * from tb_contract_pl;


---------------------------------------------------------------------------

--  ����� �� ������� ������� ������� ������. ������ ���� �� ����������� �� ���� ��������


---------------------------------------------------------------------------


-- ������ ����� ������ ���� �������� ��� ������������: ��� ����� � id �������. 

declare
 begin    
  
  show_client_info ('����') ;
 end;



----------------------------------------------------------------------


create or replace noneditionable procedure show_client_info (user_role varchar2)
is
  -- ������ ���������� ����(��� ���������), ����� �� ������������ � case.
       role_admin constant varchar2(50) := '�����';
       role_zav_otdel constant varchar2(50) := '���_������';
  
  
  -- ������ ������ client_info_list, � ������ ����� ������ ��� ��������� ����� ��� client_info_list_type,
  -- �. � ���� ������� �������� � ���� ������� � ���� ������ client_info_rec

       type client_info_rec is record (
            cl_name         varchar2(200),
            birthdate       date,
            inn             varchar2(16),
            country_name    varchar2(200),
            cl_id           number
            );

       type client_info_list_type is table of client_info_rec ;
       client_info_list  client_info_list_type;
       
       -- � ��� ������� ��� ������������ ������. � ������� ������� �� �������, ��� ��� 
       -- ����� �� ����� ����� ������ ������� ��� ����������� ��� ������ ���� 
        
       var_query varchar2(1000); 
       

begin
     -- ��������� � ���������� ������ � ���� ������  
       var_query :=
          ' select cl.name_v      as cl_name ' ||
          '        , cl.birthdate_d as birthdate ' ||
          '        , cl.inn_v       as inn ';
       
       case user_role
         when role_admin then
              var_query := var_query ||
                        '        ,c.name_v       as country_name ' ||
                        '        ,cl.id          as cl_id ';  
         when role_zav_otdel then
           var_query := var_query ||
                        '        ,c.name_v       as country_name ' ||
                        '        ,null          as cl_id ';
         else
           var_query := var_query ||
                        '        ,null          as country_name ' ||
                        '        ,null          as cl_id ';  
         end case;
         
          var_query := var_query ||
          '   from tb_client_pl cl ' ||
          '   join tb_country_pl c on c.id = cl.birth_place_id ';
    
   -- ������� ��������� ������ ����� ������������ SQL �
--      ��������� �������   ��������� � ����������-������ client_info_list
   EXECUTE IMMEDIATE var_query
   bulk collect into client_info_list ;



-- ������� �� ������� ������� ������� � ������ � ���� ������ �������

   for i in client_info_list.first .. client_info_list.last
     loop
       dbms_output.put_line
         ('������� :  ' || client_info_list(i).cl_name
          ||'. ���� ��������: ' || client_info_list(i).birthdate
          || '. ���: ' || client_info_list(i).inn
          || '. ������ ��������: ' || client_info_list(i).country_name
          || '. ID �������:  ' || client_info_list(i).cl_id
         );
          dbms_output.put_line ( '-------------------------------------');
     end loop;


end;

---------------------------------------------------------------------------
-- +  ���� �������� �������� ������� ��������� ���������� ������� � ������������� �������.
-- �������� ������� GET_COUNT_TB

create or replace function get_count_tb (

-- ������� ��������� ��� ������� � ������������ id ����� ����, ��� �������. ������ �������
       table_name varchar2,
       var_max_id out number
)

-- ���������� �����
return number

is

--�������� ���������� var_count, ������ �� � ����� � �����
       var_count number;

begin

-- ������������ ������ ������ max id � ������� ��� � var_count � var_max_id
      execute immediate
       'select count(*),
               max(id)
         from ' || table_name
       into var_count, var_max_id;


--   ������� ������ ����� �� ���������� var_count
       return var_count;

exception
  when others then return -1;
end;


 
----------------------------------------------------------------------

-- ����� ������� GET_COUNT_TB � ��������� �����. 

  
declare
-- � ������� ���� ������ �������� out, ������� ��������� max id, ��� ���, 
-- ���� ����_id ����������, �� �� ��� ������� ����� ������ ���������� � �������� ���������� ������������� ������� 
-- ������ ����� �������. � ����� ���� �� ������� ���� ����� id, �� ��� �������. 

  max_tb_id number;
  
begin    
  if (get_count_tb('tb_client_pl', max_tb_id) > 0) then
     
     dbms_output.put_line( max_tb_id);
 
  end if;
  
end;

---------------------------------------------------------------------------


create or replace procedure show_client_info (user_role varchar2)
is
       role_admin constant varchar2(50) := '�����';
       role_zav_otdel constant varchar2(50) := '���_������';

       type client_info_rec is record (
            cl_name         varchar2(200),
            birthdate       date,
            inn             varchar2(16),
            country_name    varchar2(200),
            cl_id           number
       );
       type client_info_list_type is table of client_info_rec;
       client_info_list client_info_list_type;
       
       var_query varchar2(1000);
begin
       var_query :=
          ' select cl.name_v      as cl_name ' ||
          '        , cl.birthdate_d as birthdate ' ||
          '        , cl.inn_v       as inn ';
       
       case user_role
         when role_admin then
              var_query := var_query ||
                        '        ,c.name_v       as country_name ' ||
                        '        ,cl.id          as cl_id ';  
         when role_zav_otdel then
           var_query := var_query ||
                        '        ,c.name_v       as country_name ' ||
                        '        ,null          as cl_id ';
         else
           var_query := var_query ||
                        '        ,null          as country_name ' ||
                        '        ,null          as cl_id ';  
         end case;
         
          var_query := var_query ||
          '   from tb_client_pl cl ' ||
          '   join tb_country_pl c on c.id = cl.birth_place_id ';

       execute immediate var_query    
       bulk collect into client_info_list;
       
       for i in client_info_list.first .. client_info_list.last
       loop
         dbms_output.put_line(
           '�������: ' || client_info_list(i).cl_name
           || ', ���� ��������: ' || client_info_list(i).birthdate
           || ', ���: ' || client_info_list(i).inn
           || ', ������ ��������: ' || client_info_list(i).country_name
           || ', �� �������: ' || client_info_list(i).cl_id
         );
         dbms_output.put_line('-------------------------');
       end loop;


end;


---------------------------------------------------------------------------

--  ����� �� ������ ������� ������� ������

---------------------------------------------------------------------------

-- ������ ����� ������ ���� ��������. 
declare
   
begin    
  
  show_client_info ('�� �����!') ;
 
  
end;



----------------------------------------------------------------------


create or replace noneditionable procedure show_client_info (user_role varchar2)
is
  -- ������ ������ client_info_list, � ������ ����� ������ ��� ��������� ����� ��� client_info_list_type,
  -- �. � ���� ������� �������� � ���� ������� � ���� ������ client_info_rec

       type client_info_rec is record (
            cl_name         varchar2(200),
            birthdate       date,
            inn             varchar2(16),
            country_name    varchar2(200),
            cl_id           number
            );

       type client_info_list_type is table of client_info_rec ;
       client_info_list  client_info_list_type;
       
       -- � ��� ������� ��� ������������ ������. � ������� ������� �� �������, ��� ��� 
       -- ����� �� ����� ����� ������ ������� ��� ����������� ��� ������ ���� 
        
       var_query varchar2(1000); 
       

begin
     -- ��������� � ���������� ������ � ���� ������  
     var_query := 
      ' select cl.name_v      as cl_name ,    ' ||
      '        cl.birthdate_d as birthdate,   ' ||
      '        cl.inn_v       as inn,         ' ||
      '        c.name_v       as country_name,' ||
      '        cl.id          as cl_id        ' ||
      '    from tb_client_pl cl'                ||
      '    join tb_country_pl c on c.id = cl.birth_place_id ';
    
   -- ������� ��������� ������ ����� ������������ SQL �
--      ��������� �������   ��������� � ����������-������ client_info_list
   EXECUTE IMMEDIATE var_query
   bulk collect into client_info_list ;


-- ������� �� ������� ������� ������� � ������ � ���� ������ �������

   for i in client_info_list.first .. client_info_list.last
     loop
       dbms_output.put_line
         ('������� :  ' || client_info_list(i).cl_name
          ||'. ���� ��������: ' || client_info_list(i).birthdate
          || '. ���: ' || client_info_list(i).inn
          || '. ������ ��������: ' || client_info_list(i).country_name
          || '. ID �������:  ' || client_info_list(i).cl_id
         );
          dbms_output.put_line ( '-------------------------------------');
     end loop;


end;




---------------------------------------------------------------------------


--  ����� �� ������ �������� ������� ������


---------------------------------------------------------------------------

create or replace procedure show_client_info (user_role varchar2)
is
  -- ������ ������ client_info_list, � ������ ����� ������ ��� ��������� ����� ��� client_info_list_type,
  -- �. � ���� ������� �������� � ���� ������� � ���� ������ client_info_rec
       
       type client_info_rec is record (
            cl_name         varchar2(200),
            birthdate       date,
            inn             varchar2(16),
            country_name    varchar2(200),
            cl_id           number
            );
       
       type client_info_list_type is table of client_info_rec ;
       
       client_info_list  client_info_list_type; 

begin  

--  EXECUTE  ����� ���� ������������ ������ ����� �������� ���� create table � �� ���������. 
-- ���� ��� ������ ����������. 

  EXECUTE IMMEDIATE 

      ' select cl.name_v      as cl_name ,    ' ||
      '        cl.birthdate_d as birthdate,   ' ||
      '        cl.inn_v       as inn,         ' ||
      '        c.name_v       as country_name,' ||
      '        cl.id          as cl_id        ' ||
      '    from tb_client_pl cl'                ||
      '    join tb_country_pl c on c.id = cl.birth_place_id '
     bulk collect into client_info_list ;
     
         
-- ������� �� ������� ������� ������� � ������ � ���� ������ �������

   for i in client_info_list.first .. client_info_list.last
     loop        
       dbms_output.put_line
         ('������� :  ' || client_info_list(i).cl_name 
          ||'. ���� ��������: ' || client_info_list(i).birthdate 
          || '. ���: ' || client_info_list(i).inn 
          || '. ������ ��������: ' || client_info_list(i).country_name
          || '. ID �������:  ' || client_info_list(i).cl_id 
         );
          dbms_output.put_line ( '-------------------------------------');
     end loop;  


end;       


---------------------------------------------------------------------------

-- ������ ����� ������ ���� ��������. 

declare
   
begin    
  
  show_client_info ('�� �����!') ;
 
  
end;


/*
������� :  ������ ���� ��������. ���� ��������: 01-JAN-00. ���: 222052992001111. ������ ��������: ����������. ID �������:  1
-------------------------------------
������� :  ������ ���� ��������. ���� ��������: 01-JAN-00. ���: 222052992001100. ������ ��������: ����������. ID �������:  2
-------------------------------------
������� :  ������� ����� ���������. ���� ��������: 01-JAN-00. ���: 222052992001155. ������ ��������: ����������. ID �������:  3
-------------------------------------
������� :  ������ ����� ��������. ���� ��������: 01-JAN-00. ���: 222052992001111. ������ ��������: ����������. ID �������:  4
-------------------------------------

*/











----------------------------------------------------------------



-- �������� ������ 13  ����.  ������������ SQL




--------------------------------------------------------------------------------------------------------------
