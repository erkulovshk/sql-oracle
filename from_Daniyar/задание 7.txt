create sequence seq_tb_client_id
   minvalue 1
   maxvalue 99999999999999999999
   start with 11
   increment by 1
   cache 100
   
   insert into tb_client (id,
                         name_v,
                         birth_date_d,
                         gender,
                         nationality,
                         birth_place)
   values(seq_tb_client_id.nextval,'Valeriya',to_date('17.12.1999','dd.mm.yyyy'),'wom','rus','Russia')
   
   
    insert into tb_client (id,
                          name_v,
                          birth_date_d,
                          gender,
                          nationality,
                          birth_place)
    values(seq_tb_client_id.nextval,'Mihail',to_date('31.03.1984','dd.mm.yyyy'),'man','rus','Kyrgystan')
    
    insert into tb_client(id,
                          name_v,
                          birth_date_d,
                          gender,
                          nationality,
                          birth_place)
    values(seq_tb_client_id.nextval,'Violetta',to_date('10.11.1997','dd.mm.yyyy'),'wom','rus','Kyrgystan')
    
    insert into tb_client(id,
                          name_v,
                          birth_date_d,
                          gender,
                          nationality,
                          birth_place)
    values(seq_tb_client_id.nextval,'Anastasiya',to_date('17.04.1996','dd.mm.yyyy'),'wom','rus','Russia')                   
   
    alter table tb_client add constraint check_tb_client_gender check(gender in ('man','wom'))
   
  
   select cl.id, 
          ad.city_v,
          street_v,
          house
   from tb_address ad join tb_client cl on cl.id=ad.client_id
   where ad.city_v='Bishkek'
 
   select cl.id,
          cl.name_v,
          cl.birth_date_d
          from tb_client cl
   where months_between(sysdate,birth_date_d)/12>20
   
   
   select*from tb_client
   