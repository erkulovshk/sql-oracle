 
 with 
tb_client_e as
(
  select 1 id, '������ ���� ��������'   name_v, 'M' gender_v, 27 age from dual 
  union all
  select 2,    '������ ���� ���������',         'M',          25     from dual 
  union all
  select 3,    '������� ����� ���������',       'M',          43     from dual 
  union all
  select 4,    '������ ����� ��������',         'M',          75     from dual 
  union all
  select 5,    '������ ������ ���������',       'M',          66     from dual
  union all
  select 6,    '������ ������ ���������',       'M',          66     from dual
  union all
  select 7,    '������ ������ ���������',       'M',          66     from dual
),
tb_product_e as
(
  select 1 id, 'phone'     name_v from dual 
  union all
  select 2,    'key'              from dual 
  union all
  select 3,    'TV'               from dual 
  union all
  select 4,    'car'              from dual
),
tb_order_e as
(
  select 1 id, 1 client_id, 1   product_id, 1200   t_cost, 1   t_count from dual 
  union all
  select 2,    1,           3,              2200,          1           from dual 
  union all
  select 3,    5,           4,               100,          2           from dual 
  union all
  select 4,    5,           3,               500,          1           from dual 
  union all
  select 5,    3,           2,              2500,          6           from dual 
)

/* ������  ����� 
������� �� ��, ��� � ������� ����. 
������� �� ����� ������ ������� ������� �� �����. 
("������" - 3 ��������, ����� ������� ����� �� ���). 
������������ ������ �������� � ����� ����.
*/

--�������  
-- ������� ������� ������ � ������ ������, � ������� ��� ��� � ���� �������� ����� tb_dubl_name
,
tb_dubl_name as (
select * from tb_client_e
       where name_v like '%������%' )
-- ����� �� tb_dubl_name ������� ������ ��� ������ ����. �����:
-- ������� ������������ ��� ����������� id �� ������ �����������, ��� �� ������ ��� ���� ����
-- ���������� ������ � id.
-- � ����� �������� ��������/������� ��� ������, ������� �� ������ � ���� ������       
select * 
  from tb_dubl_name cl 
 where cl.id not in 
(
    select max(cl.id)
      from tb_dubl_name cl
     group by cl.name_v
);

-- ���� ��������  ���� 
/* ������ 
������� �� ��, ��� � ������� ����. � ������� �������� ���������� ��������� 
10 �������� ����� ������������. ��������� �������� ����� ���������� 
(t_cost) ������� ������� ������� � �������� (���� �� ������ 83.52). 
��������� �������� ����� ���������� ���� ������� ���� �������� � ��������. 
������������ ������ �������� � ����� ����.*/

-- ��� ������ ���������� ������� ��� ���� ���� ���������� �� -10 ������
-- ����� �������� ���������, �������� �� ������� �� 83
select 
      substr (upper(cl.name_v), -10),
      round((sum(o.t_cost) / 83.52), 2)
    from tb_client_e cl
    join tb_order_e o on o.client_id = cl.id
    group by cl.name_v

-- ����� ���� ����� ������� ���� �������� ��������� ��������
 
select 
      substr (upper(cl.name_v), -10),
      round((sum(o.t_cost) / 83.52), 2) as sum_cost,
      (select round((sum(o1.t_cost) / 83.52)) 
            from tb_order_e o1) as sum_for_all_cost
    from tb_client_e cl
    join tb_order_e o on o.client_id = cl.id
    group by cl.name_v

/* ������ ����� 
������� �� ��, ��� � ������� ����. 
���������� ������������ ������� � �������� ����� 
������ ��������� (������� ������� � ����� ������� ��������� - 
������� ������ �������� �������). 
������������ ������ �������� � ����� ����.
*/

-- ������� 
-- ������� ������ - �� ���������� 
select (
select  max(cl.age)
  	 from tb_client_e cl )  - 
(select  min(cl.age)
   	 from tb_client_e cl ) difference_age
 	  	from dual 


-- �������� ������� 
select  max(cl.age) - min(cl.age)  as difference_age
from tb_client_e cl







---------------------------------------------------------------------------------

/* ������  ����� ��� ������� ����. 
������� �� ����� ��������� ��������: 
- ���� � NODE � ������� �������� �������� NULL, �� ��������� ROOT 
- ���� � NODE � ������� �������� ���� ��� �� id, �.�. �� ���� ���� ������, �� ������� LEAF
- ���� � NODE � ������� �������� ���� ��� �� id, �.�. ��� ����-�� �� �������� ��������� 
� �� ���� ���������, �� ������� INNER
*/
with tab as 
(
select 1 as node, 2 as parent from dual
union all
select 2 as node, 5 as parent from dual
union all
select 3 as node, 5 as parent from dual
union all
select 4 as node, 3 as parent from dual
union all
select 5 as node, null as parent from dual
)

-- ������� 
select ttt.*,
        case 
            when ttt.parent is null then 'root'
            when ttt.node not in (
                select parent from tab
                   where parent is not null) then 'leaf'
            else 'inner'
        end case_value
    from tab ttt 

-- ��� ������ ���� � ������ ������� - � value ����� root
-- ���� �������� � NODE �� ������� �  ������ - � value ����� leaf 
-- �� ���� ��������� ������� ( �� ����� ���� �������� NODE  ������� �  ������) - � value ����� inner 

-----------------------------------------------------------------------------------------------------------

-- �����  ��������� ������� 
-- ��� ������ ��������� ��������� ����� ���, ��� �������� null 

select ttt.*,
        case 
        when ttt.parent is null then 'root'
        end as case_value
    from tab ttt

--����� 

/*� ���� ���� ������ not in �� ���������, ������ ��� ������ ������� � ��� ���� �������� NULL, 
� ���� ����� (NOT IN ����� ���� ���� �����) �� ����� ���������� NULL ��������.
������� ��� ��� ���������, ������ �� �� ������� ����� ������ ������� ������� ������ case.
 � ��� ���������, ������ ��� ������ WHEN ���������� FALSE, �.e. ���� ���-�� ����������
*/

select ttt.*,
       case 
         when ttt.parent is null then 'root'
         when ttt.node not in (
                       select parent
                              from tab
                   ) then 'leaf' 
        end case_value 
      from tab ttt

-- �����, �����  ��������� ����� NULL � ����� �� ����� ����������� select  ������� ������ is not null

select ttt.*,
       case 
         when ttt.parent is null then 'root'
         when ttt.node not in (
                       select parent
                            from tab
                            where parent is not null   
                   ) then 'leaf' 
        end case_value 
      from tab ttt

-- �����, ����� ���������� ��� ������ ������� else 
 
select ttt.*,
        case 
            when ttt.parent is null then 'root'
            when ttt.node not in (
                select parent from tab
                   where parent is not null) then 'leaf'
            else 'inner'
        end case_value
    from tab ttt 

-- ��� ������ ���� � ������ ������� - � value ����� root
-- ���� �������� � NODE �� ������� �  ������ - � value ����� leaf 
-- �� ���� ��������� ������� ( �� ����� ���� �������� NODE  ������� �  ������) - � value ����� inner 



---------------------------------------------------------------------------------

--�������� ������ 

 /*
������: �������� ������, 
������� �������� ���������� ���������� ������������� � �����. 
� ����� ������ ������ �������� ������������ ��� 
���������� �������������, ������� �������������� � ������� 
� � ����, � � ����������  */



with logins as (
select 1 as user_id, to_date('01.07.2020','dd.mm.yyyy') as date_d from dual
union all 
select 234 as user_id, to_date('02.07.2020','dd.mm.yyyy') as date_d from dual
union all 
select 3 as user_id, to_date('02.07.2020','dd.mm.yyyy') as date_d from dual
union all 
select 1 as user_id, to_date('02.07.2020','dd.mm.yyyy') as date_d from dual
union all 
select 234 as user_id, to_date('01.06.2020','dd.mm.yyyy') as date_d from dual
) 

select *
    from logins log1 
    left join logins log2 
        on log1.user_id = log2.user_id 
        and to_char(log1.date_d, 'mm')
                        = to_char(log2.date_d, 'mm') + 1
                        
-- ��������.  ������ ���������: 
-- ��������� ���������� �������� �������� (������ �������� ������� �� ���������� 4� �������)
                        
with logins as (
select 1 as user_id, to_date('01.07.2020','dd.mm.yyyy') as date_d from dual
union all 
select 234 as user_id, to_date('02.10.2020','dd.mm.yyyy') as date_d from dual
union all 
select 234 as user_id, to_date('01.08.2020','dd.mm.yyyy') as date_d from dual
union all 
select 234 as user_id, to_date('01.07.2020','dd.mm.yyyy') as date_d from dual
union all 
select 234 as user_id, to_date('01.06.2020','dd.mm.yyyy') as date_d from dual
union all 
select 3 as user_id, to_date('02.07.2020','dd.mm.yyyy') as date_d from dual
union all 
select 100 as user_id, to_date('01.10.2020','dd.mm.yyyy') as date_d from dual
union all 
select 100 as user_id, to_date('01.09.2020','dd.mm.yyyy') as date_d from dual
union all 
select 100 as user_id, to_date('01.08.2020','dd.mm.yyyy') as date_d from dual
union all 
select 100 as user_id, to_date('02.07.2020','dd.mm.yyyy') as date_d from dual
union all 
select 100 as user_id, to_date('01.06.2020','dd.mm.yyyy') as date_d from dual
) 


-- �������
select  tb2.user_id,
        min(tb2.date_d)
         from (
select tb1.* from (
select log2.*
    from logins log1 
    left join logins log2 
        on log1.user_id = log2.user_id 
        and to_char(log1.date_d, 'mm')
                        = to_char(log2.date_d, 'mm') + 1
intersect
select log2.*
    from logins log1 
    left join logins log2 
        on log1.user_id = log2.user_id 
        and to_char(log1.date_d, 'mm')
                        = to_char(log2.date_d, 'mm') + 2) tb1
intersect
select log2.*
    from logins log1 
    left join logins log2 
        on log1.user_id = log2.user_id 
        and to_char(log1.date_d, 'mm')
                        = to_char(log2.date_d, 'mm') + 3 ) tb2
where tb2.user_id is not null
group by tb2.user_id

/* ���������� �������
����� �� � ���� ���������� select �������� ��������, ������� 
��������  ������� ����� +1, + 2 � + 3, ����� � ���������� ����
��������� � ������� INTERSECT �������� - ����� ������ ����������� � ������� 1, 2 � 3.  
����� ����� ������� �������  � �������� , ������� ����������� �� ����
���� �������, ����� �� ������ NULL ������, � �������� ������� ����������� 
���� ������� + � ������������ �� �����. ��� ������� �������� ������
���� ��� id ������� � ������ � ����������� �����. � ����������� ���� �������
��� � ���,  � ������ ������ ������ ��������� ������ ����� ������� � ����. 
��� �� ����� ������� ����������� ������� ������ ���� ���.
*/



--------------------------------------------------------------------------------

--     �������� ������ 

--------------------------------------------------------------------------------

-- ��������� ���������� �������� �������� (������ �������� ������� �� ���������� 4� �������)

    
select  --tb2.*
        tb2.user_id,
        min(tb2.date_d)
        from ( -- ������ 1-��  FROM
              select tb1.* from 
                               (  -- ������ 2-��  FROM
                      select log2.*
                          from   ( select cl.id as user_id,
                                 o.date_d as date_d
                                 from tb_client cl
                                 join tb_order o on o.client_id = cl.id ) log1 
                                 -- �������� � ������� �������(����������, ��� ������� - ��� ��� ���� ����� � �������)
                          left join  ( select cl.id as user_id,
                                 o.date_d as date_d
                                 from tb_client cl
                                 join tb_order o on o.client_id = cl.id ) log2 -- ����� �������� � ������� ������� 
                              on log1.user_id = log2.user_id 
                              and to_char(log1.date_d, 'mm')
                                              = to_char(log2.date_d, 'mm') + 1
intersect

        select log2.*
            from ( select cl.id as user_id,
                   o.date_d as date_d
                   from tb_client cl
                   join tb_order o on o.client_id = cl.id )  log1
                    -- 2-� �������� � ������� �������(����������, ��� ������� - ��� ��� ���� ����� � �������)
            left join ( select cl.id as user_id,
                   o.date_d as date_d
                   from tb_client cl
                   join tb_order o on o.client_id = cl.id ) log2 -- ����� 2-� �������� � ������� ������� 
                on log1.user_id = log2.user_id 
                and to_char(log1.date_d, 'mm')
                                = to_char(log2.date_d, 'mm') + 2
                                ) tb1 
                                --  ����� 2-�� FROM

intersect

select log2.*
    from ( select cl.id as user_id,
           o.date_d as date_d
           from tb_client cl
           join tb_order o on o.client_id = cl.id ) log1 
            -- 3-� �������� � ������� �������(����������, ��� ������� - ��� ��� ���� ����� � �������)
    left join ( select cl.id as user_id,
           o.date_d as date_d
           from tb_client cl
           join tb_order o on o.client_id = cl.id ) log2 -- ����� 3-� �������� � ������� ������� 
           on log1.user_id = log2.user_id 
        and to_char(log1.date_d, 'mm')
                        = to_char(log2.date_d, 'mm') + 3 
                        ) tb2 
                        -- ����� 1-�� FROM
where tb2.user_id is not null
group by tb2.user_id    
--where tb2.user_id = 1


 
/* ���������� �������
����� �� � ���� ���������� select �������� ��������, ������� 
�������� � ������� ������ +1, + 2 � + 3, ����� � ���������� ����
��������� � ������� INTERSECT �������� - ����� ������ ����������� � ������� 1, 2 � 3.  
����� ����� ������� �������  � �������� , ������� ����������� �� ����
���� �������, ����� �� ������ NULL ������, � �������� ������� ����������� 
���� ������� ������� + � ������������ �� �����.
��� ������� �������� ������ ���� ��� id ������� � ������ � ����������� �����. � ����������� ���� �������
��� � ���,  � ������ ������ ������ ���������  ��������� ������� ������ ����� . 
��� �� ����� ������� ����������� ������� ������ ���� ���. 

��� ������������: 
  ���� ������������� ������� (tb2.user_id = 1) � �������� select �������  ( tb2.* ),
  � ������� ������� �������� �����������, �� ����� ������� ������������ � �������, ��� � �������� � 
  ������������� ID ������������� ������� ������� ����� 4-� ������� ������. � ���� ����� ����� ��������, 
  �� ����� ��� � ������� ������� �� id ������������� ��� � join �������� ���� � ������� �������
*/


-- ������� �������� ���� ������ ���� ����� ��������� ��������� ������� 

-- ������ ��������� �������� 
create global temporary table tb_work
( 
    user_id             number
   ,date_d        date
)
 on commit preserve rows;

-- �������� �� ��������� ������� tb_work ������ �� id � ����� 
insert into tb_work (user_id, date_d)
      select cl.id,
             o.date_d  
               from tb_client cl
               join tb_order o on o.client_id = cl.id
         
-- ����� ���� ������������ ������ ��������� ������� �� ���������� �������  

select  --tb2.*
        tb2.user_id,
        min(tb2.date_d)
        from ( 
              select tb1.* from 
                               (  
                      select log2.*
                          from   ( select * from tb_work ) log1 
                          left join  ( select * from tb_work  ) log2 
                              on log1.user_id = log2.user_id 
                              and to_char(log1.date_d, 'mm')
                                              = to_char(log2.date_d, 'mm') + 1
intersect
        select log2.*
            from ( select * from tb_work  )  log1
            left join ( select * from tb_work  ) log2 
                on log1.user_id = log2.user_id 
                and to_char(log1.date_d, 'mm')
                                = to_char(log2.date_d, 'mm') + 2
                                ) tb1 
intersect

select log2.*
    from ( select * from tb_work  ) log1 
    left join ( select * from tb_work  ) log2 
           on log1.user_id = log2.user_id 
        and to_char(log1.date_d, 'mm')
                        = to_char(log2.date_d, 'mm') + 3 
                        ) tb2 
where tb2.user_id is not null
group by tb2.user_id    
--where tb2.user_id = 1

----------------------------------------------------------------------------------

/*

1.     ����� ���������� ���� �������� ������ ���������
�� ��������� �����?


2.     ���������� ����� ��������� ����� �� ���������
����� (������ 2� ���� ������ � �����)
*/


with tarif as (
select 1 as cl_id, 'Tarif 1' as tarif_name, sysdate - 1 as date_tarif, 'not work' as status from dual
union all  
select 2 as cl_id, 'Tarif 2' as tarif_name, sysdate - 1 as date_tarif, 'not work' as status from dual
union all  
select 1 as cl_id, 'Tarif 3' as tarif_name, sysdate - 1 as date_tarif, 'not work' as status from dual
union all  
select 3 as cl_id, 'Tarif 4' as tarif_name, sysdate - 1 as date_tarif, 'not work' as status from dual
union all  
select 1 as cl_id, 'Tarif 5' as tarif_name, sysdate - 1 as date_tarif, 'not work' as status from dual
union all  
select 2 as cl_id, 'Tarif 6' as tarif_name, sysdate - 1 as date_tarif,  'not work' as status from dual
union all  
select 3 as cl_id, 'Tarif 7' as tarif_name, sysdate - 1 as date_tarif, 'not work' as status from dual
union all  
select 1 as cl_id, 'Tarif 8' as tarif_name, sysdate as date_tarif, 'not work' as status from dual
union all  
select 1 as cl_id, 'Tarif 9' as tarif_name, sysdate as date_tarif, 'not work' as status from dual
union all  
select 1 as cl_id, 'Tarif 10' as tarif_name, sysdate as date_tarif, 'not work' as status from dual
union all  
select 1 as cl_id, 'Tarif 11' as tarif_name, sysdate as date_tarif, 'work' as status from dual
union all  
select 2 as cl_id, 'Tarif 12' as tarif_name, sysdate as date_tarif, 'work' as status from dual
union all  
select 3 as cl_id, 'Tarif 13' as tarif_name, sysdate as date_tarif, 'work' as status from dual
)
 
-- ������� ������� 2   
select * from
(
        select cl_id                       -- ������ id ������� 
               , count(*) as smena_tarifa  -- ������ ������ ���� ������ ��� ������� id, ��� ���� id ��������� 1 ���,
                                           
               from tarif
               where date_tarif between  to_date('29.08.2022','dd.mm.yyyy') 
               and to_date('30.08.2022','dd.mm.yyyy')
               group by cl_id              -- ��������� �� id �������, ����� ������ ������ ������ �������
)
where smena_tarifa >= 2;                     -- ������ ������ �� ������, ��� ���-�� >= 2


-- �������  ������� 1 
-- ����������� � ���������� �� ����� ������� ������������ ���� - ��� ����� ��������� ����� 
-- ����������� � �������� ������� �������� � �������, ��� ���� = ���������� ����������, �.�. ��������� ������.

select count(*)  
       from tarif t1
       where date_tarif =
                (
               select max(date_tarif)   
                  from tarif)   ;
-- �����  6 


----------------------------------------------------------------------------------
-- ������� ��������� ���  ���� projects
select * from  tb_project_employees;

select * from  tb_projects_for_empl;

select * from  tb_department_empl;

select * from  tb_empl_project_associate;

----------------------------------------------------------------------------------

select rowid, ass.* from tb_empl_project_associate ass

select rowid, e.* from tb_project_employees e


-- ������� ��������� ��� �������� ���� 
select * from  tb_client;
select * from  TB_PRODUCT;
select * from tb_order_type;
select * from  TB_ORDER;
select * from  TB_STUFF;
-- id ���� 4 � ������� position_id

select * from  TB_POSITION;

select * from  tb_country;

select * from  tb_gender;

select * from  tb_nationality;

select * from TB_ADDRESS;

select * from  tb_address_type;

