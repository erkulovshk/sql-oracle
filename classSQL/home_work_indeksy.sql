---------------------------------------------------------------------


-- �������� ������ 


----------------------------------------------------------------------
-- �������� �������� � ������ � ��� ������ ������ 

create table test_idx (
       id number,
       value_v varchar (200)
);

select * from test_idx 

-- ��������� ���� - ��� ������������ ������� PL SQL. ��� ������� �� declare begin end
--   dbms_output.put_line(''); �� ������ � �����, ��, ��� �� �������� ������ ������� ''
 


declare 

begin 
  
  dbms_output.put_line('������ ���!'); -- ��� ���� ����� �� ����� ����-��
  
end;

-- ����� ����� �������� insert ������  dbms_output.put_line('')

declare 
         i number := 1; 
         v varchar2(100) := '��������1';
begin 
  
         insert into test_idx values (i, v); 
  
end;

select * from test_idx 


-- ����� ����� ��������  ����-COMMIT
declare 
         i number := 2; 
         v varchar2(200) := '��������2';
begin 
         insert into test_idx values (i, v);
         commit;
 
end;

select * from test_idx 

-- ����� ����� ��������  ������� ������ ������� �� ���������

declare 
         i number := 3; 
         var_v varchar2(200) := '��������'; -- ����� � ����� ����� ���� ��������� �������� i, �.�. ����� 
begin 
       while (i < 1000) loop
         insert into test_idx values (i, var_v || i);  -- ��� ��� �������� � ����� "��������" ����� ������������ ����� 
         i := i + 1;
       end loop;
       
         commit;
 
end;

select * from test_idx 




with
tb_client_e as
(
  select 1 id, '������ ���� ��������'   name_v, 'M' gender_v, 27 age from dual
  union all
  select 2,    '������ ���� ���������',         'M',          25     from dual
  union all
  select 3,    '������� ����� ���������',       'M',          43     from dual
  union all
  select 4,    '������ ����� ��������',         'M',          75     from dual
  union all
  select 5,    '������ ������ ���������',       'M',          66     from dual
  union all
  select 6,    '������ ������ ���������',       'M',          66     from dual
  union all
  select 7,    '������ ������ ���������',       'M',          66     from dual
),
tb_product_e as
(
  select 1 id, 'phone'     name_v from dual
  union all
  select 2,    'key'              from dual
  union all
  select 3,    'TV'               from dual
  union all
  select 4,    'car'              from dual
),
tb_order_e as
(
  select 1 id, 1 client_id, 1   product_id, 1200   t_cost, 1   t_count from dual
  union all
  select 2,    1,           3,              2200,          1           from dual
  union all
  select 3,    5,           4,               100,          2           from dual
  union all
  select 4,    5,           3,               500,          1           from dual
  union all
  select 5,    3,           2,              2500,          6           from dual
)


select cl.name_v,
       pp.name_v as pr_name
    from tb_client_e cl
    left join tb_order_e o on o.client_id = cl.id
    left join tb_product_e pp on pp.id = o.product_id  
    -- ��� ��� JOIN ����� ������ � �������, ����� ���� �������, �� ���� left, 
    -- ����� �������� � ��� ��������, � ������� ��� ������� 
    order by pp.name_v, cl.name_v desc
    -- ����������� order by ����� ������� ���������� �� �������� � ��������� 
    
    
    
---------------------------------------------------------------
--           ������� 
---------------------------------------------------------------

��������� ������� � �������, ���������� ��� ��� ����� ������� �� ����� � ��� �� �������. 
��� ����� �������� ��� ���������� ������� (concatenated index). ��������� ������� ��������
 ������� ��� ����������� ������������ ��������� �������� ������� � ��� �������, ����� ��� 
 ����������� �������, ���������� ����������������� ������.


������� � ��������������� ������ � ���, �� ����, �� �� �����, ��� � ������� B-��������, �� ����������� ����, 
��� ����� ������ ��������� ������� ��� ���������� ������ ������� �� ���������������.
 ������� �������� �������� ����������, �������� ������ ������� ������.

������� �� ������ ������
���������� ������������ �������� ������� ������ � ���������� ������������������ ����� �� ���� �������� ������� �� ������ ������.
 ������ ���, ����� ������������� ���� ����� ������������� ���������, ��� �� ��������� ���������� ��������������� ������,
  ���������� ������� �� ������������� ������ �����. ��� ������:

  CREATE INDEX emp_indx1 ON employees(ename)
TABLESPACE MY_INDEXES
COMPRESS 1;





--------------------------------------------------------------------------
-- � � � � � � � �       � � � � � � 

--------------------------------------------------------------------------
-- �/�  �������


-- 1. ������� ��������� ������ � ������� �������� �� ���� �������, ���, ��������.


create index concatenated_idx_cl on tb_client(second_name_v, first_name_v,surname_v);

drop index concatenated_idx_cl;



-- 2. ������� ����������� ������ � ������� ������� �� ���� ���� ������.

create index reverse_idx_order_date on tb_order (date_d) reverse;

drop index reverse_idx_order_date;



-- 3. ������� ������ �� ������ ������ � ������� ������� �� ���� Client_id.

create index compress_idx_order_cl_id on tb_order (client_id) compress 1;

drop index compress_idx_order_cl_id;


-- 4. ������� ������ �� ������ ������� UPPER � ������� ����������� �� ���� �������

create index upper_idx_st_name on tb_stuff (upper(last_name_v));

drop index upper_idx_st_name;





----------------------------------------------------------------------------------
-- ������� ��������� ���  ���� projects

select  *  from t_currency
----------------------------------------------------------------------------------

select * from  tb_project_employees;

select * from  tb_projects_for_empl;

select * from  tb_department_empl;

select * from  tb_empl_project_associate;

----------------------------------------------------------------------------------

select rowid, ass.* from tb_empl_project_associate ass

select rowid, e.* from tb_project_employees e

 
  
-- ������� ��������� ��� �������� ���� 
select * from  tb_client;

select * from  TB_PRODUCT;

select * from tb_order_type;

select * from  TB_ORDER;

select * from  TB_STUFF;
-- id ���� 4 � ������� position_id

select * from  TB_POSITION;

select * from  tb_country;

select * from  tb_gender;

select * from  tb_nationality;

select * from TB_ADDRESS;

select * from  tb_address_type;
