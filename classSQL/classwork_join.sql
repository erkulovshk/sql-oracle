create table TB_BRANCH
(
  branch_id NUMBER not null,
  code_v    VARCHAR2(30) not null,
  name_v    VARCHAR2(100)
);

create table TB_DEP
(
  dep_id    NUMBER not null,
  code_v    VARCHAR2(30) not null,
  name_v    VARCHAR2(100),
  upper_id  NUMBER,
  branch_id NUMBER
);


alter table TB_BRANCH 
add constraint pk_br_id primary key (branch_id);

alter table TB_DEP
  add constraint FK_DEP__UPPER_ID foreign key (DEP_ID)
  references TB_BRANCH (branch_id);


INSERT INTO TB_BRANCH VALUES (1, '001', '�������� ������');
INSERT INTO TB_BRANCH VALUES (2, '002', '���������� ������');
INSERT INTO TB_BRANCH VALUES (3, '003', '�������� ������');
INSERT INTO TB_BRANCH VALUES (4, '004', '��������� ������');
INSERT INTO TB_BRANCH VALUES (5, '005', '������� ������');
INSERT INTO TB_BRANCH VALUES (6, '006', '������-�������� ������');
INSERT INTO TB_BRANCH VALUES (7, '007', '�����-�������� ������');
INSERT INTO TB_BRANCH VALUES (8, '008', '�������� ������');
INSERT INTO TB_BRANCH VALUES (9, '009', '����������� ������');
INSERT INTO TB_BRANCH VALUES (10, '010', '���������� ������');
INSERT INTO TB_BRANCH VALUES (11, '007', '�����-�������� ������');
INSERT INTO TB_BRANCH VALUES (12, '008', '�������� ������');
INSERT INTO TB_BRANCH VALUES (13, '009', '����������� ������');
INSERT INTO TB_BRANCH VALUES (14, '010', '���������� ������');
INSERT INTO TB_BRANCH VALUES (15, '010', '���������� ������');

INSERT INTO TB_DEP VALUES (1, '001', '������ �������������', null, null);
INSERT INTO TB_DEP VALUES (2, '002', '������ �������������', 1, 3);
INSERT INTO TB_DEP VALUES (3, '003', '������ �������������', null, 4);
INSERT INTO TB_DEP VALUES (4, '004', '��������� �������������', null, 6);
INSERT INTO TB_DEP VALUES (5, '005', '����� �������������', null, 8);
INSERT INTO TB_DEP VALUES (6, '006', '������ �������������', null, 10);
INSERT INTO TB_DEP VALUES (7, '007', '������� �������������', null, 2);
INSERT INTO TB_DEP VALUES (8, '008', '������� �������������', null, 2);
INSERT INTO TB_DEP VALUES (9, '009', '������� �������������', null, 5);
INSERT INTO TB_DEP VALUES (10, '010', '������� �������������', null, 2);
INSERT INTO TB_DEP VALUES (11, '011', '����������� �������������', 1, 3);
INSERT INTO TB_DEP VALUES (12, '012', '����������� �������������', null, 4);
INSERT INTO TB_DEP VALUES (13, '013', '����������� �������������', null, 6);
INSERT INTO TB_DEP VALUES (14, '014', '������������� �������������', null, 8);
INSERT INTO TB_DEP VALUES (15, '015', '����������� �������������', null, null);

--inner join 100% c��������� � ����� ����� � ������ ����� 
select br.branch_id,  
       br.name_v, 
       null, 
       dep.branch_id,
       dep.name_v
from tb_branch br
inner join tb_dep dep on br.branch_id = dep.branch_id;


--left join ������ ����� ��� ���������, ����� - ������ ����������� ��������
select br.branch_id,
       br.name_v,
       null,
       dep.branch_id,
       dep.name_v
from tb_branch br
left join tb_dep dep on br.branch_id = dep.branch_id;

--right join  ����� ����� ��� ���������, ������ - ������ �����������
select br.branch_id,
       br.name_v,
       null,
       dep.branch_id,
       dep.name_v
from tb_branch br
right join tb_dep dep on br.branch_id = dep.branch_id;

-- full join  ������� � ����� � ������ ����� �������, ��������� ��� �����������
-- ��� � �� ����������� 
select br.branch_id,
       br.name_v,
       null,
       dep.branch_id,
       dep.name_v
from tb_branch br
full join tb_dep dep on br.branch_id = dep.branch_id;

-- cros join �� ������������ �������, �.�.  size(result) = size(tb_branch) * size(tb_dep)
select br.branch_id,
       br.name_v,
       null,
       dep.branch_id,
       dep.name_v
from tb_branch br
cross join tb_dep dep

-- where ������������ ��� ���� ������ � ������ ������� ������� where

select br.branch_id,
       br.name_v,
       null,
       dep.branch_id,
       dep.name_v
from tb_branch br
right join tb_dep dep on br.branch_id = dep.branch_id
where dep. name_v = '������� �������������' and br.branch_id = 2;

-- is null �������� �� ������ �������� 

select br.branch_id,
       br.name_v,
       null,
       dep.branch_id,
       dep.name_v
from tb_branch br
full join tb_dep dep on br.branch_id = dep.branch_id
where br.branch_id is null


/*
select * from TB_DEP;
select * from TB_BRANCH;
select * from tb_additional_information;

select dep_id, code_v, name_v, upper_id, branch_id 
from TB_DEP;
 

select branch_id , code_v , name_v 
 from TB_BRANCH;
 
 
 */
