 

create table tb_product
(
  id         number not null,
  name_v     varchar2(200) not null,  
  price_n     number,                  
  constraint pk_tb_product  primary key (id)
 );

 
 create table tb_order
(
  id         number not null,
  type_v     varchar2(20) not null,  
  quantity_n number,
  product_id number not null,
  constraint pk_tb_order  primary key (id),
  constraint  fk_product_id_tb_order  foreign key  (product_id) references  tb_product (id)
 );
 

insert into tb_product values (1,'�������', 10500.56 );
insert into tb_product values (2,'������� �������', 500.86 );
insert into tb_product values (3,'������� �������', 100.06 );
insert into tb_product values (4,'������� �����', 50200.36 );
insert into tb_product values (5,'����', 9800.01 );
insert into tb_product values (6,'�����', 10500.56 );
insert into tb_product values (7,'����������', 500.86 );
insert into tb_product values (8,'�������', 100.06 );
insert into tb_product values (9,'  �������', 50200.36 );
insert into tb_product values (10,'  ������� �������', 9800.01 );



insert into tb_order values (1,'��������', 2,5 );
insert into tb_order values (2,'���������', 1,3 );
insert into tb_order values (3,'��������', 5, 2);
insert into tb_order values (4,'��������', 6,4 );
insert into tb_order values (5,'��������', 1, 1);
insert into tb_order values (6,'���������', 5, 5);
insert into tb_order values (7,'���������', 4, 3);
insert into tb_order values (8,'��������', 2, 3);
insert into tb_order values (9,'��������', 1, 2);
insert into tb_order values (10,'��������', 3, 5);
insert into tb_order values (11,'��������', 8, 6);
insert into tb_order values (12,'���������', 20,7 );
insert into tb_order values (13,'���������', 30, 8 );
insert into tb_order values (14,'��������', 20, 9 );
insert into tb_order values (15,'��������', 50, 10);



/* 1. �������� ���������� ������������� (�����) ���
�������� (�������� � ������ �������� ������ 5 �������� �������, ����
������� �������������  �_�, ����
��������, ���� �-� � ����� ��������, ��������: ������_01_05�). ������� ��������
��� ������� � ���������� �������������.  */

select cl.name_v,
       lower(substr(cl.name_v, 1, 5)) || '_' || to_char(cl.birth_date,'dd') || '_'|| to_char(cl.birth_date,'mm') as login
    from tb_client cl;


/*2. ������� ������ ��� ����������� �� 1 ����� �����
������� ��������� ������ ������ 1001.1   */

select p.name_v,
       round(p.price_n, 1)
    from tb_product p
 where p.price_n > 1001.1;
 
/* 3. ������� 20% �������� �� ����� � ������ ���������
����� �������. ��������� �������� ������� ������������ ������ (� �������
��������, �������� ��� ������� � ������ � � ����� ������ ������� trim), ��� ���������,
����������� �� 0 ������ ����� ������� � ��������� ������ � 20% ���������
����������� �� 2� ������ ����� ������� � ��������� �������.*/


select trim(upper(p.name_v)) as pr_name,
       round(p.price_n) as price,
       round(p.price_n * 1.2) as new_price
       from tb_product p

 

/*
select  * from tb_product;
select  * from tb_order;
select * from tb_projects_pw;
select * from tb_department_pw;
select * from tb_empl_project_associate;

drop table tb_order;

3
UPDATE customers
   SET last_name = 'Anderson'
 WHERE customer_id = 5000;
 
DELETE FROM tb_employees_pw
WHERE name_v = 'Win'
salary

update tb_client
set nationality_v = 'Kyrgyzstan'
where id = 3

ALTER TABLE tb_addres
  MODIFY house_v   varchar2 (50);

DELETE FROM tb_client
WHERE nationality_v = 'Russia'
AND id > 2;

DROP TABLE tb_client;
*/
