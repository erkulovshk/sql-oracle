---------------------------------------------------------------------


-- �������� ������ 


----------------------------------------------------------------------


/*

������������, � ���� "����� -- �� �����", �� ������� ��� �������� ����� �� �����
- ����� ��������� >> ������ ��� 
- ����� ������� ��������� ���� ������  �����, ������� ������� �� ������ ��������� >> ���� ������ 
- ����� ������� ��������� ���� ������  �����, ������� ������� �� ������ ��������� >> �������� �����
- � ��, ��������� �������� ��� ���� �������������� ��� ���� ������

*/
SELECT LPAD('abc', 4) FROM DUAL;
--���������:    abc



SELECT LPAD(' ', 4) FROM DUAL;
--���������: _ _ _ _ 



SELECT LPAD('  ', 4) FROM DUAL;
--���������: _ _ _ _ 



SELECT LPAD('           ', 4) FROM DUAL;
--���������: _ _ _ _ 



SELECT LPAD('abc', 4, '123') FROM DUAL;
--���������: 1abc



SELECT LPAD('abc', 2, '123') FROM DUAL;
--���������: ab



SELECT LPAD('abc', 1) FROM DUAL;
--���������: a


SELECT LPAD('a ', 2) FROM DUAL;
--���������: a_



SELECT LPAD('a', 2) FROM DUAL;
--���������: a


SELECT LPAD('a   a', 5) FROM DUAL;
  3 ������� ����� ����� a 
--���������:a_ _ _ a


SELECT LPAD('a  a', 5) FROM DUAL;
 2 ������� ����� ����� a 
--���������: _a_ _ a


----------------------------------------------------------------------------------------------------------

SELECT LPAD('a   b', 4) FROM DUAL;
3 ������a ����� a � b. ����� � ������ ��������� 5 ������
������ �������� > ������� �� (-1), �.�. �� ����� ������ �� ���� ����.
� ����� ������ �� �����������, � ���������� �� ���� ���� �
 ���������� �������� �������� � ������ ����.  b ��������
--���������: a _ _ _


SELECT LPAD('a   ', 3) FROM DUAL;
3 ������a ����� a. ����� � ������ ��������� 4 �����
������ �������� > ������� �� (-1), �.�. �� ����� ������ �� ���� ����.
� ����� ������ �� �����������, � ���������� �� ���� ���� �
 ���������� �������� ����������� �� ����. �.�. ���� ������ ��������.
--���������: a _ _ 


SELECT LPAD('a   ', 4) FROM DUAL;
3 ������a ����� a. ����� � ������ ��������� 4 �����
������ �������� > ������� �� (0), �.�. �� ����� ������ �������� ����� �������.
� ����� ������ �� ����������� � �� ������������, ������ ��������� ����� ����� ����.
--���������: a _ _ _


SELECT LPAD(' a   ', 5) FROM DUAL;
1 ������ �� 'a' � 3 ������a ����� 'a'. ����� � ������ ��������� 5 ������.
������ ��������  ����� �������.
� ����� ������ �� ����������� � �� ������������, ������ ��������� ����� ����� ����.
--���������: _a _ _ _


SELECT LPAD('   a   ', 7) FROM DUAL;
3 ������� �� 'a' � 3 ������a ����� 'a'. ����� � ������ ��������� 7 ������.
������ ��������  ����� �������.
� ����� ������ �� ����������� � �� ������������, ������ ��������� ����� ����� ����.
--���������: _ _ _ a _ _ _


SELECT LPAD('   a   ', 10) FROM DUAL;
3 ������� �� 'a' � 3 ������a ����� 'a'. ����� � ������ ��������� 7 ������.
������ �������� > ������� �� ��� �����.
� ����� ����� 'a' ����������� ��� ��� �������
--���������: _ _ _  _ _ _  a _ _ _



SELECT LPAD('    a', 2) FROM DUAL;
4 ������� �� 'a'. ����� � ������ ��������� 5 �����.
������ �������� >  ������� �� (-3), �.�. �� ����� ������ �� ��� �����.
� ����� ������ �� �����������, � ���������� �� ��� �����.  �.�. ���� 'a' � 
��� ������� ��������. ����� � ������ ����� ������ 2 �������
--���������: _ _ 



SELECT LPAD(' a   ', 4) FROM DUAL;
1 ������ �� 'a' � 3 ������a ����� 'a'. ����� � ������ ��������� 5 ������.
������ �������� > ������� �� (0), �.�. �� ����� ������ �������� ����� �������.
� ����� ������ �� ����������� � �� ������������, ������ ��������� ����� ����� ����.
--���������: _a _ __



----------------------------------------------------------------------------------------------------------

SELECT LPAD(' ', 1) FROM DUAL;
� ������ ��������� 1 ������. ������ �������� ����� �������.
� ����� ������ �� ����������� � �� ������������, ������ ��������� ����� ����� ����.
--���������: _

SELECT LPAD('', 8) FROM DUAL;
� ������ ��������� ������ ������. �� ����� �������� ������� ���������, �� ������ ������� NULL � ������.
--���������: null

SELECT LPAD('abcd', 0) FROM DUAL;
������ �������� ����� ���� . �� ����� ��������  ������� ���������, �� ������ ������� NULL � ������.
--���������: null



SELECT LPAD('   ', 1) FROM DUAL;
� ������ ��������� 3 �������. ��  ������� ���������� ������ ��� ���� ������.
����� � ������ ��������� 1 ����.������ �������� ����� �������.
� ����� ������ �� ����������� � �� ������������, ������ ��������� ����� ����� ����.
--���������: _

SELECT LPAD('  ', 0) FROM DUAL;
� ������ ��������� ����, ��� - �� ����� ������� ��������. ������� ����������  ��� ���� ������.
� � ����� ������� null , ������ ��� ������ �������� ����� ����.
--���������: null 
������ ��� ��������� � ����������, ����� ����������� �������� � ��� ������ ��������� ���� ���� '0'.
B ������� ����� ������� ����� ������ ����� �� ������� SELECT LPAD('  ', 0) FROM DUAL 
� ������ �������� * ���������. 

select level,
       e.name_v,
       lpad(' ', (level - 1) * 5) || '*'
      from tb_project_employees e
connect by prior e.id = e.upper_id
start with e.upper_id is null;



SELECT LPAD('  ', 2) FROM DUAL;
� ������ ��������� 2 �������. ��  ������� ���������� ������ ��� ���� ������.
����� � ������ ��������� 1 ����.������ �������� > ������� �� 1.
� ����� ������ �� ����������� � �� ������������, ������ ��������� ����� ����� ����.
--���������: _


----------------------------------------------------------------------------------------------------------

SELECT LPAD('lpad function', 15, 'z') FROM DUAL;

���  13 ������ ������ � 1 ��������. ����� 13 > 15 �� 2 �����. ��� z ���������
--���������:   zzlpad function


SELECT LPAD('lpad  function', 15, 'z') FROM DUAL;

���  13 ������ ������ � 1 ��������. ����� 13 > 15 �� 2 �����. ��� z ���������
--���������:   zzlpad function

----------------------------------------------------------------------------------------------------------

-- �������� ������ 

-- �������� 
----------------------------------------------------------------------
/* ������ 
������: ���������� ������� ������ � ���� �����������
�������������� �� ��������. */

select e.id,
       e.name_v,
       e.position_v,
       e.upper_id
from tb_project_employees e
connect by prior e.id = e.upper_id
start with e.upper_id is null;

/*  ������ ���� ������, �� ��� ������� - ����� � ������� �������� ��� 
 Level - ��� ������� �����������  */

select level,
       e.id,
       e.name_v,
       e.position_v,
       e.upper_id
from tb_project_employees e
connect by prior e.id = e.upper_id
start with e.upper_id is null;
 
/*  ������� case � level. Level ����� ���������� ������� 
 ������� ����� ������ ��� �� �� � �� ���������� */
 
select level,
       case 
         when level = 1 then ''
         when level = 2 then '     '
         when level = 2 then '          ' 
        end  || e.name_v as name_emp
      from tb_project_employees e
connect by prior e.id = e.upper_id
start with e.upper_id is null;


 � ������ ���� ���� ��������, ��� ��� ���� ����� 20 �������, �� ��� �������� 
when ������ ������ 20 ���. ����� �������� ���. 
���� �� ������� lpad. ��������: Lpad ('������', 10, '*'). ���� ��� ������� ������ ������
'������' ������ ������� � ��� ������, ���� ����� ������ ������ 10.
���� ����� ������ 4, �� ������� 6 '*' ��������� 
 
 select level,
         case 
           when level = 1 then ''
           when level = 2 then '     '
           when level = 2 then '          ' 
          end  || e.name_v as name_emp,
          lpad('������', 10, '*')
        from tb_project_employees e
  connect by prior e.id = e.upper_id
  start with e.upper_id is null;

  

����� 

select level,
       case 
         when level = 1 then ''
         when level = 2 then '     '
         when level = 2 then '          ' 
        end  || e.name_v as name_emp,
        lpad(' ', level) || '*'
      from tb_project_employees e
connect by prior e.id = e.upper_id
start with e.upper_id is null;


-- ����� ������������ � ������ case: 
lpad(' ', (level - 1) * 5) 

� ������ � ����������, ������ �������� lpad ������������� � ����� ���� (1-1)*5 =0.
������ ��� ����� lpad ����� ����� null, � ����� � ���� �� ������������ ������ ������ '*'
 
� ������ � ������, ������ �������� lpad ����� ������������� � �����   (1-1)*5 =5.
������ ��� ����� lpad ����� ����� 5 �������� - ��� ��� ��� ����� 5, 
� ����� � ���� �� ������������ ������ ������ '*'. � �.�. �� ���� �����. 

select level,
       e.name_v,
       lpad(' ', (level - 1) * 5) || '*'
      from tb_project_employees e
connect by prior e.id = e.upper_id
start with e.upper_id is null;

-- ����� ������ * ������� ��� 

select level,
       lpad(' ', (level - 1) * 5) || e.name_v
      from tb_project_employees e
connect by prior e.id = e.upper_id
start with e.upper_id is null;

-- ����� � ���������� ������� ������� ���������� ������ �������� � ������� order siblings by  
select level,
       lpad(' ', (level - 1) * 5) || e.name_v
      from tb_project_employees e
connect by prior e.id = e.upper_id
start with e.upper_id is null
order siblings by e.name_v; -- ������ ���������� ������ �������� 

-- ����� ����������, � �������� �������� ������� ��������, � ����� ������������ ������ ID
select   e.id,
         lpad(' ', (level - 1) * 5) || e.name_v as name_result,
         e.position_v,
         e.upper_id
      from tb_project_employees e
connect by prior e.id = e.upper_id
start with e.id = 4;



select e.id,
       lpad(' ', (level - 1) * 5) || e.name_v as name,
       e.position_v,
       e.upper_id
    from tb_project_employees e
connect by e.id = prior e.upper_id
start with e.id = 12; 


----------------------------------------------------------------------------------
-- ����� ����������� with 
-- � ������: �������� ������� � ����� ��������, ��� ���� ������� flag = 1, �� ���� �������� dt 
--  ���   ������� flag = 0, �� ���� �������� kt

with acc_tab as
 (select 111122223333 as dt, 44444445555555 as kt, 1 as flag
    from dual
  union all
  select 223333332222 as dt, 7777766655555 as kt, 1 as flag
    from dual
  union all
  select 4546676767665 as dt, 2245566454355 as kt, 0 as flag
    from dual
  union all
  select 3423432432432 as dt, 2323232323232 as kt, 0 as flag
    from dual
  union all
  select 111122223333 as dt, 43565464565463 as kt, 1 as flag
    from dual)
    
 select * from  acc_tab
 

/*  
  � ������: 
  �������� ������� � ����� ��������, ��� ���� ������� flag = 1, �� ���� �������� dt 
  ���   ������� flag = 0, �� ���� �������� kt
  
  ��� ������������� with ���� ����������� ������������ ������� 
 select. 
 ����� ';' ����� � ������� � ���� ���� ���� �����������.  */

--                ������� � ������� case               

/*
select acc.*,
       case 
           when acc.flag = 1 then acc.dt
           when acc.flag = 0 then acc.kt
        end as res_case
     from acc_tab acc
*/

--                ������� � ������� decode              
/*
select acc.*,
        decode(acc.flag, 1, acc.dt, acc.kt) as res_decode  
  from acc_tab acc

  decode(acc.flag, 1, acc.dt, acc.kt) as res_decode  
���� acc.flag ����� ������� �������� (1), ����� �������� � res_decode  �������� acc.dt
���� acc.flag �� ����� ������� �������� (1), ����� �������� � res_decode  ��������  acc.kt

*/

--                ������� � ������� �������������    

/*
select acc.*, 
       acc.dt * acc.flag + acc.kt * (1 - acc.flag) as result
  from acc_tab acc

��� ��� � ��� ��� �����, �� �� ����� ������� ��� ������������. 
������ ���������� ����� ����� ������ ������ acc.dt, � ������ ����� flag = 1,
������ ��� (acc.dt * 1) = acc.dt. 
� ����� (1-1) = 0;   acc.dt + 0 = acc.dt


������ ���������� ����� ����� ������ ������ acc.kt, � ������ ����� flag = 0, 
������ ��� (1 - 0) = 1; � ���� 1 *  acc.kt = acc.kt

*/




---------------------------------------------------------------------

-- �������� ������ 

-- ������ � ��������� t_currency. ����� ������� ����� ����� ������ �� ���� 

----------------------------------------------------------------------
create table t_currency
       (code_v varchar2(32) not null,
       date_d date not null,
       value_n  number not null);


insert into t_currency values ('USD',to_date('25.05.2009 12:00:00','DD.MM.YYYY HH:MI:SS'),84);
insert into t_currency values ('USD',to_date('26.06.2009 12:00:00','DD.MM.YYYY HH:MI:SS'),83);
insert into t_currency values ('USD',to_date('29.06.2009 12:00:00','DD.MM.YYYY HH:MI:SS'),82);
insert into t_currency values ('USD',to_date('30.06.2009 12:00:00','DD.MM.YYYY HH:MI:SS'),84);
insert into t_currency values ('USD',to_date('01.07.2009 12:00:00','DD.MM.YYYY HH:MI:SS'),83);
insert into t_currency values ('USD',to_date('02.07.2009 10:00:00','DD.MM.YYYY HH:MI:SS'),80);
insert into t_currency values ('USD',to_date('02.07.2009 12:00:00','DD.MM.YYYY HH:MI:SS'),79);
insert into t_currency values ('USD',to_date('02.07.2009 07:00:00','DD.MM.YYYY HH:MI:SS'),81);
insert into t_currency values ('USD',to_date('08.07.2009 09:00:00','DD.MM.YYYY HH:MI:SS'),83);

-- ����� �������� � 24-� �������, ���� �������� ����� hh24:mi:ss
insert into t_currency values ('USD',to_date('09.07.2009 18:00:00','DD.MM.YYYY hh24:mi:ss'),84);
insert into t_currency values ('USD',to_date('09.07.2009 15:00:00','DD.MM.YYYY hh24:mi:ss'),84);
insert into t_currency values ('USD',to_date('09.07.2009 14:00:00','DD.MM.YYYY hh24:mi:ss'),84);

select * from t_currency;


-- ������: �������� ���� ����� �� ������������ ����, ������� ������������ ������ ( �������� � ������� where)
-- ������� 1     

select c9.*,
       to_char(c9.date_d, 'dd.mm.yyyy')
     from t_currency c9 
  where to_char(c9.date_d, 'dd.mm.yyyy') = '29.06.2009';
 


-- ������� 2 ����, ��� ����������� ������ ����.    trunc(c9.date_d)  
-- ����� ������ ��� ��� (ORA-01843: not a valid month), �������� ��� ������� � ���, ��� ������ ����� 12, � �� 24 

select     c9.code_v,
           c9.date_d,
           c9.value_n,
           trunc(c9.date_d)  
     from t_currency c9 
  where trunc(c9.date_d) = to_date('29.06.2009', 'dd.mm.yyyy'); -- ����� ������ � ������ ���� ��� ����� ��������� � ���������

 
select c9.code_v,
       c9.date_d,
       c9.value_n
       from t_currency c9 
    where trunc(c9.date_d) = to_date('02.07.2009', 'dd.mm.yyyy'); -- ��� ��� ������ � ������, ��� ��� 02.07.2009 ��������� ������ 
    
  
    


-- �� 30 ����� ��� �������, � ��� ��������, ���� �������� 02.07.2009 ���� ��������� ������ �����?
-- ������� 
-- ������� 3:   ����� max (date)  ����� to_char 

select max(c9.date_d) 
       from t_currency c9 
    where to_char(c9.date_d, 'dd.mm.yyyy') = '02.07.2009';


-- ������� 4:   ����������� ������ ����, group by  � having � max
-- �� max ������ �� ��������, ��� ��� ���� ���� ���� ��� ���� ������������ 

select max(c9.date_d) 
       from t_currency c9 
    where trunc(c9.date_d) = to_date('02.07.2009', 'dd.mm.yyyy')
    group by c9.date_d
    having c9.date_d = max(c9.date_d);

-- ������� 5:   �����������   ����� ������ select  �� max 
-- � �������� ��� ������ ���� ���� 

select *
       from t_currency c5
    where c5.date_d =
        (select  max(c9.date_d) 
               from t_currency c9 
            where trunc(c9.date_d) = to_date('02.07.2009', 'dd.mm.yyyy')); 

-- ������: ���� �� ����, ���������� ��������, ���� �������, 
-- �� ���� �������� ���������� ���� 

-- �������:  ������ � ���������� ������ ���� �������� ���� ">" ����� "="
-- � ����� �� ������� ���� ��, ��� �����. � ���� ����� ����, �� ������� 

select *
       from t_currency c5
    where c5.date_d =
        (select  max(c9.date_d) 
               from t_currency c9 
            where trunc(c9.date_d) <= to_date('28.06.2009', 'dd.mm.yyyy')); 
-- ����: �� 28 ���� ������ ����� ����, ������� ������� �������� ������ �� 26



--------------------------------------------------------------------------
-- � � � � � � � �       � � � � � � 

--------------------------------------------------------------------------
-- �/�   ��������
/*
��������� ����������� �������
������� �������� �� �������� � �����������.
*/ 

-- ��� ����� ������ 
select   c.id           as cl_id,
         c.first_name_v as cl_name,
         c.stuff_id     as cl_st_id,
         s.first_name_v as st_name,
         s.id           as st_id 
      from   tb_client c
      join tb_stuff s on s.id = c.stuff_id 
                  
/* �������
  ��� ����� ����� ������� � ������� stuff � ��������, ����� ��� ����� � ����� ������� �������� 
 id ����������� � ���� �� �������� ���� � ������� stuff_id, ������� ��������� �� id ����������. 
 ������� ����� null ���, ��� upper � stuff � id � �������. */
 
with result_tb as 
       (select tb_stuff.id as id,
               null as upper_id,
               tb_stuff.first_name_v as all_name 
           from tb_stuff
               union all
        select 
               null as id,
               tb_client.stuff_id as upper_id,
               tb_client.first_name_v as all_name from tb_client)
 -- select * from result_tb  

-- ���������� � �������� �������  
select level,
       lpad(' ', (level-1)* 5) || res.all_name
   from result_tb res 
      connect by prior res.id = res.upper_id
      start with res.upper_id is null
      order  siblings by  res.all_name desc;  


-- �������� �� ����� 
select level,
       lpad(' ', (level-1)* 5) || res.all_name
   from result_tb res 
      connect by prior res.id = res.upper_id
      start with res.upper_id is null
      order siblings by res.all_name; -- ���������� �� ����� 

-- ����������� ��� � ������� lpad 

select level,
       lpad(' ', (level-1)* 5) || res.all_name
   from result_tb res 
      connect by prior res.id = res.upper_id
      start with res.upper_id is null

-- ���������� level + ��� + lpad � ��������� ������� 
select level,
       res.all_name,
       lpad(' ', (level-1)* 5) || '*'
   from result_tb res 
      connect by prior res.id = res.upper_id
      start with res.upper_id is null
     

-- ���������� � level c ��������� � ����� ������ 
select level,
       res.id,
       res.all_name,
       res.upper_id
      from result_tb res 
      connect by prior res.id = res.upper_id
      start with res.upper_id is null

----------------------------------------------------------------------------------
-- ������� ��������� ���  ���� projects

select  *  from t_currency
----------------------------------------------------------------------------------

select * from  tb_project_employees;

select * from  tb_projects_for_empl;

select * from  tb_department_empl;

select * from  tb_empl_project_associate;

----------------------------------------------------------------------------------

select rowid, ass.* from tb_empl_project_associate ass

select rowid, e.* from tb_project_employees e

 
  
-- ������� ��������� ��� �������� ���� 
select * from  tb_client;

select * from  TB_PRODUCT;

select * from tb_order_type;

select * from  TB_ORDER;

select * from  TB_STUFF;
-- id ���� 4 � ������� position_id

select * from  TB_POSITION;

select * from  tb_country;

select * from  tb_gender;

select * from  tb_nationality;

select * from TB_ADDRESS;

select * from  tb_address_type;
