--------------------------------------------------------------------------
--                      ������� ������ 


--------------------------------------------------------------------------


create table test_tr (
       id number,
       value varchar2 (200)
       
       );
-- value ��������, �� ��� ����� ����������

select * from test_tr 

insert into test_tr values (1, 'test_1');
insert into test_tr values (2, 'test_2');
insert into test_tr values (3, 'test_3');
insert into test_tr values (4, 'test_4');
insert into test_tr values (5, 'test_5');

delete from test_tr;

-- truncate ����������� � ������� ��� ������� ��� ������ � �������� ������� ������
truncate table test_tr;




declare

  i number := 1; 


begin 
  
  while (i < 1000000) loop 

  insert into test_tr values (i, 'test_' || i);
  i := i + 1;
  
   if (0 = mod(i, 1000)) then commit;
  -- ������ ���� ������������ ��� ���� ���: if (mod(i, 1000) = 0) commit;
    end if;
    
  end loop;

  end;



/*
 ������ #1
 ��������� �������� ������,
 ������������ ��� ������� �������� ����������� ����, ����� ���������� ������� ���� max,
 � ������������ ����, ����� ���������� ������� ���� min,
 � ����� ��������� �������� ���������� ������� �������� �� �� �����.
 */
with call as (
    select 'u1' as name, to_date('10.06.2022','dd.mm.yyyy') as e_date, 5 as event_cnt from dual
    union all
    select 'u1' as name, to_date('11.06.2022','dd.mm.yyyy')as e_date, 10 as event_cnt from dual
    union all
    select 'u1' as name, to_date('12.06.2022','dd.mm.yyyy')as e_date, 5 as event_cnt from dual
    union all
    select 'u2' as name, to_date('13.06.2022','dd.mm.yyyy')as e_date, 3 as event_cnt from dual
    union all
    select 'u2' as name, to_date('14.06.2022','dd.mm.yyyy')as e_date, 2 as event_cnt from dual
    union all
    select 'u3' as name, to_date('15.06.2022','dd.mm.yyyy')as e_date, 12 as event_cnt from dual
    union all
    select 'u3' as name, to_date('01.06.2022','dd.mm.yyyy')as e_date, 55 as event_cnt from dual
    union all
    select 'u3' as name, to_date('05.06.2022','dd.mm.yyyy')as e_date, 55 as event_cnt from dual
    union all
    select 'u3' as name, to_date('03.06.2022','dd.mm.yyyy')as e_date, 1 as event_cnt from dual
), -- select * from call;

-- ���������� ������� 

c1 as (
   select name,
       min(e_date),
       event_cnt 
     from call
     having max(event_cnt) in 
(
select 
       max (event_cnt)
     from call
     group by name  
)
group by name,  event_cnt), 

c2 as (
   select name,
       max(e_date),
       event_cnt 
     from call
     having max(event_cnt) in 
(
select 
       min (event_cnt)
     from call
     group by name  
)
group by name,  event_cnt), 

-- ������ ������ �������� ��� 3-����� ������� 
-- � ����� ��������� �������� ���������� ������� �������� �� �� �����.
c3 as (select name,
              sum(event_cnt) 
              from call
              group by name )

select * 
       from c1
       join c2 on c1.name = c2.name
       join c3 on c3.name = c2.name
;


-- ����� 2 ������ 
-- ������������ ����, ����� ���������� ������� ���� min,
-- �������
select name,
       max(e_date),
       event_cnt 
     from call
     having max(event_cnt) in 
(
select 
       min (event_cnt)
     from call
     group by name  
)
group by name,  event_cnt
;




-- ����� 1 ������ 
-- ������, ������������ ��� ������� �������� ����������� ����, ����� ���������� ������� ���� max

/*�������  */
select name,
       min(e_date),
       event_cnt 
     from call
     having max(event_cnt) in 
(
select 
       max (event_cnt)
     from call
     group by name  
)
group by name,  event_cnt
;


/*������� �� �� �����: �� ������� ����������� ������, � ������� ���� ������� 
�������� � ������ ���� ������� ����������� select. �� ��� ��� � U3 ��� ������� ������������,
 �� ��� ������� ��� ������ �� U3, ������� ���� �������� ������ ����������� 
 ��� ����� ������� � max ��������� 
*/
select name, e_date, event_cnt from call having max(event_cnt) in 
(
select 
       max (event_cnt)
     from call
     group by name  
)
group by name, e_date, event_cnt
;


 -- ��� �������� �������, ��� ��� ������ �� ������� ����� �����
select    name,
          min(e_date),
          max (event_cnt)
        from call
        group by name
   ;




---------------------------------------------------------------------------------

/*
     ������ 2 
1. ��������� ��������� ����������� ������� ������� � �������� �� ������� �� ������� ������
2. ���������� ����� ���������� ������� ����� ������ ����� ������������ ����������� ������
(������� - ����������)
 
*/ 

with ord as (select 1 as clien_id, 1 as product_id, to_date('03.06.2022', 'dd.mm.yyyy') as ord_date
             from dual
             union all
             select 2 as clien_id, 2 as product_id, to_date('03.04.2022', 'dd.mm.yyyy') as ord_date
             from dual
             union all
             select 2 as clien_id, 2 as product_id, to_date('03.03.2022', 'dd.mm.yyyy') as ord_date
             from dual
             union all
             select 2 as clien_id, 2 as product_id, to_date('03.06.2022', 'dd.mm.yyyy') as ord_date
             from dual
             union all
             select 2 as clien_id, 2 as product_id, to_date('03.04.2022', 'dd.mm.yyyy') as ord_date
             from dual
             union all
             select 2 as clien_id, 1 as product_id, to_date('03.03.2022', 'dd.mm.yyyy') as ord_date
             from dual
             union all
             select 1 as clien_id, 1 as product_id, to_date('03.03.2022', 'dd.mm.yyyy') as ord_date
             from dual
             union all
             select 1 as clien_id, 1 as product_id, to_date('03.03.2022', 'dd.mm.yyyy') as ord_date
             from dual
             union all
             select 1 as clien_id, 2 as product_id, to_date('03.03.2022', 'dd.mm.yyyy') as ord_date
             from dual
             union all
             select 1 as clien_id, 1 as product_id, to_date('03.04.2022', 'dd.mm.yyyy') as ord_date
             from dual
             union all
             select 1 as clien_id, 1 as product_id, to_date('03.06.2022', 'dd.mm.yyyy') as ord_date
             from dual
             union all
             select 1 as clien_id, 1 as product_id, to_date('03.06.2022', 'dd.mm.yyyy') as ord_date
             from dual
             union all
             select 1 as clien_id, 1 as product_id, to_date('03.06.2022', 'dd.mm.yyyy') as ord_date
             from dual
             union all
             select 1 as clien_id, 1 as product_id, to_date('03.06.2022', 'dd.mm.yyyy') as ord_date
             from dual), 
            -- select * from ord


-- ������� 2 �����  
-- 2. ���������� ����� ���������� ������� ����� ������ ����� ������������
-- ����������� ������ (������� - ����������)

month_ord as
          (select clien_id id,
                  to_char(ORD_DATE,'mm') month_n,
                  count(*) cnt_ord
               from ord
               group by clien_id,to_char(ORD_DATE,'mm')
               order by to_char(ord.ORD_DATE,'mm'),clien_id)

select month_ord.* ,
       nvl((cnt_ord-lag(cnt_ord) over(partition by id order by month_n)),0) diff
     from month_ord
     order by month_n,id




-- ������� 
-- 1. ��������� ��������� ����������� ������� ������� � �������� �� ������� �� ������� ������

itog as ( select product_id ,
       to_char(ord_date, 'mm') as months,
       count(product_id) as cnt
  from ord  
 where product_id = 1
 group by to_char(ord_date, 'mm'), product_id
 order by to_char(ord_date, 'mm') )
 
select i.*,
       nvl(i.cnt - lag(i.cnt) over (order by i.months), 0) as res
  from itog i
  /* 
  nvl ����� ��� ����, ����� �������� 0 ����, ��� �������� ������ ����� ������ null
  */
 ;



-- �� ���� � �������: ��� ������ ������� ���������� ������ �������� �� �������

select       product_id,
             to_char(ord_date, 'mm') as month,
             count(product_id) as cnt
          from ord
       where product_id = 1
       group by to_char (ord_date, 'mm'), product_id
       order by to_char (ord_date, 'mm')

-- to_char(ord_date, 'month')  ���� ��� ���������� �������� ������ 
;



