---------------------------------------------------------------------


-- �������� ������ 


----------------------------------------------------------------------
-- ������: ������� ���� ����������� � ��������� ���� �������
-- ��� ����� ������� ������ -- ������� �� ����������� 

select avg(e.salary) as avg_s
       from tb_project_employees e;
-- ����� 24939,94673 

-- ������ ������ ����� ��������� ������ 
select *
       from tb_project_employees e1
    where e1.salary >
           (
     select avg(e.salary) as avg_s
       from tb_project_employees e);
       
       
  select *
       from tb_project_employees e1
    where e1.salary >     24939.94673 
--������    
/*
������: ������� ���� ����������� � ����� ����������, � ������� �������� �����
������������������ ��������� (�� ������ boss)
���������:
���������� ������������ �������� (�������� boss);
������� ��������� ���������� � ������������ ��;
������� ����������� � ���� ����������;  */

select *
      from tb_project_employees e3
  where e3.position_v = 
  (
             select e2.position_v
                    from tb_project_employees e2
                 where e2.salary = 
                 ( 
                       select  max(e1.salary)
                       from tb_project_employees e1
                       where e1.position_v != 'boss' )
                       );


/*
������: 
������� ���� �����������, � ��������� ������� �������� ������
������ �������� 
             ���������
       ������� ���������, � ������� ����������� ������ ������
       �� ����������� ������ ���������� ������� ���� �����������*/

select e.position_v
     from tb_project_employees e
group by e.position_v
    having count(*) > 1   

 /* ����� (clerc    manager ) 

��� ��� ��������: 
������� ���������� ������ ���� �������, ����� ������ ����� ������� ����� group by, 
� �� ���� ������� �������� ���������� ������� � select ��� ����������� ������ �� �������� ����������,
��� �������� ��������� ������ 1 ���� �����������. 
�.�. HAVING  ��������� ��� ����������� ��������� �������� ���������, ������� ����������� � �������
 1 ��� ��� �� ����������� ������.
*/

select * 
        from tb_project_employees e1
     where e1.position_v in  
     (
select e.position_v
     from tb_project_employees e
group by e.position_v
    having count(*) > 1  )

 /*  
��� ��� ��������: 
������� ���������� ��������� ����������� ������ �������� � ������ � �������������('clerc','manager'),
 � �������� � �������� WHERE IN 
  ������ ���� ����� ������ ���� */ 
select * 
        from tb_project_employees e1
     where e1.position_v in ('clerc','manager');

/*
������: ����������� ������
������ ���� ������� ���� �����������, � ��������� ������� ��������
���� �������*/

select * 
        from tb_project_employees e1
     where e1.position_v not in  
     (
select e.position_v
     from tb_project_employees e
group by e.position_v
    having count(*) > 1  )
-- NOT IN ������� �������� �������, � �� �������� ������ �� ������, ��������� �. ������������
-- � ����� ��������� 


-- ��������������� ����������
/*������:
������� ����������� � ��������� ������� �������� � ������
���������       */

select e.*,
       (select  avg (e1.salary)
                from tb_project_employees e1
             where  e1.position_v = e.position_v
        )as avg_positione
     from tb_project_employees e 
/* ��� ��� ��������: 
�� ���������� �������  � ����������� ������ ������� ���������� ������� 
� ������� ��������, � ���, ��� ���� � �� �� ������� - ����� ������� �� 
��� ���� �������. ����� ������, � ������ self join �������, �� � �������� ������ 
������� ����������� ������� �������. 
����� �� ������� select ����� ��� ������ + ����� ������� avg
*/


-- ��������������� ���������� (� ������� from � join) 
 
select res.avg_e,
       res.posit
          from 
          (
       
    select avg(e1.salary) as avg_e,
           sum(e1.id) as sum_e,
           e1.position_v as posit
               from tb_project_employees e1
       where e1.position_v = 'clerc'
       group by e1.position_v
       )               res

/* ��� ��� ��������? 
�� ���������� ������� ������ ���� ������� RES �� ��������� 
- avg_e
- sum_e
- posit
� ����� �� ������� ������� ������ res.avg_e, res.posit ���� �������� RES
 */

select avg(e1.salary) as avg_e,
           sum(e1.id) as sum_e,
           e1.position_v as posit
               from tb_project_employees e1
       where e1.position_v = 'clerc'
       group by e1.position_v
-- � ������� ����  sum(e1.id) ������ ���������� ID-���

---------------------------------------------------------------------


-- �������� ������            ����������   ����� 2


----------------------------------------------------------------------
-- ������: ������� ���� �����������, �� ������� ������ ����� �������� � ���������� clerc
-- �������: ����� ������ ��, ������� ������ ����� ������ �� ���������� select, �.�. �� ����� ������ ����������� ��, 
-- ������ �������� � ����� 
select *
from tb_project_employees emp1
where emp1.salary > any
(select emp.salary from tb_project_employees  emp where emp.position_v =
'clerc')


-- �������� ����� ALL
-- ������ ������� ���� �����������, �� ������� ������ ���� ����������� � ���������� manager

-- �������: � ���� �� ���������� select �� �������� �� ���������� �������������, � ����� ��� ������ �����
-- ����� �������  > all ���������� ������ ��� ������������ �������� ������ ����������� select 

select *
from tb_project_employees emp1
where emp1.salary > all
(select emp.salary from tb_project_employees emp where emp.position_v =
'manager')

 
--������� EXISTS
--������: ������� ���� ��������, � ������� ���� ������������ �� ��� ���������

-- �������: EXISTS ����� ��� ����, ����� ��������� ���������� �� ��� ��������� �����-���� ���������. 
-- ������ ���������� select ������ � �������. ������ � join, �� � ��� �����������, ��� �� ������� select �� �� 
-- ������ �������� ������ �� ������� ����������� seltct 

select *
from tb_client cl
where exists
          (select *
    from TB_STUFF st
    where st.id = cl.stuff_id)

-- ����: ���� exists ������ �� ������, ������ � ������� ������ �� ������ 

-- ������ UNION ALL ����� ���������� ����� �������� ������    
--������:  ������� � ������� ������� �������� (��� + ������� � ����� ������) , � ����� ����� ���������� �������� 
-- �����������, �  ������� ����� ������� ���� �������� '����� ����������'

-- �������: 

select cl.first_name_v || ' ' || cl.second_name_v,
       null as salary
    from tb_client cl 
union all 
select '--����� ����������--',
       null 
       from dual 
union all 
select st.first_name_v,
       st.salary_n
      from tb_stuff st;


-- ��� ������� ������ � union all 
select cl.first_name_v || ' ' || cl.second_name_v
      from tb_client cl 
 union all 
 select '--------------------------' from dual -- select null from dual   ������ NULL ����� �������� 
 union all 
select st.first_name_v
      from tb_stuff st;


-- ������ ���� �� ��������
-- ��������   � ����� �������:  
-- 1 ������� (id �������, ����� ����� ��� ���������� ����� stuff) 
-- 2 ������� (����� �������, ����� ����� ��� ���������� ����� client)

select  to_char(cl.id),
        cl.first_name_v
    from tb_client cl
union all 
select  st.first_name_v,
        to_char (st.id)
    from tb_stuff st 


/* 
  ������: 
������� ��� �������, � ������� ���������� ��������� � �������.
 1 - � �������� ������ ����������� �� ����������� � id ������ 3, 5, 9
 2 - � ���������� ��������� � ����� ������ 3� �������� 
*/ 

-- ������� 
/* 
1 - ������� ������� ��� ��� �������: ����������, �������, ����������.
2 - � where ������ ��� ��� � �������, ����� ��������� select ������  ����������� 
�� ����� ����������� � id (3, 5, 9)
����� � where ������ ��, ������� ������ ��� �� � select 
� � ����� ������� ��������, ����� ����� ���� ������ 3
*/ 

select pr.name 
   from tb_projects_for_empl pr
   join tb_empl_project_associate ass on ass.project_id = pr.id
   join tb_project_employees e on e.id = ass.empl_id
  where e.state_v = 'onHoliday' 
            and e.salary > ( select min(e1.salary)
                                    from tb_project_employees e1
                                  where e1.id in (3, 5, 9)
                            )
            and length(e.name_v) > 3

 


--------------------------------------------------------------------------
-- � � � � � � � �       � � � � � � 

--------------------------------------------------------------------------

-- �������� ������ ����������, ����� 1

/* ������ 1
������� ���� �����������, � ������� �������� ������ ���� ����� 80% �� �������� ���������� �
���������� ����. ������� ������� �� 0,5 �� ��, ����� ����� ����-�� �������. 
 */

select *
       from tb_project_employees e1
   where e1.salary >= 0.5* (
             select e.salary
                from tb_project_employees e
                   where e.position_v = 'boss' )
  and e1.position_v != 'boss'
     

/* ������ 2     
������� ����� ���� ��������, ������� �������� ����� �� ����� ��� ������ � ID = 72 (���� �������
���������, ����������� ������� ���� �����, ID ������� ����� ��������)
�������: 
� select 1 ������ � ����� ��������, ��� id ��������� � ���������   select 2.
� select 2 ������ id ��������, ������� �������� ������� � id 4 (��� ����������� id ��������
������� �������� ������� � id 72 ). 
� select 3 ���� ����������� ����� id ��������, �. �������� �������� � id 72.
 */

select cl.*
       from tb_client cl
   where  cl.id in 
   (
select o.client_id
       from TB_ORDER o
   where  o.product_id =  
             (
       select min(o.product_id)
            from TB_ORDER o
          where  o.client_id = 72 ) )
   


/* ������ 3     
����� ��������� ����� �����������, ��� ���������� ������� ���������� ����� ������������
�������: 
 
����� ��� ���������� select ������� "result" � �� ������� select
� select 1 ������ ������� � ������� ��� ������ �������
� select 2  ������ max �������� ��� ������� result.av_empl_s   �� ������  select 1.
� select 3 ������ �������� �������, �� �������� ����� max �������� ��, ����������� � ������ select 2.
�����: boss
*/

select e2.position_v
       from tb_project_employees e2
    where e2.salary = 
    (
    select   max(result.av_empl_s)
        from (
             select e.position_v          as empl_pos, 
                 avg(e.salary)         as av_empl_s
               from tb_project_employees e
              group by e.position_v ) result );
 
 


/* ������ 4
����� ������� ����� �������� ���������� �������� ����������� � ���������� �������� �
���������� �����.
�������: ����� ������� ���������, ����� ��������� ������ ����. 
������ �� ����� ������� ��� ��� ������: 
select (10 - 9) as res_10_9 from dual;

� ������� ������� ��� ��� ��� ��� ������:    
select (clerc_salary - manager_salary) as result_clerc_manager from dual;

����������, ������ ����� ��������� ��� ����� ������
� select 2   
 */

select 
 ( select  result.av_sal
   from (
     select  e.position_v         as emp_pos,
            avg(e.salary)        as av_sal
        from tb_project_employees e
      where e.position_v in ('clerc', 'manager')
      group by e.position_v ) result
     where result.emp_pos = 'clerc' ) 
  -
  -- ��� ��� ���� ����� ���� ����� "-"
               (
                 select  result.av_sal
                    from (
                         select  e.position_v         as emp_pos,
                                avg(e.salary)        as av_sal
                            from tb_project_employees e
                          where e.position_v in ('clerc', 'manager')
                          group by e.position_v ) result
                         where result.emp_pos = 'manager' ) 
as result_clerc_manager from dual;
                         
 

--------------------------------------------------------------------------
-- � � � � � � � �       � � � � � � 

--------------------------------------------------------------------------
-- �/�   ����������/join

/* ��������� ���������� � join ���� ��������:
������ 1     
������ �������� � ��������� ����� �������� (� �����) �� �����������, ������� ������
������ �� ���� ��������  */

-- ������� join 

select pr.name,
       sum(e.salary),
       count(e.id)
             from tb_project_employees e 
       join tb_empl_project_associate ass on ass.empl_id = e.id
       join tb_projects_for_empl pr on pr.id = ass.project_id
       group by pr.name


-- ������� ����������� 
-- ���������: � select ����� �������� ������ � ���� �����: in (3,10)
select pr.*, 
   (
      select sum(e.salary) from tb_project_employees e where e.id 
           in  (select ass.empl_id from  tb_empl_project_associate ass where ass.project_id = pr.id)
    ) as salary_sum,
      (
    select count(e1.id) from tb_project_employees e1  where e1.id 
           in ( select ass1.empl_id from tb_empl_project_associate ass1 
      where ass1.project_id = pr.id)) as count_empl
from tb_projects_for_empl pr;


-- ���������. �������� ������ ���������� select, ��� ��������� ������ ������,
-- ������� ����� �������� � in (___) 
select ass.empl_id
 from tb_empl_project_associate ass
  where ass.project_id = 1

select sum(e.salary) from tb_project_employees e
 where e.id in (select ass.empl_id from tb_empl_project_associate ass where ass.project_id = 1)


-- ���������. �������� ��� �������� � ����� ���, ����� ��������� ����� ����
select *
        from tb_project_employees e 
       join tb_empl_project_associate ass on ass.empl_id = e.id
       join tb_projects_for_empl pr on pr.id = ass.project_id



/* ������ 2
������� ������ ������� (����) � ��������� �������������� ������� �� ���� �����������
������� ������  */
-- ������� �����������  

select dep.*,
       (
       select avg(e.salary)
        from tb_project_employees e 
             where e.dep_id = dep.id) as avg_salary 
from tb_department_empl dep
     order by dep.id;


         
-- ������� ����� join         
select d.id              as d_id,
       d.name            as d_name,
       count(e.name_v)   as count_e,
       avg(e.salary)     as avg_s
         from tb_project_employees e
       join tb_department_empl d on d.id = e.dep_id
       group by d.name, d.id
       order by d.id;

-- ����������� (�� ����������) ������ ��������� + join  
select  new_tb.d_id,
        new_tb.d_name,
        count( new_tb.e_id) as count_e,        
        avg(new_tb.e_salary)  as avg_s
        from (select d.id        as d_id,
                     d.name      as d_name,
                     e.id        as e_id,
                     e.name_v    as e_name,
                     e.salary    as e_salary,
                     e.upper_id  as e_dep_id
             from tb_department_empl d
             join tb_project_employees e  on e.dep_id = d.id )  new_tb
         group by new_tb.d_name, new_tb.d_id
         order by new_tb.d_id;         
--------------------------------------------------------------------------------------

/*������:
������� ����������� � ��������� ������� �������� � ������
��������� */

select e.*,
       (select  avg (e1.salary)
                from tb_project_employees e1
             where  e1.position_v = e.position_v
        )as avg_positione
     from tb_project_employees e 
/* ��� ��� ��������: 
�� ���������� �������  � ����������� ������ ������� ���������� ������� 
� ������� ��������, � ���, ��� ���� � �� �� ������� - ����� ������� �� 
��� ���� �������. ����� ������, � ������ self join �������, �� � �������� ������ 
������� ����������� ������� �������. 
����� �� ������� select ����� ��� ������ + ����� ������� avg
*/


--------------------------------------------------------------------------
-- � � � � � � � �       � � � � � � 

--------------------------------------------------------------------------
-- �/�   ����������. ����� 2

/*   
1. (������� �� �����, ������ ��� ����) ������������� ������ �� Excel ����� � �� 
  �������, �/� ����� � ������������ ��  */


/* 2.1 (������������) � ������� ���������� (ALL) ������� ��� �������, ���������
������ ���� �������, ��������� ������� ������ 5000 (����� ����� �������� ����
������ �� �����������) */

-- ������ � ������, ������� 192 ������                            
select  second_name_v, quantity_n*price_n SUM_ORDER
       from tb_order o
       join tb_product pr on pr.id=o.product_id
       join tb_client cl on cl.id=o.client_id
    where quantity_n*price_n > all 
                (select quantity_n*price_n 
            from tb_order o
            join tb_product pr on pr.id=o.product_id
            where quantity_n*price_n < 319000)

-- ������ � id, ������� 192 ������                                              
select cl.id, quantity_n*price_n SUM_ORDER
       from tb_order o
       join tb_product pr on pr.id=o.product_id
       join tb_client cl on cl.id=o.client_id
    where quantity_n*price_n > all 
                (select quantity_n*price_n 
            from tb_order o
            join tb_product pr on pr.id=o.product_id
            where quantity_n*price_n < 319000)


--------- ���� �� ������ ������� ���� ������, ����� ����������� ��� ����� 
-- ������ ��� + �����
select cl.first_name_v, 
       sum(pr.price_n)
     from tb_order o
       join tb_client cl on cl.id = o.client_id
       join tb_product pr on pr.id = o.product_id
       having sum(pr.price_n)  > all
            (select sum(pr.price_n)
                   from tb_order o
                   join tb_client cl on cl.id = o.client_id
                   join tb_product pr on pr.id = o.product_id
                   having sum(pr.price_n) < 9500500
                   group by cl.first_name_v)
                   group by cl.first_name_v 
 
/* ������ � id 
��, �� ������, ��� 9 �������, � �� 23.
� ��� ��� 2 ������ �� � ����, ��� ������ ��� 9 ��� 500 ��� 500, � �� ����� ������ ��� ���, 
�.�. ������ 9 ��� 500 ��� 500.  �������� �����������. */
 select cl.id, 
       sum(pr.price_n)
     from tb_order o
       join tb_client cl on cl.id = o.client_id
       join tb_product pr on pr.id = o.product_id
       having sum(pr.price_n)  > all
            (select sum(pr.price_n)
                   from tb_order o
                   join tb_client cl on cl.id = o.client_id
                   join tb_product pr on pr.id = o.product_id
                   having sum(pr.price_n) < 9500500
                   group by cl.first_name_v)
                   group by cl.id
 
 -- ��� ��� cl.id ��������� ������ ���� ������ id 36 ����� 20 ��������� 
 select cl.id, 
       sum(pr.price_n)
     from tb_order o
       join tb_client cl on cl.id = o.client_id
       join tb_product pr on pr.id = o.product_id
       having sum(pr.price_n)  > all
            (select sum(pr.price_n)
                   from tb_order o
                   join tb_client cl on cl.id = o.client_id
                   join tb_product pr on pr.id = o.product_id
                   having sum(pr.price_n) < 18500500
                   group by cl.first_name_v)
                   group by cl.id
                                     
 
 -- ��� ��� cl.first_name_v ���������  10 ������� 
 select cl.first_name_v, 
       sum(pr.price_n)
     from tb_order o
       join tb_client cl on cl.id = o.client_id
       join tb_product pr on pr.id = o.product_id
       having sum(pr.price_n)  > all
            (select sum(pr.price_n)
                   from tb_order o
                   join tb_client cl on cl.id = o.client_id
                   join tb_product pr on pr.id = o.product_id
                   having sum(pr.price_n) < 18500500
                   group by cl.first_name_v)
                   group by cl.first_name_v

--------- ����  �� ������ ������� ���� ������, ����� ����������� ��� ����� 
       
/*2.2 (������������) � ������� ���������� (EXISTS) ������� ���� �����������, �
������� ���������� ���� �� ���� ����� (���� �� ���� ������ � ������� �������, �
���� � ����������� ������� ���)
*/

select  *from tb_stuff st
where exists (select *from tb_client cl where exists(
select  *from tb_order o where cl.id=o.client_id and stuff_id=st.id));

-- id ���� 4
-- id ���������� 7
-- � ������ � ��� ��� ���� id, ������ ��� ����� 

select  *from tb_position


/* 3. (������������) ��������� ���� ������ (union all) ������� ������ ��
��������, ���������, ����������� � ������� (id, name)
������������ ����������� � ���� ������ ������, ��������� �������� ������ */
 
select cl.id,
       cl.first_name_v  
      from tb_client cl 
union all
select null,
        '����� ������ �������' from dual  
union all
select null, 
      '����� ������ ��������' from dual
union all 
select pr.id,
       pr.name_v
       from tb_product pr
union all
select null, 
      '����� ������ ����������' from dual
union all  
select st.id,
       st.first_name_v
      from tb_stuff st
union all 
select null, 
      '����� ������ ������' from dual
union all 
select  co.id,
          co.name_v
      from tb_country co
      ;

--- ����� ���� ������ 
SELECT 
TO_CHAR(id) id,
surname_v,
first_name_v,
second_name_v,
TO_CHAR(birth_date_d) birth_date,
TO_CHAR(birth_place_id) birth_place_id,
TO_CHAR(gender_id) gender_id,
TO_CHAR(nationality_id) nationality_id,
TO_CHAR(stuff_id) stuff_id
FROM tb_client

UNION ALL
SELECT ' ',' ',' ',' ',' ',' ',' ',' ',' '
FROM DUAL
UNION ALL

SELECT 'id','product_name','price',' ',' ',' ',' ',' ',' '
FROM DUAL
UNION ALL

SELECT 
TO_CHAR(id),
name_v,
TO_CHAR(price_n),
null,null,null,null,null,null
FROM tb_product

UNION ALL
SELECT ' ',' ',' ',' ',' ',' ',' ',' ',' '
FROM DUAL
UNION ALL

SELECT 'id','surname','first_name','last_name','salary','position_id',' ',' ',' '
FROM DUAL
UNION ALL

SELECT 
TO_CHAR(id),
surname_v,
first_name_v,
last_name_v,
TO_CHAR(salary_n),
TO_CHAR(position_id),
null,null,null
FROM tb_stuff

UNION ALL
SELECT ' ',' ',' ',' ',' ',' ',' ',' ',' '
FROM DUAL
UNION ALL

SELECT 'id','country_name',' ',' ',' ',' ',' ',' ',' '
FROM DUAL
UNION ALL

SELECT 
TO_CHAR(id),
name_v,
null,null,null,null,null,null,null
FROM tb_country
 

      
----------------------------------------------------------------------------------
-- ������� ��������� ���  ���� projects
select * from  tb_project_employees;

select * from  tb_projects_for_empl;

select * from  tb_department_empl;

select * from  tb_empl_project_associate;

----------------------------------------------------------------------------------

select rowid, ass.* from tb_empl_project_associate ass

select rowid, e.* from tb_project_employees e


-- ������� ��������� ��� �������� ���� 
select * from  tb_client;
select * from  TB_PRODUCT;
select * from tb_order_type;
select * from  TB_ORDER;
select * from  TB_STUFF;
-- id ���� 4 � ������� position_id

select * from  TB_POSITION;

select * from  tb_country;

select * from  tb_gender;

select * from  tb_nationality;

select * from TB_ADDRESS;

select * from  tb_address_type;
