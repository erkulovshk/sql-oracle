-- FUNCTION: core.generate_nb_transaction_by_appl_detail(integer, boolean)

-- DROP FUNCTION IF EXISTS core.generate_nb_transaction_by_appl_detail(integer, boolean);

CREATE OR REPLACE FUNCTION core.generate_nb_transaction_by_appl_detail(
	arg_doc_id integer,
	arg_reserve boolean)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$


declare
	--09.11.2018
	--Руслан, Акжол, Тилек
	v_ec_nerasp integer;
	v_gl_ec integer;
	v_bc_ec integer;
	v_parent integer;
	v_credit integer;
	v_2824 boolean;
	v_debit integer;
	v_param_casplan integer;
	v_param_estimate integer;
	v_cp_record record;
	v_recipient_record record;
	v_conclusion_record record;
	v_doc_record record;
	ck_rok integer;
	acc_type integer;
	result integer;
	v_bc_record record;
	var_row record;
	var_ed integer ;
	v_bol_amount numeric(18,2);
	var_left_amount numeric(18,2);
BEGIN

	select d.* into v_doc_record from public.document as d where d.id = arg_doc_id;
	--FOR v_cp_record IN ()

	select
		ad.payment_amount as amount,
		ad.econom_classifier_to as ec_to,
		ad.recipient_bank as recipient_bank,
		ad.recipient_acc as recipient_acc,
		doc.confirm_date::date as confirm_date,
		doc.doc_date as doc_date,
		rok.id as s_rok_id,
		rok.code as s_rok_code,
		bias.id as s_bia_id,
		bia.code as s_bia_code,
		bias.account_type as s_acc_type,
		ac.code as s_acc_type_code,
		--pp.id as es_gr,
		ec.id as ec,
		ec.code as ec_code,
		ec_to.code as ec_to_code,
		ec.name as ec_name,
		vc.code as vc_code,
		fc.code as fc_code,
		ad.budget_comitment as bc_id,
		sd.econom_classifier as sd_ec,
		db.id as budget_bia,
--		bbia.account_type as budget_account_type,
		d.parent as budget_rok
		into v_cp_record
	FROM
		public. document doc
		INNER JOIN appl_detail as ad on doc.id = ad.document
		INNER JOIN dict bia on bia.id = ad.budget_institution_account
		INNER JOIN budget_institution_account as bias on bias.id = bia.id
		INNER JOIN dict bi on bia.parent = bi.id
		INNER JOIN dict vc on bias.vedom_classifier = vc.id
		INNER JOIN dict fc on bias.function_classifier = fc.id
		inner join dict ac on ac.id = bias.account_type
		INNER JOIN dict rok on bi.parent = rok.id
		INNER JOIN dict as ec on ec.id = ad.econom_classifier
		LEFT JOIN dict as ec_to on ec_to.id = ad.econom_classifier_to
		LEFT JOIN special_directive as sd on sd.id = ad.special_directive
		inner join dict as d on bias.budget_id = d.id
		inner join budget_institution_account as bbia on bbia.budget_id = d.id
		inner join dict as db on db.id = bbia.id and db.parent = 36585
		--inner join dict as pp on pp.code = substring(ec.code, 1, 4) and pp.type = ec.type
	where doc.id = arg_doc_id;

	-- Размещение 
	IF v_cp_record.s_bia_code in ('4401001101026204') THEN 
		RETURN result;
	END IF;

	-- Проверка на смету доходов
		IF (v_cp_record.ec_code like '1423%' or v_cp_record.ec_code like '1441%') AND (v_cp_record.s_acc_type_code like '%20' AND v_cp_record.s_acc_type_code not like '2%')
			AND (v_cp_record.s_bia_code not in ('4402011103022821',
				'4403091103004449',
				'4405031103002833',
				'4408101103001151',
				'4402051103009922',
				'4402011103000892',
				'4402041103008524',
				'4407011103006405',
				'4402011103009279',
				'4402061103001014',
				'4402051103001030',
				'4402041103006201',
				'4402011103022821',
				'4402011102005937',
				'4406041103008021',
				'4402011102005937',
				'4405011103011252',
				'4402011103027871')
				AND NOT(v_cp_record.recipient_bank like '440%' and (substring(v_cp_record.recipient_acc,7,4)='1001' or substring(v_cp_record.recipient_acc,7,5)='10001'))
				)   THEN
			SELECT
				ed."id" into var_ed
				FROM
				"public".estimate AS est
				INNER JOIN "public".estimate_detail AS ed ON ed.estimate_id = est."id"
				INNER JOIN "public".document AS doc ON doc."id" = est.document_id
				WHERE
				est.bia_id = v_cp_record.s_bia_id
				AND ed.econom_classifier_id =  v_cp_record.ec
				AND doc.status = 100;
			IF var_ed is null THEN
				SELECT
				ed."id" into var_ed
				FROM
				"public".estimate AS est
				INNER JOIN "public".estimate_detail AS ed ON ed.estimate_id = est."id"
				INNER JOIN "public".document AS doc ON doc."id" = est.document_id
				INNER JOIN "public".dict AS ec ON ec.id = ed.econom_classifier_id
				WHERE
				est.bia_id = v_cp_record.s_bia_id
				AND ec.code =  '15111100'
				AND doc.status = 100;

				IF var_ed is null THEN
				-- Не найдена в смете

				 RAISE EXCEPTION 'По данному коду у отправителя отсутствует смета!';
				END IF;
			END IF;

		END IF;

	v_2824=true;
	--Проверка на больничный лист
	IF v_cp_record.ec_code in ('27212100', '27212200', '27212300','28241100') THEN
		SELECT SUM(dt_amount) into v_bol_amount
		FROM
			core.transaction tr
			INNER JOIN core.tr_param trp ON tr.tr_param_id = trp.id
		WHERE
			trp.acc_type = v_cp_record.s_acc_type AND
			trp.rok = v_cp_record.s_rok_id AND
			trp.bia = v_cp_record.s_bia_id AND
			trp.ec = v_cp_record.ec;
		IF v_bol_amount*(-1) >= v_cp_record.amount then
			IF v_cp_record.ec_code in ('28241100') THEN
				v_2824=false;
			ELSE
				RETURN result;
			END IF;
		END IF;
	END IF;

	IF v_2824=true THEN
		IF v_cp_record.fc_code = '70942' AND v_cp_record.vc_code IN ('23220','39120','44121','34121') THEN
			--спец & vc 23220 на 2219
			IF (v_cp_record.s_acc_type_code like '%20' AND v_cp_record.vc_code <> '34121') OR v_cp_record.vc_code = '23220' THEN
				SELECT id INTO v_gl_ec FROM dict WHERE code = '2219' AND type = 2;
			ELSIF (substr(v_cp_record.ec_code,1,4) in ('2218','2821') AND v_cp_record.vc_code IN ('44121','34121'))OR
				(substr(v_cp_record.ec_code,1,4) in ('2821') AND v_cp_record.vc_code IN ('39120')) THEN
				SELECT id INTO v_gl_ec FROM dict WHERE code = substr(v_cp_record.ec_code,1,4) AND type = 2;
			ELSE
				SELECT id INTO v_gl_ec FROM dict WHERE code = '2219' AND type = 2;
			END IF;
		ELSIF
			((v_cp_record.vc_code = '36131' OR v_cp_record.vc_code = '36141' OR v_cp_record.vc_code = '36220' )AND v_cp_record.fc_code <> '70761')
			OR ((v_cp_record.vc_code in ('37121','37131') AND v_cp_record.fc_code = '70743') AND
				v_cp_record.s_bia_code not in ('4407011101094289','4407011102004776','4404071101036444',
				'4404071102002097','4402011101101918','4402011102009775',
				'4405031101014333','4405031102000804','4402011101101716',
				'4402011102009674','4402011101101918','4402011102009876')) THEN
			IF 	v_cp_record.ec_code not like '2711%' THEN
				SELECT id INTO v_gl_ec FROM dict WHERE code = '2216' AND type = 2;
			ELSE
				v_gl_ec = v_cp_record.ec;
			END IF;
		ELSE
			v_gl_ec = v_cp_record.ec;
		END IF;

		IF v_cp_record.s_rok_code = '440206' AND v_cp_record.ec_code in ('26311200','26311300')  THEN
		-- Ссуды БИШКЕК
		--нераспределенный красным
			SELECT dict.id INTO acc_type FROM public.dict AS dict WHERE dict.code = 'NB_SM' and dict.type = 5;
				-- Ссуды БИШКЕК
				--нераспределенный красным
			--	IF v_cp_record.ec_code in ('26311300') THEN
			--		SELECT dict.id INTO v_ec_nerasp FROM public.dict AS dict WHERE dict.code = '15111100' and dict.type = 2;
			--	ELSE
					v_ec_nerasp = v_cp_record.sd_ec;
			--	END IF;
			v_credit = core.get_param_by_dicts(ARRAY[acc_type, v_cp_record.budget_rok, v_cp_record.budget_bia, v_ec_nerasp]);
				INSERT INTO core.transaction(tr_type, document_id, dt_amount, kt_amount, dt_cnt, kt_cnt, doc_date, tr_param_id, dt_cy_amount, kt_cy_amount, parent) VALUES
					--(2, arg_doc_id, 0,v_cp_record.amount*(-1), 0, 0, v_cp_record.confirm_date, v_credit, 0,v_cp_record.amount*(-1), v_parent) returning id into v_parent;
					(iif(arg_reserve,3,2), arg_doc_id, 0,iif(arg_reserve,0.00,v_cp_record.amount*(-1)), 0, 0, v_cp_record.confirm_date, v_credit,0,iif(arg_reserve,v_cp_record.amount*(-1),0.00), v_parent);

		ELSIF (v_cp_record.s_acc_type_code like '%10' OR (v_cp_record.s_acc_type_code like '%20' AND v_cp_record.ec_code not like '14%')) 
			AND v_cp_record.s_bia_code not in ('4401001101026204') -- 
				THEN
			-- расход распределенный счет + ФОМС, спец
			IF (v_cp_record.s_acc_type_code not like '2%') THEN
				FOR var_row IN (
					select
						coalesce(adp.amount,ad.payment_amount) as amount,
						ad.econom_classifier_to as ec_to,
						ad.recipient_bank as recipient_bank,
						ad.recipient_acc as recipient_acc,
						doc.confirm_date::date as confirm_date,
						doc.doc_date as doc_date,
						rok.id as s_rok_id,
						rok.code as s_rok_code,
						bias.id as s_bia_id,
						bias.account_type as s_acc_type,
						ac.code as s_acc_type_code,
						adp.program_classifier_id as pc_id,
						--pp.id as es_gr,
						ec.id as ec,
						ec.code as ec_code,
						ec_to.code as ec_to_code,
						ec.name as ec_name,
						vc.code as vc_code,
						fc.code as fc_code,
						ad.budget_comitment as bc_id,
						sd.econom_classifier as sd_ec,
						db.id as budget_bia,
				--		bbia.account_type as budget_account_type,
						d.parent as budget_rok
					FROM
						public. document doc
						INNER JOIN appl_detail as ad on doc.id = ad.document
						INNER JOIN dict bia on bia.id = ad.budget_institution_account
						INNER JOIN budget_institution_account as bias on bias.id = bia.id
						INNER JOIN dict bi on bia.parent = bi.id
						INNER JOIN dict vc on bias.vedom_classifier = vc.id
						INNER JOIN dict fc on bias.function_classifier = fc.id
						inner join dict ac on ac.id = bias.account_type
						INNER JOIN dict rok on bi.parent = rok.id
						INNER JOIN dict as ec on ec.id = ad.econom_classifier
						LEFT JOIN dict as ec_to on ec_to.id = ad.econom_classifier_to
						LEFT JOIN special_directive as sd on sd.id = ad.special_directive
						LEFT JOIN appl_detail_program_classifier adp on adp.appl_detail_id=ad.id
						inner join dict as d on bias.budget_id = d.id
						inner join budget_institution_account as bbia on bbia.budget_id = d.id
						inner join dict as db on db.id = bbia.id and db.parent = 36585
						--inner join dict as pp on pp.code = substring(ec.code, 1, 4) and pp.type = ec.type
					where doc.id = arg_doc_id)
				LOOP

				-- СМ
					SELECT dict.id INTO acc_type FROM public.dict AS dict WHERE dict.code = 'NB_EM' and dict.type = 5;
					v_debit = core.get_param_by_dicts(ARRAY[acc_type, var_row.s_rok_id, var_row.s_bia_id, core.get_ec_id_by_len(v_gl_ec,4),var_row.pc_id]);
					INSERT INTO core.transaction(tr_type, document_id, dt_amount, kt_amount, dt_cnt, kt_cnt, doc_date, tr_param_id, dt_cy_amount, kt_cy_amount, parent) VALUES
						(iif(arg_reserve,3,1), arg_doc_id, iif(arg_reserve,0.00,var_row.amount),0, 0, 0, var_row.confirm_date, v_debit, iif(arg_reserve,var_row.amount,0.00),0, v_parent);
					-- КП
					IF var_row.s_acc_type_code like '%10' AND var_row.s_acc_type_code <> '12510' THEN
						SELECT dict.id INTO acc_type FROM public.dict AS dict WHERE dict.code = 'NB_CP' and dict.type = 5;
						v_debit = core.get_param_by_dicts(ARRAY[acc_type, var_row.s_rok_id, var_row.s_bia_id,
						iif(var_row.s_acc_type_code like '12%',core.get_ec_id_by_len(v_gl_ec,4),core.get_ec_id_by_len(v_gl_ec,4)),var_row.pc_id]);
						INSERT INTO core.transaction(tr_type, document_id, dt_amount, kt_amount, dt_cnt, kt_cnt, doc_date, tr_param_id, dt_cy_amount, kt_cy_amount, parent) VALUES
							(iif(arg_reserve,3,1), arg_doc_id, iif(arg_reserve,0.00,var_row.amount),0, 0, 0, var_row.confirm_date, v_debit, iif(arg_reserve,var_row.amount,0.00),0, v_parent);
					END IF;
				END LOOP;
			END IF;

			-- БО
			IF v_cp_record.bc_id is not null THEN
				SELECT dict.id INTO acc_type FROM public.dict AS dict WHERE dict.code = 'NB_BC' and dict.type = 5;
				select bch.*,bcd.econom_classifier into v_bc_record  from budget_commitment_header bch
					inner join budget_commitment_detail bcd on bcd.budget_commitment_header=bch.id
						and (core.get_ec_id_by_len(v_cp_record.ec,4)=bcd.econom_classifier or core.get_ec_id_by_len(v_cp_record.ec,4)=bcd.econom_classifier)
				where bch.id=v_cp_record.bc_id;

				IF v_bc_record is null THEN
					select bch.*,bcd.econom_classifier into v_bc_record  from budget_commitment_header bch
						inner join budget_commitment_detail bcd on bcd.budget_commitment_header=bch.id
							and (v_gl_ec=bcd.econom_classifier)
					where bch.id=v_cp_record.bc_id;
				END IF;

				RAISE INFO '1--%, %, %, %, %',v_cp_record.ec,core.get_ec_id_by_len(v_cp_record.ec,4),core.get_ec_id_by_len(v_cp_record.ec,4),v_bc_record.id,v_bc_record.econom_classifier;
				v_debit = core.get_param_by_dicts(ARRAY[acc_type, v_cp_record.bc_id, v_cp_record.s_bia_id, v_bc_record.econom_classifier]);
				IF v_bc_record.type = 0 or v_bc_record.type = 1	THEN
					INSERT INTO core.transaction(tr_type, document_id, dt_amount, kt_amount, dt_cnt, kt_cnt, doc_date, tr_param_id, dt_cy_amount, kt_cy_amount, parent) VALUES
					(iif(arg_reserve,3,2), arg_doc_id, 0,iif(arg_reserve,0.00,v_cp_record.amount), 0, 0, v_cp_record.confirm_date, v_debit,0,iif(arg_reserve,v_cp_record.amount,0.00), v_parent);
				END IF;
				INSERT INTO core.transaction(tr_type, document_id, dt_amount, kt_amount, dt_cnt, kt_cnt, doc_date, tr_param_id, dt_cy_amount, kt_cy_amount, parent) VALUES
					(iif(arg_reserve,3,1), arg_doc_id, iif(arg_reserve,0.00,v_cp_record.amount),0, 0, 0, v_cp_record.confirm_date, v_debit, iif(arg_reserve,v_cp_record.amount,0.00),0, v_parent);
			END IF;
			-- Нераспределенный
			IF v_cp_record.s_acc_type_code like '%10' AND v_cp_record.s_acc_type_code <> '11010' THEN
				SELECT dict.id INTO acc_type FROM public.dict AS dict WHERE dict.code = 'NB_SM' and dict.type = 5;
				v_debit = core.get_param_by_dicts(ARRAY[acc_type, v_cp_record.budget_rok, v_cp_record.budget_bia, v_cp_record.sd_ec]);
				INSERT INTO core.transaction(tr_type, document_id, dt_amount, kt_amount, dt_cnt, kt_cnt, doc_date, tr_param_id, dt_cy_amount, kt_cy_amount, parent) VALUES
					(iif(arg_reserve,3,1), arg_doc_id, iif(arg_reserve,0.00,v_cp_record.amount),0, 0, 0, v_cp_record.confirm_date, v_debit, iif(arg_reserve,v_cp_record.amount,0.00),0, v_parent);
			END IF;

		END IF;
		-- спец, депозит
		IF v_cp_record.s_acc_type_code like '%20' or v_cp_record.s_acc_type_code like '%30' THEN
			SELECT dict.id INTO v_gl_ec FROM public.dict AS dict WHERE dict.code = '15111100' and dict.type = 2;
			SELECT dict.id INTO acc_type FROM public.dict AS dict WHERE dict.code = 'NB_SM' and dict.type = 5;
			v_debit = core.get_param_by_dicts(ARRAY[acc_type, v_cp_record.s_rok_id, v_cp_record.s_bia_id, v_gl_ec]);
			INSERT INTO core.transaction(tr_type, document_id, dt_amount, kt_amount, dt_cnt, kt_cnt, doc_date, tr_param_id, dt_cy_amount, kt_cy_amount, parent) VALUES
				(iif(arg_reserve,3,1), arg_doc_id, iif(arg_reserve,0.00,v_cp_record.amount),0, 0, 0, v_cp_record.confirm_date, v_debit, iif(arg_reserve,v_cp_record.amount,0.00),0, v_parent);
		END IF;
	END IF;

 		-- Кредит
	IF v_cp_record.recipient_bank like  '440%' THEN
		-- ВКП МКП МПЦ
		-- находим получателя
		SELECT
			budget.id budget_id,
			brok.id as brok_id,
			brok.code as brok_code,
			bbia.account_type as bacc_type,
			bia.id bia_id,
			bbia.id as bbia_id,
			rok.id as rok_id,
			rok.code as rok_code,
			ac.id as ac_id,
			ad.conclusion_id,
			ac.code as ac_code INTO v_recipient_record
		FROM
			appl_detail ad
		INNER JOIN dict dbia on dbia.code = ad.recipient_acc and dbia.type=8
		INNER JOIN budget_institution_account bia on bia.id = dbia."id"
		INNER JOIN dict bi on dbia.parent = bi.id and bi.type=7
		INNER JOIN dict ac on ac.id = bia.account_type and ac."type"=5
		INNER JOIN dict rok on bi.parent = rok.id
		INNER JOIN dict budget on budget."id" = bia.budget_id
		INNER JOIN budget_institution_account as bbia on bbia.budget_id = budget.id
		INNER JOIN dict as bbi on bbi.id = bbia.id and bbi.parent = 36585
		INNER JOIN dict as brok on brok."id" = budget.parent
		WHERE ad.document=arg_doc_id;

		-- Проверка на смету доходов
		IF (v_cp_record.ec_to_code like '1423%' ) AND (v_recipient_record.ac_code like '%20' AND v_recipient_record.ac_code not like '2%')
			AND (v_cp_record.recipient_acc not in ('4402011103022821',
				'4403091103004449',
				'4405031103002833',
				'4408101103001151',
				'4402051103009922',
				'4402011103000892',
				'4402041103008524',
				'4407011103006405',
				'4402011103009279',
				'4402061103001014',
				'4402051103001030',
				'4402011102005937'))
				THEN
			SELECT
				ed."id" into var_ed
				FROM
				"public".estimate AS est
				INNER JOIN "public".estimate_detail AS ed ON ed.estimate_id = est."id"
				INNER JOIN "public".document AS doc ON doc."id" = est.document_id
				WHERE
				est.bia_id = v_recipient_record.bia_id
				AND ed.econom_classifier_id =  v_cp_record.ec_to
				AND doc.status = 100;
			IF var_ed is null THEN
				-- Не найдена в смете
				 RAISE EXCEPTION 'По данному коду у получателя отсутствует смета!';
			END IF;

		END IF;

		SELECT dict.id INTO v_gl_ec FROM public.dict AS dict WHERE dict.code = '15111100' and dict.type = 2;
		SELECT dict.id INTO acc_type FROM public.dict AS dict WHERE dict.code = 'NB_SM' and dict.type = 5;

		IF v_recipient_record.ac_code like '%20' OR v_recipient_record.ac_code like '%30' THEN
			--спец
--			IF not(v_cp_record.ec_to_code not like '1%' AND v_cp_record.ec_to_code not LIKE '3____1__') THEN
				-- INSERT INTO transaction_old(operation, "type", account_type, account, econom_classifier, amount, purpose, rok)
					-- VALUES(var_op_id, 2, get_old_account_type(var_row.rec_acc_type), var_row.rec_bia_id, var_row.ec_to, var_row.amount, var_row.descr, var_row.rec_rok);
			v_credit = core.get_param_by_dicts(ARRAY[acc_type,v_recipient_record.rok_id, v_recipient_record.bia_id, v_gl_ec]);
			INSERT INTO core.transaction(tr_type, document_id, dt_amount, kt_amount, dt_cnt, kt_cnt, doc_date, tr_param_id, dt_cy_amount, kt_cy_amount, parent) VALUES
			(iif(arg_reserve,3,2), arg_doc_id, 0,iif(arg_reserve,0.00,v_cp_record.amount), 0, 0, v_cp_record.confirm_date, v_credit,0,iif(arg_reserve,v_cp_record.amount,0.00), v_parent);
--			END IF;
		ELSIF (v_recipient_record.ac_code like '1%10' OR v_recipient_record.ac_code like '3%10') AND v_recipient_record.ac_code <> '11010' THEN
			raise info 'ВОЗВРАТ Б ec = % % % %', v_recipient_record.ac_id,v_recipient_record.rok_id, v_recipient_record.bia_id, v_cp_record.ec_to;
--			ВОЗВРАТ Б
			v_debit = core.get_param_by_dicts(ARRAY[acc_type,v_recipient_record.brok_id, v_recipient_record.bbia_id, v_gl_ec]);
			INSERT INTO core.transaction(tr_type, document_id, dt_amount, kt_amount, dt_cnt, kt_cnt, doc_date, tr_param_id, dt_cy_amount, kt_cy_amount, parent) VALUES
				(iif(arg_reserve,3,1), arg_doc_id, iif(arg_reserve,0.00,v_cp_record.amount*(-1)),0, 0, 0, v_cp_record.confirm_date, v_debit,iif(arg_reserve,v_cp_record.amount*(-1),0.00),0, v_parent);

		END IF;
		IF v_recipient_record.conclusion_id is not null THEN
			--estimate
			var_left_amount:= v_cp_record.amount;
			FOR var_row IN
				(select trp.id, tr.id, Sum(tr.dt_amount) as dt, Sum(tr.kt_amount) as kt
				from appl_detail ad
					inner join core.transaction tr on tr.document_id=ad.document
					inner join core.tr_param trp on trp.id=tr.tr_param_id
					inner join dict acc on acc.id=trp.acc_type
				  where
					acc.code like 'NB_EM' and (tr.dt_amount<>0 or tr.kt_amount<>0)-- and acc.code not like 'NB_SM'
					and ad.id=v_recipient_record.conclusion_id
					group by trp.id,tr.id
					order by tr.id)
			LOOP
				IF var_left_amount < var_row.dt AND var_left_amount>0 THEN
					 var_row.dt:= var_left_amount;
				END IF;
				IF var_left_amount >= var_row.dt THEN
					INSERT INTO core.transaction(tr_type, document_id, dt_amount, kt_amount, dt_cnt, kt_cnt, doc_date, tr_param_id, dt_cy_amount, kt_cy_amount, parent) VALUES
					(iif(arg_reserve,3,1), arg_doc_id, iif(arg_reserve,0.00,var_row.dt*(-1)),0, 0, 0, v_cp_record.confirm_date, var_row.id,iif(arg_reserve,var_row.dt*(-1),0.00),0, v_parent);
					var_left_amount:= var_left_amount -  var_row.dt;
				End IF;
			END LOOP;
			--kp
			var_left_amount:= v_cp_record.amount;
			FOR var_row IN
				(select trp.id, tr.id, Sum(tr.dt_amount) as dt, Sum(tr.kt_amount) as kt
				from appl_detail ad
					inner join core.transaction tr on tr.document_id=ad.document
					inner join core.tr_param trp on trp.id=tr.tr_param_id
					inner join dict acc on acc.id=trp.acc_type
				  where
					acc.code like 'NB_CP' and (tr.dt_amount<>0 or tr.kt_amount<>0)-- and acc.code not like 'NB_SM'
					and ad.id=v_recipient_record.conclusion_id
					group by trp.id,tr.id
					order by tr.id)
			LOOP
				IF var_left_amount < var_row.dt AND var_left_amount>0 THEN
					 var_row.dt:= var_left_amount;
				END IF;
				IF var_left_amount >= var_row.dt THEN
					INSERT INTO core.transaction(tr_type, document_id, dt_amount, kt_amount, dt_cnt, kt_cnt, doc_date, tr_param_id, dt_cy_amount, kt_cy_amount, parent) VALUES
					(iif(arg_reserve,3,1), arg_doc_id, iif(arg_reserve,0.00,var_row.dt*(-1)),0, 0, 0, v_cp_record.confirm_date, var_row.id,iif(arg_reserve,var_row.dt*(-1),0.00),0, v_parent);
					var_left_amount:= var_left_amount -  var_row.dt;
				End IF;
			END LOOP;
			--bc
			var_left_amount:= v_cp_record.amount;
			FOR var_row IN
				(select trp.id, tr.id, Sum(tr.dt_amount) as dt, Sum(tr.kt_amount) as kt
				from appl_detail ad
					inner join core.transaction tr on tr.document_id=ad.document
					inner join core.tr_param trp on trp.id=tr.tr_param_id
					inner join dict acc on acc.id=trp.acc_type
				  where
					acc.code like 'NB_BC' and (tr.dt_amount<>0 or tr.kt_amount<>0)-- and acc.code not like 'NB_SM'
					and ad.id=v_recipient_record.conclusion_id
					group by trp.id,tr.id
					order by tr.id)
			LOOP
				IF var_left_amount < var_row.dt AND var_left_amount>0 THEN
					 var_row.dt:= var_left_amount;
				END IF;
				IF var_left_amount >= var_row.dt THEN
					INSERT INTO core.transaction(tr_type, document_id, dt_amount, kt_amount, dt_cnt, kt_cnt, doc_date, tr_param_id, dt_cy_amount, kt_cy_amount, parent) VALUES
					(iif(arg_reserve,3,1), arg_doc_id, iif(arg_reserve,0.00,var_row.dt*(-1)),0, 0, 0, v_cp_record.confirm_date, var_row.id,iif(arg_reserve,var_row.dt*(-1),0.00),0, v_parent);
					var_left_amount:= var_left_amount -  var_row.dt;
				End IF;
			END LOOP;

		END IF;

	END IF;

	return 0;
END;
$BODY$;

ALTER FUNCTION core.generate_nb_transaction_by_appl_detail(integer, boolean)
    OWNER TO postgres;

GRANT EXECUTE ON FUNCTION core.generate_nb_transaction_by_appl_detail(integer, boolean) TO PUBLIC;

GRANT EXECUTE ON FUNCTION core.generate_nb_transaction_by_appl_detail(integer, boolean) TO kazna4owner;

GRANT EXECUTE ON FUNCTION core.generate_nb_transaction_by_appl_detail(integer, boolean) TO postgres;

