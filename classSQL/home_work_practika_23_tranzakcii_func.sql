--------------------------------------------------------------------------
--                      ������� ������ 


--------------------------------------------------------------------------
 

-- �������� ����������� � ������� ����������� ( ����� ����� ) 
delete from tb_project_employees where salary is null;


select rowid, e.* 
       from  tb_project_employees e; 


-- ������� ������� ��� �����������
select seq_my_test.nextval from dual; 


-- ����������. ������ ������ ������� ������.

declare
   i number := 21;
begin 

insert into tb_project_employees (id, name_v) values (i, 'Co������� �' || i);
    commit; 
end;


--------------------------------------------------------------------------

-- ������� ������� ��� �����������
select seq_my_test.nextval from dual; 
-- ������� ���������� 22. 

-- ����������. ���������. ������� � �������������� �������� rollback  savepoint  � �.�. 
/* � ���� ������� */
declare
   i number := seq_my_test.nextval; -- 23 
begin 
    
   insert into tb_project_employees (id, name_v) values (i, 'Co������� �' || i);
   i  := seq_my_test.nextval; -- 24 
   
   savepoint point1;
   
   insert into tb_project_employees (id, name_v) values (i, 'Co������� �' || i);
   i   := seq_my_test.nextval; -- 25 
   
   rollback to point1; -- ������� ����� �� point1 ( �� ��� �������) 
   
   insert into tb_project_employees (id, name_v) values (i, 'Co������� �' || i);
   i   := seq_my_test.nextval; -- 26 
   rollback; -- ��  � ����� ������� ����� ���, ��� �� ���� ���� 
   
   insert into tb_project_employees (id, name_v) values (i, 'Co������� �' || i); 
   -- ����������� 26
  
    commit; -- ������ ��������� insert ��������� 
end;
---------------------------------------------------------------------------------------------------------

-- ������� ������� ��� �����������
select seq_my_test.nextval from dual; 
-- ������� ���������� 37.  � � ��� ���� ��� ��������� 36

declare
   i number := seq_my_test.nextval; -- 38
begin 
    
   insert into tb_project_employees (id, name_v) values (i, 'Co������� �' || i);
   i  := seq_my_test.nextval; -- 39
   
   savepoint point1;
   
   insert into tb_project_employees (id, name_v) values (i, 'Co������� �' || i);
   i   := seq_my_test.nextval; -- 40
   
   rollback to point1; -- ������� ����� �� point1 ( �� ��� �������) 
   
   insert into tb_project_employees (id, name_v) values (i, 'Co������� �' || i);
   i   := seq_my_test.nextval; -- 41
   
   
   insert into tb_project_employees (id, name_v) values (i, 'Co������� �' || i); 
     
  
    commit;
    -- ��������� ������ insert (38), ������ ��� ��� save point ��������. ������� +1 ����������
    -- ��������� 40 ������ ��� ��� seq ������ �� �����, � ��� insert ����� ����� rollback, ��� ������� 41 ������
    -- ��������� 41 ������ ��� ��� ��������� insert, � ������ insert ����� ����� rollback
end;


 
--------------------------------------------------------------------------

-- ������� ������� ��� �����������
select seq_my_test.nextval from dual; 
-- ������� ����������  51 

declare
   i number := seq_my_test.nextval;   
begin 
    
   insert into tb_project_employees (id, name_v) values (i, 'Co������� �' || i); 
     
    commit; -- ��������� insert � commit � 51 ��������� ��������� 
    
    i := i / 0;  --- ��� ������ 
    
exception when others then rollback;  
-- ����� �����, �� commit ��� ��������, �� ��� �� �������� ������ ������� �� ����,
-- � ��� ����� �� ����� ������� 
    end;

--------------------------------------------------------------------------

-- ������� ������� ��� �����������
select seq_my_test.nextval from dual; 
-- ������� ����������    

declare
   i number := seq_my_test.nextval;   
begin 
    
   insert into tb_project_employees (id, name_v) values (i, 'Co������� �' || i); 
    i := i / 0;   
    commit;  
    
-- ���� ������, �. ����� ����������    
exception
   when NO_DATA_FOUND then
      null;
   when TOO_MANY_ROWS then
        null;
   when others then rollback;      
 
    end;

select * from tb_project_employees

--------------------------------------------------------------------------
-- ���������� ����������. �������� ��� ������ ����� ����� ������������ 


DECLARE

-- ���������, ����������� ����������� ����������

PROCEDURE TEST0 IS

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

INSERT INTO TB_LOAN_LOG (LOAN_LOG_ID, RESULT_V, ON_DT) VALUES
(sq_tb_loan.nextval, 'TEST0', SYSDATE);

COMMIT;

END;

--------------------------------------------------------------------------
--  �������. ������� ���������� �� �������� ���, ��� ��� ���-�� ����������.
-- subst, count � �.�. 

-- �������� ������� 

create or replace function getClientNameById (cl_id number)
return varchar2                                     -- ��� ������, �. ���������� ���� �������
is
       cl_name varchar2(500);                       -- ���������� � �. ����� ������ ���� ������
begin
       select cl.second_name_v || ' '
              || cl.first_name_v || ' '
              || cl.surname_v
       into cl_name                                 -- ���������� � �. ����� ������ ���� ������
       from tb_client cl
       where cl.id = cl_id;

       return cl_name;                              -- � ����� ���������� ����� ���������� � ����� � result
exception when NO_DATA_FOUND then
       return '�� ���������� ID: "' || cl_id || '" ������ �� �������!';
          when TOO_MANY_ROWS then
        return '�� ���������� ID: "' || cl_id || '" ������� ����� 1� ������';
          when others then
       return '������ ������';
end;


-- �������� ������� � ���� � �������� ������ ��� ������

select 563,
       getClientNameById('dffg')
      from dual;                    -- �����:  �� ��������� � ������ ����� ' invalid number
      
      
      
-- �������� ������� � ���� � �������� id ��������������� ������� 
select 900500,
       getClientNameById(900500)
      from dual;                      -- �����: '�� ���������� ID: "900500" ������ �� �������!'
      
      
-- �������� ������� � ���� � �������� ��� id �������� 
select cl.id,
       getClientNameById(cl.id)
      from tb_client cl;


--- ��������� 

CREATE [OR REPLACE] PROCEDURE ���_���������
[ (�������� [, ��������, �]) ] IS
[��������� ����������]
BEGIN
����������� �����������
[EXCEPTION
����������� ����������]
END [���_���������];











       












--------------------------------------------------------------------------
--                     �������� ������ 


--------------------------------------------------------------------------
 

