---------------------------------------------------------------------


-- �������� ������ 


----------------------------------------------------------------------

-- �������� ��������� ��������

create global temporary table temp_count 
(
       name_v varchar2(200),
       count_n number
);

-- ������� ��������� ������, ����� ������������� ��������� ���������� ����������� 
insert into temp_count values ('����������', (select count(*) from  TB_STUFF) );
insert into temp_count values ('�������', (select count(*) from  tb_client) );

commit; -- ����� � ��� �������� ����� � �� ���������

-- ��!!! ����� ����, ��� �� ��������� �����, ��� ������ � ��������� ������� �������� 

select * from  temp_count
-- � select ���� ��� ������ ������ ������� 
 

drop table temp_count

-------------------------------------------

-- ���� ��� (��� ������ ������ )
CREATE GLOBAL TEMPORARY TABLE temp_tab_name
(column datatype [,column datatype] )
[ON COMMIT {DELETE | PRESERVE} ROWS];

-- ������� ��� ���� ���������� � �� ����� ������� ���� ��������

CREATE GLOBAL TEMPORARY TABLE temp_count
(name_v varchar2(200), 
 count_n number)
 ON COMMIT   PRESERVE  ROWS ;

-- ������ insert, � ������� ����� COMMIT 

commit;

-- ������ SELECT � �����, ��� ������ �����������. �� � ������ ���� ������ �� ���������
-- ����� ���� ������� ��� ����  - ���� ��������� ������� �������� 
select * from  temp_count



-- ������: ������: �������� ������, ����� �������� ����� ������� �� ������ ������ (id),
-- ������������ zach@g.com. �� �������� ������ ������.

with
emails as (

select '1' as id, 'Yosemite' as subject, 'zach@g.com' as from_v, 'thomas@g.com' as
to_v, to_date('2018-01-02 12:45:03','yyyy-mm-dd hh24:mi:ss') as timestamp_d from
dual
union all

select '2' as id, 'Big Sur' as subject, 'sarah@g.com' as from_v, 'thomas@g.com' as
to_v, to_date('2018-01-02 16:30:01','yyyy-mm-dd hh24:mi:ss') as timestamp_d from
dual
union all

select '3' as id, 'Yosemite' as subject, 'thomas@g.com' as from_v, 'zach@g.com' as
to_v, to_date('2018-01-02 16:35:04','yyyy-mm-dd hh24:mi:ss') as timestamp_d from
dual
union all

select '4' as id, 'Running' as subject, 'jill@g.com' as from_v, 'zach@g.com' as
to_v, to_date('2018-01-03 08:12:45','yyyy-mm-dd hh24:mi:ss') as timestamp_d from
dual
union all

select '5' as id, 'Yosemite' as subject, 'zach@g.com' as from_v, 'thomas@g.com' as
to_v, to_date('2018-01-03 14:02:01','yyyy-mm-dd hh24:mi:ss') as timestamp_d from
dual
union all

select '6' as id, 'Yosemite' as subject, 'thomas@g.com' as from_v, 'zach@g.com' as
to_v, to_date('2018-01-03 15:01:05','yyyy-mm-dd hh24:mi:ss') as timestamp_d from
dual

)
/* 
������� �� �����  (subject ), ������������ ������ �����������, � ����� �� ������� b > a �
���������� ����� ����� (a) ��������  = 'zach@g.com'. ����� ������ ����������� �� ������, ���  
���������� zach@g.com � ����������� ���� zach@g.com. 

�� ������� a < b, ������ ��� a - ��� ����������, � ��� ����� ������, ������ ������
b - ��� �����������, � ��� ����� ����� - ������ ������. ������� a ������ b 
��� ������ ������� �� ������� ���� �������� - ��� �����. ������� ���� �� �� 24 ��������, ����� 
�������� ����:
 round (( b.timestamp_d - a.timestamp_d ) * 24, 2) as res1

*/

-- ������� ���� 
select a.*,
       round (( b.timestamp_d - a.timestamp_d ) * 24, 2) as res1
     from emails a 
     join emails b on a.subject = b.subject 
                   and a.from_v = b.to_v
                   and a.timestamp_d < b.timestamp_d
     where a.to_v = 'zach@g.com'
                   

-- ��� ������ ��� �� ������ � �������� ������� : ����������� ����, �����, �����

select e.*,
       to_char(e.timestamp_d, 'dd.mm.yyyy hh24:mi:ss') as timestamp_new
 from emails e 


select * from emails



-- ������ 
/*��������: ��������, � ���  ���� ������� table ������ ����, 
��� ������ � ���� �� ������������ user �����  ��������������� ������ �������� ������ class:

| user | class |
|------|-------|
| 1	| a 	|
| 1	| b 	|
| 1	| b 	|
| 2	| b 	|
| 3	| a 	|

*/


with
tb_table as (
select 1 as user_n, 'a' as class_v from dual
union all
select 1 as user_n, 'b' as class_v from dual
union all
select 1 as user_n, 'b' as class_v from dual
union all
select 2 as user_n, 'b' as class_v from dual
union all
select 3 as user_n, 'a' as class_v from dual
)

/*������: �����������, ��� ���������� ������ ��� ��������� �������� ��� ������. 

�������� ������ ��� �������� ���������� ������������� � ������ ������. ��� ���� ������������ �
������ ������� a � b ������ ���������� � ������ b.

|-------|-------|
| a 	| 1 	|
| b 	| 2 	|

*/

/* ������� ���� 
� ���� ����� union �� ������ ���������� ������ ������, � ���� ����� ����� B. 
� ���� ����� union �� ������ ���������� ������ ������, ������� �� ������ � ������ 
������, ���������� � ���� ����� union. 
����� ��� ��� ����� � ����� union �� �������� � select � ��������� ���������� ������� 
������ ����� ����������� ������
*/


select itog.class_v, count(itog.user_n)
  from (select distinct 'b' as class_v, t.user_n
          from tb_table t
         where t.class_v = 'b'
       
        union
       
        select distinct 'a' as class_v, t.user_n
          from tb_table t
         where t.user_n not in (select distinct t.user_n
                                  from tb_table t
                                 where t.class_v = 'b')) itog
 group by itog.class_v




-- ��������� distinct � �������� ������ ���������� ������, � ���� ������ class B

select distinct 'b' as class_v,
       t.user_n
 from tb_table t 
 where t.class_v = 'b'


-- ������ ��  �������� ���, � ���� class B, �� �������� ��� ���� ����������� � 
-- ��� ��� ������, ��� ��� ��������� ���-�� ��������� �������
-- ����� distinct � ��������� ��� 


select 'b',
       t.user_n
 from tb_table t 
 where t.class_v = 'b'
 
 

-- ����� ��� ���, �� �� �� ��� ����. ������ ��� �� �������� � ������ A ������ �����, � �������� ���� � ����� B, 
-- ������ ��� ���� ���� �� � A ��������, � � B ��� ��������

select count(t.user_n),
       t.class_v
       from tb_table t 
     group by  t.class_v
     

/*  ������ �����
��������, � ��� ���� ������� transactions � ����� ����, ��� cash_flow � ���
������� ����� ������� �� ������ ����.  

2.  | date   	   | cash_flow |
3.  |------------|-----------|
4.  | 2018-01-01 | -1000     |
5.  | 2018-01-02 | -100  	   |
6.  | 2018-01-03 | 50    	   |
7.  | ...        | ...   	   |

������: �������� ������,
����� �������� ����������� ���� ��� ��������� ������ ������ ���� ����� �������,
����� � �������� ����� ���������� ������� � ����� �����:

| date   	   | cumulative_cf |
|------------|---------------|
| 2018-01-01 | -1000     	   |
| 2018-01-02 | -1100         |
| 2018-01-03 | -1050     	   |
| ...    	| ...       	     |

*/
 
with
transactions as (
select
to_date('2018-01-01','yyyy-mm-dd') as date_d, -1000 as
cash_flow   from
dual
union all
select
to_date('2018-01-02','yyyy-mm-dd') as date_d, -100 as
cash_flow   from
dual
union all
select
to_date('2018-01-03','yyyy-mm-dd') as date_d, 50 as
cash_flow   from
dual
)

-- ������� ���� 
select t.*,
       sum(t.cash_flow) over (order by t.date_d)
       from transactions  t 
        

select * from transactions;


-- ������� �������: ������������ ��� �� ������� � �����, ������� ��� ������� 

select *
       
       from transactions t
       left join transactions t1 on t.date_d < t1.date_d  

select * from transactions;











--------------------------------------------------------------------------
-- � � � � � � � �       � � � � � � 

--------------------------------------------------------------------------

/* 
1. ������� ��������� ������� �������� (cl_temp) 
2. �������� ��� ������ �� ������� ��������
3. ��������� commit (!)
4. �������� ��� ������ �������� �� ��������� ������� (select * from cl_temp)
*/


-- ������� ��������� ������� �������� (cl_temp) 
CREATE GLOBAL TEMPORARY TABLE cl_temp
( 
    id                              number
  , first_name_v                    varchar2(500)
  , birth_date_d                    DATE
  , gender_id                       number(4)
  , nationality_id                  number
  , birth_place_id                  number
  , second_name_v                   varchar2(500)
  , surname_v                       varchar2(500)
  , stuff_id                        number
 )
 ON COMMIT   PRESERVE  ROWS ;

-- �������� ��� ������ �� ������� ��������
insert into cl_temp  
       select * 
              from tb_client

-- ��������� commit (!)
commit;

-- �������� ��� ������ �������� �� ��������� ������� (select * from cl_temp)
select * from cl_temp


drop table cl_temp;

----------------------------------------------------------------------------------
-- ������� ��������� ���  ���� projects

select  *  from t_currency
----------------------------------------------------------------------------------

select * from  tb_project_employees;

select * from  tb_projects_for_empl;

select * from  tb_department_empl;

select * from  tb_empl_project_associate;

----------------------------------------------------------------------------------

select rowid, ass.* from tb_empl_project_associate ass

select rowid, e.* from tb_project_employees e

 
  
-- ������� ��������� ��� �������� ���� 
select * from  tb_client;

select * from  TB_PRODUCT;

select * from tb_order_type;

select * from  TB_ORDER;

select * from  TB_STUFF;
-- id ���� 4 � ������� position_id

select * from  TB_POSITION;

select * from  tb_country;

select * from  tb_gender;

select * from  tb_nationality;

select * from TB_ADDRESS;

select * from  tb_address_type;
