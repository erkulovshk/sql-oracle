create table tb_test_tables (
id number,
name varchar2(50),
position varchar2(100)
);


create sequence tb_test_tables_seq -- �������� �������� 
minvalue 1 -- � ������ ����� ������ (���� �� max �����, �� ������� ������ � 1
maxvalue 200  -- ������������ ��������
start with 1 -- � ������ ����� ������ ����� ������ 
increment by 1 -- ������������� ����� � ������ �����  
cache 10; -- ���-�� ����� ���������� ������ 

select tb_test_tables_seq.nextval from dual; -- ��� ������ ������ �������� ���� �������� 
select tb_test_tables_seq.currval from dual; -- ��� ������ ������ �������� ������� �������� 

insert into tb_test_tables 
values (tb_test_tables_seq.nextval, -- ��� ��� � ���������� ��� 
--��������� id �������� ��� �������. ����� ��������� �� ��������
'��������� ' || tb_test_tables_seq.currval  , null); -- ��� ������������ ��� �������������, 
-- ������ ����� '��������� ' � ��� ��������� ���������� ������� id







/*

select * from tb_test_tables
select * from tb_employeers;
select * from tb_orders;

update tb_client
set nationality_v = 'Kyrgyzstan'
where id = 3

ALTER TABLE tb_addres
  MODIFY house_v   varchar2 (50);

DELETE FROM tb_client
WHERE nationality_v = 'Russia'
AND id > 2;

DROP TABLE tb_client;
*/

