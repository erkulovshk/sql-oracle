---------------------------------------------------------------------


-- �������� ������ 


----------------------------------------------------------------------

--------------------------------------------------------------------------------

-- �������� �������, �. ��� ����������� 
create sequence seq_my_test 
minvalue 1
start with 1
increment by 1
cache 20; 


select seq_my_test.nextval from dual;

-- ������� ������ 

-- before insert - ������ ������ �������� ����� insert
-- ����� ��� ������  :new. ����� �������� ��� ������� ������� tb_project_employeesert
-- for each row - ������ ��� ������ ������ ����� �����������

create or replace trigger tg_tb_empl__add_dop_info 
   before insert                  
   on tb_project_employees
   for each row                  
begin
   :new.id := seq_my_test.nextval;

end;

-- ������ insert ������ ����� ����������, ����� ��������, ��������� �� ID 
insert into tb_project_employees (name_v) values ('���������111');

-- ��������� ������� ID 
select * from tb_project_employees;
-- ����� ID 2  ��������� ��� ������ ����������: ���������111


-- �����: ��������� ������� ���, ����� ������ �������� ID  � ��� ������, ���� ������� � insert ID �� �������� 
-- ������ ���  ���� ���� ������� ����� ��������� �� ID ������� ������� �������� �  insert, 
-- � ��� id ������� ���� ��������� � ����� ��������   seq_my_test.nextval

create or replace trigger tg_tb_empl__add_dop_info 
   before insert                  
   on tb_project_employees
   for each row                  
begin
  if (:new.id is null) then 
    :new.id := seq_my_test.nextval;
  end if;

end;



-- ������ insert ID �  ����� ����������, ����� ��������, ��������� �� ID ��� ��� ��������
insert into tb_project_employees ( id ,name_v) values (56, '���������222');

-- ��������� ������� ID 
select * from tb_project_employees;
-- ����� ID 56  ��������� ��� ID 




-- ������ insert ��� ID, ������ ���, ����� ��������, ��������� �� ID  ��������
insert into tb_project_employees (  name_v) values ( '���������333');

-- ��������� ������� ID 
select * from tb_project_employees;
-- ����� ID 3  ���������  ID �� �������� 


-- �����:  ��� ���� ������� ���, ����� ��� insert �� ��������� ����������, ������ � ������� 
-- ������������� � ����� ��������� ���� ������� (inter_name) �� ��������. 
-- Insert  �������  '���������555' - ���������� � ������� 'Sotrudnik555'

-- ����� ������� ������� 

alter table tb_project_employees add inter_name  varchar2(500);


create or replace trigger tg_tb_empl__add_dop_info 
   before insert                  
   on tb_project_employees
   for each row                  
begin
  if (:new.id is null) then 
    :new.id := seq_my_test.nextval;
  end if;
  
    if (:new.name_v is not null) then
     :new.inter_name
        := translate(:new.name_v, '���������', 'Sotrudnik');
   end if;
/* ���� :new.name_v  �� ������, �� � inter_name ������ ����� ������, ��� '���������' 
����� ������� �� 'Sotrudnik' */

end;

-- ������ insert ��� ID, ������ ���, ����� ��������, ��������� �� ID  ��������
insert into tb_project_employees (name_v) values ( '���������666');

-- ��������� ������� ID 
select * from tb_project_employees;
-- �����  � inter_name  ���������   Sotrudnik666  


-----------------------------------------------------------------------------------------------------------
-- �����: ����� � ����� ��������-����� ���������� ��� ���������� ������   �������� tb_address

select * from tb_address;

create table tb_address_arhiv (
       id number,
       client_id number,
       old_city_v varchar2(200),
       change_date_d date
); 


select * from tb_address_arhiv;


/*     C������ ������: 
       - ������ ����������� ����� update ������� tb_address 
       
       - for each row >>>  ������ �������� ��� ������ ������ 
       
       - ���� ������ �������� ������ � ������� tb_address �� ����� ������ �������� ������,
       �� � ������� tb_address_arhiv ���� �������� ��������
       1 - �������� ���� �������� ( tb_employeers_seq.nextval)
       2 - �������� client_id  �� ��������� ������ �������� id ������� 
       3 - ������ �������� ������ � ������� tb_address
       4 - ��������� ����  */
       
create or replace trigger tg_tb_address_arhiv
   before update
   on tb_address
   for each row
begin
   
   if (:old.city_v != :new.city_v) then
      insert into tb_address_arhiv values
      (
         seq_my_test.nextval,
         :new.client_id,
         :old.city_v,
         sysdate
      );
   end if;
end;

-- ������� ��� ��� ������, ��� ������� �������� city_v = ������  � where client_id = 1 and id = 2;

update tb_address set city_v = '������'  where client_id = 1 and id = 2;



-- ����� ������� ��� ��� ������ (������� � ����� � ID �������), ��� ������� �������� city_v = ������  � where client_id = 1 and id = 1;

update tb_address set city_v = '����', client_id = 8  where client_id = 1 and id = 1;


-- � ��� tg_tb_address_arhiv ������ ���������� ����� ������ (2 ��)
-- �����: �� ������ ������ � ��� ������ ���������� ����� ID �������, ������� �� ������� ���������,
-- � � ���� ������ ������ ���� ������ �������� ������, �.�. ������ 


select * from tb_address_arhiv;
-- �����: ��� ������, � ����� ������, � ����� id ������� = 1, � �� ID ������� = 8 
-- �����! 


select * from tb_address;
select rowid,  a.* from tb_address a



-----------------------------------------------------------------------------------------
--             ������ ��������� ���������� ���� ��������� ����� �� ����� 
 

with tb_tarif as (

select 1 as id_client, '�����1' as tarif , sysdate - 1 as date_d, 'work' as status from dual
union all
select 1 as id_client, '�����2' as tarif , sysdate - 4 as date_d, 'not work' as status from dual
union all
select 1 as id_client, '�����3' as tarif , sysdate - 6 as date_d, 'not work' as status from dual
union all
select 2 as id_client, '�����4' as tarif , sysdate - 8 as date_d, 'not work' as status from dual
union all
select 2 as id_client, '�����5' as tarif , sysdate - 1 as date_d, 'work' as status from dual
union all
select 2 as id_client, '�����6' as tarif , sysdate - 9 as date_d, 'not work' as status from dual
)

select count(*) 
       from tb_tarif
       where status = 'not work';
-- ����� max (date) ��� ������� �������, - ��� ������ � ��������� 
-- ����� ������ �������� ���, ��� ������ ������ ���������� ���� 
-- � �.�. 





--------------------------------------------------------------------------
-- � � � � � � � �       � � � � � � 

--------------------------------------------------------------------------


������� ������� ��� ������� ��������, ����� ����������� ���������� ��� ������� �������������.

�������� ��������: 
1. ��� ���������� ������ ������� stuff_id �� ���������
2. � �������� ����� �������� ������ � ������� �������� - ��������� ��������� �� stuff id,
 ���� �� ��������� - ��������� ��������������
3. ���������� ���������� ����������� ��������� ���������� ����� (����������� ���������, �������� ���� ������� � ������)
4. ����������� ��������� ����� � stuff id, ��������� �������.

PS �������� ����� ����������� � ���, ��� stuff id ��� ��������� ���� � ��� ����� ����� ��� �� 
��������, ����������� ������������ � ����� �������� �� 1 �� ������������� id ����������

-- ������� 1 c mod, �� ���� � RANDOM, ����� ���� ���������  
create or replace trigger tg_client_add_stuff
       before insert
       on tb_client
       for each row
begin 
  if (:new.stuff_id is null) then
    :new.stuff_id := mod( :new.id, 20); 
     
    end if;
end;

insert into tb_client (id, first_name_v) values (101, 'Aaaaa');

insert into tb_client (id, first_name_v) values (102, 'Bbbbb');

select * from tb_client; 

-- ������� 2   c mod, �� ���� � RANDOM, ����� ���� ���������

create trigger tg_tb_client
  before insert on tb_client
  for each row
 declare
    max_n number;
  begin
 select max(id) 
  into max_n
  from tb_stuff;

  if(:new.stuff_id is null) then
     :new.stuff_id:=mod(:new.id,max_n);
     end if;
end;


-- ������� 3 � RANDOM, �� ���� ��� ������ ����� �� ������ ������ ������������ ����� ��� RANDOM)
-- � ��� ���� �������� ����� �� max(stuff.id) 

create or replace trigger tg_client_add_stuff
  before insert on tb_client
  for each row
declare
    res_random number; 
begin
  select trunc(dbms_random.value(1, 20)) into res_random from dual;
 
       
  if(:new.stuff_id is null) then
     :new.stuff_id:= res_random;
     end if;
end;

insert into tb_client (id, first_name_v) values (101, 'Aaaaa');

-- ������� 4 - ���� ����� for me :) 
-- ��� ����� ��   ������������ ����� ��� RANDOM ����� �� max(stuff.id) 
create or replace trigger tg_client_add_stuff
  before insert on tb_client
  for each row
declare
    max_st_id number;
    res_random number; 
begin
  select max(id) into max_st_id from tb_stuff;
  select trunc(dbms_random.value(1, max_st_id)) into res_random from dual;
        
  if(:new.stuff_id is null) then
     :new.stuff_id:= res_random;
     end if;
end;

insert into tb_client (id, first_name_v) values (101, 'Aaaaa');



--------------------------------------------------------------



insert into tb_client (id, first_name_v) values (101, 'Aaaaa');

insert into tb_client (id, first_name_v) values (102, 'Bbbbb');

select * from tb_client 


-- ��������� ��������� ����� 

SELECT TRUNC(DBMS_RANDOM.VALUE(0, 100)) FROM DUAL; 

select * from ( SELECT TRUNC(DBMS_RANDOM.VALUE(1, 20)) FROM DUAL)
 
select rowid, cl.* from tb_client cl

 


-----------------------------------------------------------------------------------

create or replace trigger tg_tb_empl__add_dop_info 
   before insert                  
   on tb_project_employees
   for each row                  
begin
  if (:new.id is null) then 
    :new.id := seq_my_test.nextval;
  end if;

end;























----------------------------------------------------------------------------------
-- ������� ��������� ���  ���� projects

select  *  from t_currency
----------------------------------------------------------------------------------

select * from  tb_project_employees;

select * from  tb_projects_for_empl;

select * from  tb_department_empl;

select * from  tb_empl_project_associate;

----------------------------------------------------------------------------------

select rowid, ass.* from tb_empl_project_associate ass

select rowid, e.* from tb_project_employees e

 
  
-- ������� ��������� ��� �������� ���� 
select * from  tb_client;

select * from  TB_PRODUCT;

select * from tb_order_type;

select * from  TB_ORDER;

select * from  TB_STUFF;
-- id ���� 4 � ������� position_id

select * from  TB_POSITION;

select * from  tb_country;

select * from  tb_gender;

select * from  tb_nationality;

select * from TB_ADDRESS;

select * from  tb_address_type;
