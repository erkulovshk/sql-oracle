
--------------------------------------------------------------------------
--������� ������  

-- ������������� ������� ����� 1 


--------------------------------------------------------------------------


-- ��������� ������������� ������� 
 
SELECT ������������� ������� 
OVER([PARTITION ��������������] 
ORDER BY (�������������� ���������))
 

-- ���������� � ������������� ��������, ���� �� ������ ����� �� ������ ������,
-- � ������ ��������� �� ��� ������ ������ �������, � �. ��� ���������
select o.*,
       sum (o.quantity_n) over (order by o.date_d) as sum_a
      from tb_order o 
      join tb_client cl on cl.id = o.client_id
      

-- ���� �������� ���������� � �������� ����, ��� ���� �������� �������������, 
-- �� ����������� ����� �������� �������� ����������  
select o.*,
       sum (o.quantity_n) over (order by o.date_d) as sum_a
      from tb_order o 
      join tb_client cl on cl.id = o.client_id
     order by o.date_d desc  
     -- ���������� ��������� �������. ��� ��������� � ����� ��������� � ������ �� ���������� �� ������
     -- ���� � ��� � 24 ������� ��������, � ��  � ������ ����� � 28 ������� 
     

/* ����� lag � lead  � ������������� ��������. ����� ����� ��������� 
��� ������� ������ �� �������, ����� ������ ��������� ����������� ������� �� ��������� �����.
����� ����� ������������ � �������, ����� ���� � ����� ������� �������� '�����'. ����� ����� ������� lead,
� ���� �� NULL, �� �������� �����. ��� �������. 


 � ����� �� ������:  
   - � lag �� �������� ���. � ��� �� ����� ���������� � ����������� ���. 
   ������� � ������ ����� NULL � lag
   - � lead �� �������� ���. � ��� �� ����� ��������� � ����������� ���. 
   ������� � ������ � lead ����� Ivan. 

*/
select e.*,
       lag(e.name_v) over (order by e.name_v) as prev_name,
       lead(e.name_v) over (order by e.name_v) as next_name
    from tb_project_employees e


/*  ������� LISTAGG ���������� �������� measure_column ��� ������ ������ �� ������ order_by_clause.

                  LISTAGG (measure_column [, 'delimiter'])
                  WITHIN GROUP (order_by_clause)
    
   
    LISTAGG - �� ��������� ���������� ������ 
    measure_column - ����� ������ �� ����� ����������� � LISTAGG 
    WITHIN GROUP - �������� �� ������� ������������ �������� 
    delimiter - ������������ ������ ������������ � listagg, ����� �������, ����� � ������� � �.�. 
    
    ��������, ����� ������� ��� ������� � �������� � ���� ��������� ������ ������ ���� ��� ������, 
    ��� ������ ���� ��� ������� ���������. 
    
    ������ ��� �� ��������� �������. 
    ������� � ������� � 2 ������ ���� ���������, ������� ������� ������. 
*/

select cl.first_name_v,
       listagg (pr.name_v, '; ') within group (order by cl.id)
      from tb_order o
      join tb_product pr on pr.id = o.product_id
      join tb_client cl on cl.id = o.client_id
     where pr.id in (1, 3)
     group by cl.first_name_v


/*  
������: ���� ������ �100011110110011101� ����������,
������� � ��� ������
*/

-- ���������� � ��� ����: 18 ��������
select length ('100011110110011101') as str
       from dual; 

-- ������ ��� ���� ������� �� �������, � ��� ��������� ������ ��������, � ��������� ����� ������
-- ����:  11 ��������, �.�. 11 �������� 

select length (replace('100011110110011101', '0', '')) as str
       from dual;      
    


   
/*  
������: ������� �� ����� ������, ������ ���� � ������� ��� ������,
������� ���������� �� ����� �� ����� (tb_country)
*/
 
-- ������� 1
-- ��� ��� ����� �������� ��� ������, ����� ������� �� �������� ����� �� ������� ������� ������� ������� ���� (0) 
select  c.id,
        c.name_v
   from tb_country c
   where  ( 
            select count(*)
                   from tb_country c1
                   where c.id !=c1.id 
                         and substr (c.name_v, 1, 1) = substr(c1.name_v, 1, 1)       
                         ) = 0
 -- ���� ��� ��� ��������� 1, �� � ������ �� ��� ������ ��� ������� ������, ����� ������ ����� �� ������
                         ;
                         
-- ������� 2 � ������� exists
-- �� ���������� ����� �������� ������, � �. ������ ����� ������
-- � � ������� not exists ����� �������� ������, �. �� ������ �� ���������� select  
select  c.id,
        c.name_v
   from tb_country c
   where not exists ( 
                  select *
                         from tb_country c1
                         where c.id !=c1.id 
                               and substr (c.name_v, 1, 1) = substr(c1.name_v, 1, 1)       
                         )
                         ;
                       
     
   
/* ������ ����� 
������� �� ����� ������, ������ ���� � ������� ��� ������, �������  ������������� �� ����� �� 3 
��������� ����� 
 */   
 
 -- ������� 
 -- �� ���������� select �� ������� ������, � ������� ��������� ��� ������� ������
 -- � ������� select ��������� ���, ��� �� ����� �� ���������� select, �� ���� ���������� ��� ����� ���������� ������

 select c.id,
       c.name_v
      -- ,substr(c.name_v, -3)  -- ���� �������������, �� ����� ������� ��������� ��� ������� ���� �����  
  from tb_country c
       where not exists (
          select *
            from tb_country c1
           where c.id != c1.id
                 and substr(c.name_v, -3) = substr(c1.name_v, -3)
               )
   -- ��� ������ � ����� ����������� ��� ������� � �� �������� �� ����� �����, �� ���� ��� ������� �����
 
 
 
 
 
 /* 
������ �����:
������� �� ����� ������������ ��������, ���������� ��� ������� � ���������� ������� �� 
����� �������� (���������� ���� ��������� ������� ��� ����� ��������)
*/

-- ��������� �������� ����� � ���������� ��� �������. 
select pr.name_v,
       count (o.id)
      from tb_product pr 
      join tb_order o on pr.id = o.product_id 
      group by pr.name_v

-- �����: ��������� ���� ������, �� ������ ���-�� ���� ������� 
 
select count(*)
                    from tb_product p
                    join tb_order o on p.id = o.product_id 
       )
       
-- �����: ������ ������ 

select pr.name_v,        -- ��� ������� 
       count (o.id),     -- ���������� ������� ����� ��������
       (
             select count(*)
                    from tb_product p
                    join tb_order o on p.id = o.product_id 
       ) - count (o.id)  other_order -- ����� ���-�� MINUS ���-�� ������� ����� ��������
      from tb_product pr 
      join tb_order o on pr.id = o.product_id 
      group by pr.name_v




--------------------------------------------------------------------------

--������� ������  



--------------------------------------------------------------------------
-- ������������� ������� ����� 2




/*  
������ -- ������� ����� �����

1. ����� ��������� ������ ������ ����� ������ "���������" � ������� 2018 ����?

2. �������� ��� 3 ����� ������� ������ ����� ��������

3. �������� ��� 3 ������ ���������� (������� ���������� ���������� �������)
*/

--------------------------------------------------------------------------

-- RANK � ���������� ���� (= �������) �������� � ������ ��������

/* � order by � ��� ���������� �� ��������, ���������� �� ���� �������
 �� ��������. �� rank ��� ������� � ��� ������ ���� ������ ������������� �������,
������� ��� ���������� �� �������� ������� 9 

�����: ����� ������� ����������� ������ �� �����������, ������ �� �������� ��� �������,
��� boss �������� ����. ������, �� ���� �� ������� manager ���� 3-�, � �� 2-� � ������ */ 

select e.name_v,
       e.position_v,
       rank() over (order by e.position_v)
  from tb_project_employees e
 order by e.position_v
 
 
 
/* dense_rank  ������������ ���� ������ �������� +1 � ����������� ������, 
� �� ������������� �� ���-�� ������� ��� ���� �� ������ rank  */ 
select e.name_v,
       e.position_v,
       rank() over (order by e.position_v),
       dense_rank () over (order by e.position_v) as dens_rank_res
-- ���� �������� desc   (order by e.position_v DESC) �� �������� ���������� �������� ���������
        from tb_project_employees e
        order by e.position_v



/*  ������: 
����� ������ (������, ���������, ������� � n-e) �� �������� �������� ��������� � ������� ���������.
�.�. �������� ������ ������������ ��������, ����� ������ � �.�. 

����� ������ � ������� ����������� � ���������� ������� max().
�� ��� ����� ������������� ������� (������ ��������������), ������� ����� ������������ DENSE_RANK  */ 

-- ������� ����������� ��� ������ ������ �������� 
-- � ���������� select ������ ������������ price
-- � �.�. ������ � �.�. �� ��������  

select max(p.price_n)
   from tb_product p
   where p.price_n < 
 (
 select max(pr.price_n)
   from tb_product pr
 )


--  ��� ��������, 10, 20 � ������� �� �������� ��������?
-- ������� ������� ��������, ��� ����������� ������� ��� �������
-- � ������ ����������� � �������� ������� ( �� �������� � ��������)

select dense_rank() over (order by p.price_n desc) as rnk,
               p.price_n as pr
               from tb_product p 




               
-- ������ ����� �������� ���������� (distinct) ������ ��� rnk (������� ����� ��������)
-- � pr ( �����), ��� �� ������� (rnk) ����� � ������� 5. �.�. �������� 5 ����� � ��������.

select distinct x.rnk, x.pr
   from (
        select dense_rank() over (order by p.price_n desc) as rnk,
               p.price_n as pr
               from tb_product p 
        ) x 
    where x.rnk = 5; 


-- ������ ����� �������� ��������� ����, ������� ������� ���  x.rnk (��������) � where in 
select distinct x.rnk, x.pr
   from (
        select dense_rank() over (order by p.price_n desc) as rnk,
               p.price_n as pr
               from tb_product p 
        ) x 
    where x.rnk in (5, 9, 30); 


/*       
          ������: �� ������� ������� ������� ���� ����������� ������. 
*/

-- LAG � ���������� �������� �� ���������� ������ � �������.
-- ��������� ������� ���������� ��������
select p.*,
       lag(p.price_n) over (order by p.price_n) as prev_price 
         from tb_product p





-- LEAD � ���������� �������� �� ��������� ������ � �������.
-- ���������� ������� lag � ��������� �� ��������: ��������� ������� ���� �������� ��� �����


select p.*,
       lead(p.price_n) over (order by p.price_n) as next_price 
       from tb_product p






-- ������ ���������
select p.*,
       lag(p.price_n) over (order by p.price_n) as prev_price,
       lead(p.price_n) over (order by p.price_n) as next_price 
       from tb_product p



-- �������

select o.product_id,
       o.date_d,
       lag(o.date_d, 1) over (order by o.date_d) as prev_date 
       --����� �������� ������ ���������� ���������� �������
  from tb_order o;


-- LAST_VALUE � ���������� ��������� �������� � ������������� ������ �� �������������� ����. 
--(first_value � ��������, ���������� ������ ��������)

/* 
����� � first_v �� ������� �� �������� �� ��������, ��� ������ ��������,
�.�. ��� ������ ��������� ������� ����� ������/��������� ������ 
� ����������� �� �������� ���� ������� ��������������� ��������, �.�. �������.
� ����� ����� ������� ������ �������� �� = 5213, ������� ��� ��������� ���� �������
� ������� first_v.
����� ���������� ��������� �������� � ������� 12500, ������� ��� ��������� ���� ����������
� ������� last_v. 
*/
-- ����� �������� ����� ���������� ��� ����������� ����� �� ��.
-- ����� ��� ������� ������ ����/��� ������ ��� ������ � ����������� ������������


select e.*,
       first_value(e.salary) over 
                    (partition by e.position_v order by e.position_v) as first_v,
       last_value(e.salary) over 
                     (partition by e.position_v order by e.position_v) as last_v
      from tb_project_employees e;
-- �������� ���������� �� ��. � ������ �������� �� ������� �������� ����������� �� �������� � ��������
select e.*,
       first_value(e.salary) over 
                    (partition by e.position_v order by e.salary) as first_v,
       last_value(e.salary) over 
                     (partition by e.position_v order by e.salary) as last_v
      from tb_project_employees e;



-- row_number  ������ ����������� ���������� ����� ������ ������.

--������: ������� ����� ������ ��� ������� ������ � ������� �� ��������.

--�������: ����� ��������. �. �� �������� ���������� ������� ����������� ������� PRICE �� �������� � ��������
-- � ����� row_number ��������� ���������� ����� ������ ������

select *
   from (
        select row_number() over (order by p.price_n desc ) as r_num,
               p.price_n as pr 
               from tb_product p
        ) x ;
     
      
-- ���� ������ �������  PRICE, �� ������� ������ ����� ��, ��� �� ����� � �������       

select *
   from (
        select 
               --row_number() over (order by p.price_n desc ) as r_num,
               p.price_n as pr 
               from tb_product p
        ) x ;


-- ������� ������� ������ �� �������� ��������� PRICE ������  � �������� 
select *
   from (
        select row_number() over (order by p.price_n desc ) as r_num,
               p.price_n as pr 
               from tb_product p
        ) x
   where x.r_num = 3;



-- ������� �������� ���������� ���� �� ������ �������� 


select o.product_id,
       o.date_d,
       lag(o.date_d, 1) over (order by o.date_d) as prev_date   
  from tb_order o
  where o.product_id = 22;



--      NTH_VALUE � ���������� n-�� �������� � ������������� ������ �������� �� �������������� ����. 

-- ������: ������� ������ ������������ ����� ��� ������� �������

select cl.id,
       cl.second_name_v,
       o.quantity_n * p.price_n,
       nth_value(o.quantity_n * p.price_n, 3) 
            over (partition by cl.id order by o.quantity_n * p.price_n desc
          )as nth_v --��������� ��� ���� ����� �������� ������� ������������ ����� ������ ������� � ���� �����
  from tb_client cl 
  join tb_order o on o.client_id = cl.id
  join tb_product p on p.id = o.product_id
  

  
-- 
/* 
PERSENT_RANK � ���������� ���� �������� � ������ ��������. 
������� ������� �� ���������� ����� � ������, ������� ����� ������� ��������, ��� ������� ������.

CUME_DIST � ��� �� ���������� ���� �������� � ������ ��������, �� ��� 
������� ������� �� ���������� ����� � ������, ������� ����� ������� ���� ������ ��������, ��� ������� ������.

��������� �� �� � ������ �������� ����������� �� �������.
- ��� ���������� PERSENT_RANK � ������� ������� ����, ������ ��� ��������� ����� ������� 0, ������ ��� ��� ������ 
� ����� ��������. ����� � ������� � ������� ����� 1, ������ ��� 100 % ������� ������ ��� ������� ������.
- ��� ���������� CUME_DIST ����� ������� ������� ������ ��� ����� �������� ��������.
�����: ��� ��� ������� ������������� �� ����� ������ � �������� ������ �� ��������, 
�� ��� ������� ��������� �� ������� ������ �� ������ �������� ����� ������
*/

select e.name_v,
       e.salary,
       e.position_v,
       percent_rank() over (partition by e.position_v order by e.salary) as percent_runk, 
       cume_dist() over (partition by e.position_v order by e.salary) as cume_dist_runk
  from tb_project_employees e


/*  
������  

1. ����� ��������� ������ ������ ����� ������ "���������" � ������� 2018 ����?

2. �������� ��� 3 ����� ������� ������ ����� ��������

3. �������� ��� 3 ������ ���������� (������� ���������� ���������� �������)
*/


-- ������: 1. ����� ��������� ������ ������ ����� ������ "���������" � ������� 2018 ����?
-- ������� 

-- ��� ������ ������� �������, ������ ���� �� ��� ���� 
select p.name_v
   from tb_product p
   where lower(trim(p.name_v)) = '���������'

/* ����� �� ����� ��������, ����� ������ � ������� ��� ��������� ���������.
���������� ������� �������� � ������� ������.
*��� ��� �����, ����� ����� ������� ����� �� ����������  */
select p.name_v,
       count(o.id),
       o.client_id
   from tb_product p
   join tb_order o on o.product_id = p.id
    where lower(trim(p.name_v)) = '���������'
    group by o.client_id, p.name_v


/* ����� � ������� ����, ����� �� �������� ������ 2018 �.
��� ����� ��������� ��������� ����� � ���, ��� ����� 
� ����������� ���� ������� �������� to_char ��� ����.
����� � where ������� �������� ��������� � '08-2018'  */

select p.name_v,
       count(o.id),
       o.client_id,
       to_char(o.date_d, 'mm-yyyy')
   from tb_product p
   join tb_order o on o.product_id = p.id
    where lower(trim(p.name_v)) = '���������'
          and to_char(o.date_d, 'mm-yyyy') = '08-2018'
    group by o.client_id, p.name_v, to_char(o.date_d, 'mm-yyyy')

/* ����� ��� ���� ����� �� ����������, �. ������� ��������� :)
��� ����� ��������� ��������, ����������� � ������ �� ����� ����� ����� id ����������.
� ������� ������ � �������� ����� id �������.
������� � select ��� Stuff 

� ����� �� �������� ������ ������, ����� ��������� �����-� �������� 
��������� ������-������ ������� � ������� 2018.

 */

select p.name_v,
       count(o.id),
       o.client_id,
       to_char(o.date_d, 'mm-yyyy'),
       st.last_name_v
   from tb_product p
   join tb_order o on o.product_id = p.id
   join tb_client cl on cl.id = o.client_id
   join tb_stuff st on st.id = cl.stuff_id
    where lower(trim(p.name_v)) = '���������'
          and to_char(o.date_d, 'mm-yyyy') = '08-2018'
    group by o.client_id, p.name_v, to_char(o.date_d, 'mm-yyyy'), st.last_name_v
 


/* ����� ��� ����  ������� count ������� ������ ��� ������� ����������
��� ����� ������ ���� �������� � select. � ������� �������� result.
� ����������� select ������� ����� id ����������.

������ �� �������� result ������� id ����������, ��� ���������� � ����� �� ������� ���������� ������ 
����������.

� ����� �� �������� �������� ��� � ������� ����������� �������, ������ ��� ����� ������ �� ������ ������, 
�  ������ � ������� ������ (pr_count ) ���������� ����������� ��� ������� ����������. 
 */

select result.st_id,
       result.st_name,
       sum(result.pr_count) as sr
    from
    (
      select p.name_v                     as pr_name,
             count(o.id)                  as pr_count, -- ��� ���������� ������ ����������
             o.client_id                  as cl_id,
             to_char(o.date_d, 'mm-yyyy') as o_date,
             st.last_name_v               as st_name,
             st.id                        as st_id
        from tb_product p
        join tb_order o on o.product_id = p.id
        join tb_client cl on cl.id = o.client_id
        join tb_stuff st on st.id = cl.stuff_id
       where lower(trim(p.name_v)) = '���������'
             and to_char(o.date_d, 'mm-yyyy') = '08-2018'
       group by o.client_id, p.name_v, to_char(o.date_d, 'mm-yyyy'), st.last_name_v, st.id
    ) result  
    group by result.st_id, result.st_name


/* ����� ��� ���� ������� ������ ��� �����������, � ���� ������������ �������. 
� ����� ������ 4 ������� ������������.
��� �����: 
- ������� � ��������� �������� ����
- �� ���������� select ������� ������� ��� ����� ������ sum(result.pr_count) 
- �� ������� select � ������� x ����� �������� ��� ������, ��� dens_rank_res = 1 */

select *
        from (
   select result.st_id,
          result.st_name,
          sum(result.pr_count) as sr
          ,dense_rank () over (order by sum(result.pr_count) desc) as dens_rank_res
          -- �������� �������, � �. ����� ��� sum(result.pr_count) ����������� �� desc  
        from
    (
      select p.name_v                     as pr_name,
             count(o.id)                  as pr_count, -- ��� ���������� ������ ����������
             o.client_id                  as cl_id,
             to_char(o.date_d, 'mm-yyyy') as o_date,
             st.last_name_v               as st_name,
             st.id                        as st_id
        from tb_product p
        join tb_order o on o.product_id = p.id
        join tb_client cl on cl.id = o.client_id
        join tb_stuff st on st.id = cl.stuff_id
       where lower(trim(p.name_v)) = '���������'
             and to_char(o.date_d, 'mm-yyyy') = '08-2018'
       group by o.client_id, p.name_v, to_char(o.date_d, 'mm-yyyy'), st.last_name_v, st.id
    ) result  
    group by result.st_id, result.st_name
) x
where x.dens_rank_res = 1; 

-- �����!



     





--------------------------------------------------------------------------
-- � � � � � � � �       � � � � � � 



--------------------------------------------------------------------------

/*��������� ������� �������, ��������� ���� ���� ������ (date_d ����
����). ��� ������ ������ ������ ���������� ���� ������ (��� ������ ������������
�� ���) */


-- ���������� ���� date_d
alter table tb_order 
      add date_d date; 

-- ������������ ���� ( �� �������� � Excel, ������� �� ���� ����������� �������. ��� ������� ���� ������) 
update tb_order
       set date_d = to_date('08/01/2018', 'DD/MM/YYYY')
       where id = 1600;

/* ������� (��������� ������������� �������):
������� ����� ������� ����� ������ � ����� ���������
����� ������ �� ������ �����. */

select distinct 
       min(pr.price_n*o.quantity_n) over (partition by to_char(o.date_d, 'mm.yy')) min_sum_order,
       max(pr.price_n*o.quantity_n) over (partition by to_char(o.date_d, 'mm.yy')) max_sum_order
       from tb_order o 
       join tb_product pr on o.product_id = pr.id;


-- ������, ������� �� �������� �� �����:
/*
������� �� ����� ������������ ��������,
���������� ��� ������� �
���������� ������� �� ����� �������� (���-�� ���� ��������� ������� ��� ����� ��������)
*/

-- ���� 
select p.name_v,
       count(o.id),
       (
         select count(p1.id)
             from tb_product p1
             join tb_order o1 on o1.product_id = p1.id
            where p.id != p1.id
            group by p1.id, p.id
       )
  from tb_product p
  join tb_order o on o.product_id = p.id
 group by p.name_v;

-- ����� 
 

select  p.name_v,
        (select count(*)
                from tb_product pr
                join tb_order o on pr.id=o.product_id
                where pr.id=p.id
                      ) as orders_this_product,
         (select (count(*))
                 from  tb_product pr
                 join  tb_order o on pr.id=o.product_id
                 where pr.id!=p.id
                       ) as orders_other_product
         from tb_product p;




--------------------------------------------------------------------------
-- � � � � � � � �       � � � � � � 

-- ������������� ������� ����� 2

--------------------------------------------------------------------------


-- 2. �������� ��� 3 ����� ������� ������ ����� ��������

select *
       from (
       select dense_rank() over(order by pr.price_n desc) as pos,
              pr.name_v,
              price_n
            from tb_product pr) sort_pr
            where sort_pr.pos in(1,2,3);

-- 3. �������� ��� 3 ������ ���������� (������� ���������� ���������� �������)

select best_st.surname,
       best_st.sum_ord
     from (
       select
            dense_rank() over (order by sum(o.quantity_n) desc) pos,
            sum(o.quantity_n) sum_ord,
            st.surname_v surname
         from tb_order o
          join tb_client cl on cl.id=o.client_id
          join tb_stuff st on st.id=cl.stuff_id
          group by st.id,st.surname_v
                 ) best_st 
          where pos in(1,2,3)




--------------------------------------------------------------------------
--     � � � � � � � � 

-- ������������� �������  


--------------------------------------------------------------------------


create table channel (
  id_channel     number        primary key,
  name_channel   varchar(100)  not null);
  
insert into channel (id_channel, name_channel) values (1,'������ �����');
insert into channel (id_channel, name_channel) values (2,'������ 1');
insert into channel (id_channel, name_channel) values (3,'���');
insert into channel (id_channel, name_channel) values (4,'�����');
insert into channel (id_channel, name_channel) values (5,'��������');
insert into channel (id_channel, name_channel) values (6,'4 �����');
insert into channel (id_channel, name_channel) values (7,'���-��');

create table opros (
  date_opros    date,
  nom_operator  number,
  channels      varchar2(20),
  constraint  pk_opros primary key(date_opros, nom_operator)
);

insert into opros (date_opros, nom_operator, channels) values (to_date('11.01.2015 19:35','dd.mm.yyyy hh24:mi'),2, '1,4,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('11.01.2015 19:41','dd.mm.yyyy hh24:mi'),4, '2,3,6');
insert into opros (date_opros, nom_operator, channels) values (to_date('12.01.2015 20.00','dd.mm.yyyy hh24:mi'),1, '1,2,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('13.01.2015 17.10','dd.mm.yyyy hh24:mi'),3, '6,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.01.2015 15.33','dd.mm.yyyy hh24:mi'),2, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.01.2015 19.33','dd.mm.yyyy hh24:mi'),1, '1,2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('15.01.2015 18.44','dd.mm.yyyy hh24:mi'),1, '1,4,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('16.01.2015 20.30','dd.mm.yyyy hh24:mi'),3, '6,5,2');
insert into opros (date_opros, nom_operator, channels) values (to_date('16.01.2015 18.10','dd.mm.yyyy hh24:mi'),2, '1,6,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('17.01.2015 15.33','dd.mm.yyyy hh24:mi'),4, '1,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('17.01.2015 19.33','dd.mm.yyyy hh24:mi'),5, '2');
insert into opros (date_opros, nom_operator, channels) values (to_date('17.01.2015 18.44','dd.mm.yyyy hh24:mi'),4, '1');
insert into opros (date_opros, nom_operator, channels) values (to_date('18.01.2015 16.42','dd.mm.yyyy hh24:mi'),3, '1,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('19.01.2015 15.44','dd.mm.yyyy hh24:mi'),2, '1,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('20.01.2015 17.59','dd.mm.yyyy hh24:mi'),1, '7,3,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('21.01.2015 19.00','dd.mm.yyyy hh24:mi'),2, '1,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('22.01.2015 21.18','dd.mm.yyyy hh24:mi'),3, '5,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('23.01.2015 20.01','dd.mm.yyyy hh24:mi'),4, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('24.01.2015 20.02','dd.mm.yyyy hh24:mi'),4, '2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('25.01.2015 20.55','dd.mm.yyyy hh24:mi'),3, '2,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('26.01.2015 16.54','dd.mm.yyyy hh24:mi'),2, '1,6');
insert into opros (date_opros, nom_operator, channels) values (to_date('27.01.2015 15.33','dd.mm.yyyy hh24:mi'),3, '1,3,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('28.01.2015 19.33','dd.mm.yyyy hh24:mi'),2, '5,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('29.01.2015 18.44','dd.mm.yyyy hh24:mi'),1, '7,3,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('30.01.2015 20.30','dd.mm.yyyy hh24:mi'),2, '3,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('31.01.2015 18.10','dd.mm.yyyy hh24:mi'),3, '3,2');
insert into opros (date_opros, nom_operator, channels) values (to_date('01.02.2015 15.33','dd.mm.yyyy hh24:mi'),4, '2,5,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('02.02.2015 19.33','dd.mm.yyyy hh24:mi'),3, '1,4,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('02.02.2015 18.44','dd.mm.yyyy hh24:mi'),2, '2,3,6');
insert into opros (date_opros, nom_operator, channels) values (to_date('04.03.2015 16.42','dd.mm.yyyy hh24:mi'),1, '1,2,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('04.03.2015 15.44','dd.mm.yyyy hh24:mi'),1, '6,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('04.03.2015 17.59','dd.mm.yyyy hh24:mi'),1, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('04.03.2015 20.00','dd.mm.yyyy hh24:mi'),2, '1,2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('05.03.2015 17.10','dd.mm.yyyy hh24:mi'),3, '1,4,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('05.03.2015 15.33','dd.mm.yyyy hh24:mi'),5, '6,5,2');
insert into opros (date_opros, nom_operator, channels) values (to_date('05.03.2015 19.33','dd.mm.yyyy hh24:mi'),3, '1,6,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('05.03.2015 18.44','dd.mm.yyyy hh24:mi'),4, '1,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('06.03.2015 20.30','dd.mm.yyyy hh24:mi'),3, '2');
insert into opros (date_opros, nom_operator, channels) values (to_date('06.03.2015 18.10','dd.mm.yyyy hh24:mi'),2, '1');
insert into opros (date_opros, nom_operator, channels) values (to_date('06.03.2015 15.33','dd.mm.yyyy hh24:mi'),1, '1,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('06.03.2015 19.33','dd.mm.yyyy hh24:mi'),5, '1,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('07.03.2015 18.44','dd.mm.yyyy hh24:mi'),4, '7,3,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('07.03.2015 16.42','dd.mm.yyyy hh24:mi'),3, '1,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('07.03.2015 15.44','dd.mm.yyyy hh24:mi'),4, '5,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('07.03.2015 17.59','dd.mm.yyyy hh24:mi'),4, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('07.03.2015 19.00','dd.mm.yyyy hh24:mi'),3, '2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('07.03.2015 21.18','dd.mm.yyyy hh24:mi'),3, '2,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('07.03.2015 20.01','dd.mm.yyyy hh24:mi'),2, '1,6');
insert into opros (date_opros, nom_operator, channels) values (to_date('07.03.2015 20.02','dd.mm.yyyy hh24:mi'),1, '1,3,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('07.03.2015 20.55','dd.mm.yyyy hh24:mi'),1, '5,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('07.03.2015 16.54','dd.mm.yyyy hh24:mi'),2, '7,3,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('07.03.2015 15.33','dd.mm.yyyy hh24:mi'),3, '3,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('08.03.2015 19.33','dd.mm.yyyy hh24:mi'),4, '3,2');
insert into opros (date_opros, nom_operator, channels) values (to_date('09.03.2015 18.44','dd.mm.yyyy hh24:mi'),3, '2,5,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('12.03.2015 20.30','dd.mm.yyyy hh24:mi'),3, '1');
insert into opros (date_opros, nom_operator, channels) values (to_date('13.03.2015 18.10','dd.mm.yyyy hh24:mi'),4, '1,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('13.03.2015 15.33','dd.mm.yyyy hh24:mi'),5, '1,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.03.2015 19.33','dd.mm.yyyy hh24:mi'),4, '7,3,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.03.2015 18.44','dd.mm.yyyy hh24:mi'),3, '1,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.03.2015 16.42','dd.mm.yyyy hh24:mi'),5, '5,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.03.2015 15.44','dd.mm.yyyy hh24:mi'),1, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.03.2015 17.59','dd.mm.yyyy hh24:mi'),1, '2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.03.2015 16.54','dd.mm.yyyy hh24:mi'),1, '5,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.03.2015 15.33','dd.mm.yyyy hh24:mi'),2, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.03.2015 19.33','dd.mm.yyyy hh24:mi'),3, '2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.03.2015 18.44','dd.mm.yyyy hh24:mi'),4, '2,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.03.2015 20.30','dd.mm.yyyy hh24:mi'),5, '1,6');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.03.2015 18.10','dd.mm.yyyy hh24:mi'),3, '1,3,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.03.2015 15.33','dd.mm.yyyy hh24:mi'),4, '5,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.03.2015 19.33','dd.mm.yyyy hh24:mi'),1, '1,4,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('15.03.2015 18.44','dd.mm.yyyy hh24:mi'),5, '2,3,6');
insert into opros (date_opros, nom_operator, channels) values (to_date('16.03.2015 16.42','dd.mm.yyyy hh24:mi'),4, '1,2,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('17.03.2015 15.44','dd.mm.yyyy hh24:mi'),3, '6,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('18.03.2015 17.59','dd.mm.yyyy hh24:mi'),2, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('18.03.2015 16.54','dd.mm.yyyy hh24:mi'),1, '1,2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('18.03.2015 15.33','dd.mm.yyyy hh24:mi'),1, '1,4,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('18.03.2015 19.33','dd.mm.yyyy hh24:mi'),2, '6,5,2');
insert into opros (date_opros, nom_operator, channels) values (to_date('18.03.2015 18.44','dd.mm.yyyy hh24:mi'),1, '1,6,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('19.03.2015 20.30','dd.mm.yyyy hh24:mi'),2, '1,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('19.03.2015 18.10','dd.mm.yyyy hh24:mi'),2, '2');
insert into opros (date_opros, nom_operator, channels) values (to_date('19.03.2015 15.33','dd.mm.yyyy hh24:mi'),2, '1');
insert into opros (date_opros, nom_operator, channels) values (to_date('19.03.2015 19.33','dd.mm.yyyy hh24:mi'),1, '1,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('19.03.2015 18.44','dd.mm.yyyy hh24:mi'),2, '1,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('19.03.2015 16.42','dd.mm.yyyy hh24:mi'),3, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('19.03.2015 15.44','dd.mm.yyyy hh24:mi'),3, '1,2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('19.03.2015 17.59','dd.mm.yyyy hh24:mi'),4, '1,4,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('19.03.2015 16.54','dd.mm.yyyy hh24:mi'),5, '6,5,2');
insert into opros (date_opros, nom_operator, channels) values (to_date('19.03.2015 15.33','dd.mm.yyyy hh24:mi'),4, '1,6,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('20.03.2015 19.33','dd.mm.yyyy hh24:mi'),3, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('20.03.2015 18.44','dd.mm.yyyy hh24:mi'),2, '2,6');
insert into opros (date_opros, nom_operator, channels) values (to_date('20.03.2015 20.30','dd.mm.yyyy hh24:mi'),1, '4,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('20.03.2015 18.10','dd.mm.yyyy hh24:mi'),2, '6,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('20.03.2015 15.33','dd.mm.yyyy hh24:mi'),1, '1,2,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('20.03.2015 19.33','dd.mm.yyyy hh24:mi'),2, '5,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('21.03.2015 18.44','dd.mm.yyyy hh24:mi'),2, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('21.03.2015 16.42','dd.mm.yyyy hh24:mi'),2, '2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('21.03.2015 15.44','dd.mm.yyyy hh24:mi'),1, '2,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('21.03.2015 17.59','dd.mm.yyyy hh24:mi'),2, '1,6');
insert into opros (date_opros, nom_operator, channels) values (to_date('21.03.2015 16.54','dd.mm.yyyy hh24:mi'),3, '1,3,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('22.03.2015 15.33','dd.mm.yyyy hh24:mi'),3, '5,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('22.03.2015 19.33','dd.mm.yyyy hh24:mi'),4, '7,3,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('22.03.2015 18.44','dd.mm.yyyy hh24:mi'),5, '3,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('22.03.2015 20.30','dd.mm.yyyy hh24:mi'),4, '3,2');
insert into opros (date_opros, nom_operator, channels) values (to_date('22.03.2015 18.10','dd.mm.yyyy hh24:mi'),3, '2,5,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('22.03.2015 15.33','dd.mm.yyyy hh24:mi'),2, '5,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('23.03.2015 19.33','dd.mm.yyyy hh24:mi'),1, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('23.03.2015 18.44','dd.mm.yyyy hh24:mi'),3, '2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('23.03.2015 16.42','dd.mm.yyyy hh24:mi'),2, '2,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('23.03.2015 15.44','dd.mm.yyyy hh24:mi'),1, '1,6');
insert into opros (date_opros, nom_operator, channels) values (to_date('23.03.2015 17.59','dd.mm.yyyy hh24:mi'),1, '1,3,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('23.03.2015 16.54','dd.mm.yyyy hh24:mi'),3, '5,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('23.03.2015 15.33','dd.mm.yyyy hh24:mi'),2, '7,3,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('24.03.2015 19.33','dd.mm.yyyy hh24:mi'),4, '3,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('25.03.2015 18.44','dd.mm.yyyy hh24:mi'),5, '3,2');
insert into opros (date_opros, nom_operator, channels) values (to_date('26.03.2015 20.30','dd.mm.yyyy hh24:mi'),4, '2,5,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('27.03.2015 18.10','dd.mm.yyyy hh24:mi'),3, '1');
insert into opros (date_opros, nom_operator, channels) values (to_date('27.03.2015 15.33','dd.mm.yyyy hh24:mi'),2, '1,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('27.03.2015 19.33','dd.mm.yyyy hh24:mi'),1, '1,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('27.03.2015 18.44','dd.mm.yyyy hh24:mi'),2, '1,4,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('28.03.2015 16.42','dd.mm.yyyy hh24:mi'),3, '2,3,6');
insert into opros (date_opros, nom_operator, channels) values (to_date('28.03.2015 15.44','dd.mm.yyyy hh24:mi'),4, '1,2,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('28.03.2015 17.59','dd.mm.yyyy hh24:mi'),4, '6,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('28.03.2015 16.54','dd.mm.yyyy hh24:mi'),3, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('28.03.2015 15.33','dd.mm.yyyy hh24:mi'),2, '1,2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('28.03.2015 19.33','dd.mm.yyyy hh24:mi'),3, '1,4,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('28.03.2015 18.44','dd.mm.yyyy hh24:mi'),2, '6,5,2');
insert into opros (date_opros, nom_operator, channels) values (to_date('28.03.2015 20.30','dd.mm.yyyy hh24:mi'),1, '1,6,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('28.03.2015 18.10','dd.mm.yyyy hh24:mi'),2, '1,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('29.03.2015 15.33','dd.mm.yyyy hh24:mi'),3, '2');
insert into opros (date_opros, nom_operator, channels) values (to_date('30.03.2015 19.33','dd.mm.yyyy hh24:mi'),4, '1');
insert into opros (date_opros, nom_operator, channels) values (to_date('30.03.2015 18.44','dd.mm.yyyy hh24:mi'),3, '1,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('30.03.2015 16.42','dd.mm.yyyy hh24:mi'),2, '1,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('30.03.2015 15.44','dd.mm.yyyy hh24:mi'),1, '7,3,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('30.03.2015 17.59','dd.mm.yyyy hh24:mi'),1, '1,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('31.03.2015 16.54','dd.mm.yyyy hh24:mi'),1, '5,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('01.04.2015 15.33','dd.mm.yyyy hh24:mi'),2, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('01.04.2015 19.33','dd.mm.yyyy hh24:mi'),3, '2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('01.04.2015 20.00','dd.mm.yyyy hh24:mi'),5, '2,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('01.04.2015 17.10','dd.mm.yyyy hh24:mi'),3, '1,6');
insert into opros (date_opros, nom_operator, channels) values (to_date('01.04.2015 15.33','dd.mm.yyyy hh24:mi'),4, '1,3,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('01.04.2015 18.44','dd.mm.yyyy hh24:mi'),2, '7,3,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('03.04.2015 20.30','dd.mm.yyyy hh24:mi'),1, '3,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('04.04.2015 18.10','dd.mm.yyyy hh24:mi'),5, '3,2');
insert into opros (date_opros, nom_operator, channels) values (to_date('06.04.2015 15.33','dd.mm.yyyy hh24:mi'),4, '2,5,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('09.04.2015 18.44','dd.mm.yyyy hh24:mi'),4, '2,3,6');
insert into opros (date_opros, nom_operator, channels) values (to_date('10.04.2015 16.42','dd.mm.yyyy hh24:mi'),4, '1,2,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('12.04.2015 15.44','dd.mm.yyyy hh24:mi'),3, '6,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('13.04.2015 17.59','dd.mm.yyyy hh24:mi'),3, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('15.04.2015 19.00','dd.mm.yyyy hh24:mi'),2, '1,2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('16.04.2015 21.18','dd.mm.yyyy hh24:mi'),1, '1,4,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('16.04.2015 20.01','dd.mm.yyyy hh24:mi'),1, '6,5,2');
insert into opros (date_opros, nom_operator, channels) values (to_date('16.04.2015 20.02','dd.mm.yyyy hh24:mi'),2, '1,6,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('17.04.2015 20.55','dd.mm.yyyy hh24:mi'),3, '1,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('17.04.2015 16.54','dd.mm.yyyy hh24:mi'),4, '2');
insert into opros (date_opros, nom_operator, channels) values (to_date('19.04.2015 15.33','dd.mm.yyyy hh24:mi'),3, '1');
insert into opros (date_opros, nom_operator, channels) values (to_date('20.04.2015 19.33','dd.mm.yyyy hh24:mi'),3, '1,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('22.04.2015 18.44','dd.mm.yyyy hh24:mi'),4, '1,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('23.04.2015 20.30','dd.mm.yyyy hh24:mi'),5, '7,3,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('25.04.2015 18.10','dd.mm.yyyy hh24:mi'),4, '1,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('26.04.2015 15.33','dd.mm.yyyy hh24:mi'),3, '5,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('28.04.2015 19.33','dd.mm.yyyy hh24:mi'),5, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('29.04.2015 18.44','dd.mm.yyyy hh24:mi'),1, '2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('01.05.2015 16.42','dd.mm.yyyy hh24:mi'),1, '2,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('02.05.2015 15.44','dd.mm.yyyy hh24:mi'),1, '1,6');
insert into opros (date_opros, nom_operator, channels) values (to_date('04.05.2015 17.59','dd.mm.yyyy hh24:mi'),2, '1,3,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('05.05.2015 20.00','dd.mm.yyyy hh24:mi'),3, '5,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('07.05.2015 17.10','dd.mm.yyyy hh24:mi'),4, '7,3,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('08.05.2015 15.33','dd.mm.yyyy hh24:mi'),5, '3,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('10.05.2015 19.33','dd.mm.yyyy hh24:mi'),3, '3,2');
insert into opros (date_opros, nom_operator, channels) values (to_date('11.05.2015 18.44','dd.mm.yyyy hh24:mi'),4, '2,5,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('13.05.2015 20.30','dd.mm.yyyy hh24:mi'),4, '1');
insert into opros (date_opros, nom_operator, channels) values (to_date('14.05.2015 18.10','dd.mm.yyyy hh24:mi'),5, '1,3');
insert into opros (date_opros, nom_operator, channels) values (to_date('16.05.2015 15.33','dd.mm.yyyy hh24:mi'),4, '1,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('17.05.2015 19.33','dd.mm.yyyy hh24:mi'),3, '7,3,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('19.05.2015 18.44','dd.mm.yyyy hh24:mi'),2, '1,4');
insert into opros (date_opros, nom_operator, channels) values (to_date('20.05.2015 16.42','dd.mm.yyyy hh24:mi'),1, '5,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('25.05.2015 15.44','dd.mm.yyyy hh24:mi'),1, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('25.05.2015 17.59','dd.mm.yyyy hh24:mi'),2, '2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('27.05.2015 19.00','dd.mm.yyyy hh24:mi'),1, '5,2,7');
insert into opros (date_opros, nom_operator, channels) values (to_date('28.05.2015 21.18','dd.mm.yyyy hh24:mi'),2, '4,1');
insert into opros (date_opros, nom_operator, channels) values (to_date('30.05.2015 20.01','dd.mm.yyyy hh24:mi'),2, '2,5');
insert into opros (date_opros, nom_operator, channels) values (to_date('31.05.2015 20.02','dd.mm.yyyy hh24:mi'),2, '2,4');

commit;

select * from channel;
select * from opros;
select min(date_opros) from opros; 
select max(date_opros) from opros; 
-- ������: ������� ���������� ������� �� ������� � ������� ������� ������
-- ��������� ������� ����� level 
select  to_date('01.'|| lpad(level, 2, '0') || '.2015', 'dd.mm.yyyy') date_d
    from dual
  connect by level <= 12

-- ������ �������� �� ������� 

select * 
      from (
    select  to_date('01.'|| lpad(level, 2, '0') || '.2015', 'dd.mm.yyyy') mm
        from dual
      connect by level <= 12 ) m
  -- ������� 1 
/* ���� � ������ ���� ����������� ������ ������, �.�. ����� �����join. 
������  <last_day(m.mm)+1  ����� ��������� ���� ������� ������.
������ ',' || o.channels || ',' ����� like �������� id ������ � ����������� �������.
������  ����� like: '%,' || c.id_channel || ',%' ���� � ������ ������ ������ id-��� ������� � �������� id ������.
������ to_char(m.mm, 'Month yyyy') nm_month  ������ January   2015.
������  group by m.mm, c.name_channel, c.id_channel ���������� ������, ������ ������ ������
*/


select to_char(m.mm, 'Month yyyy') nm_month,
       c.name_channel,
       count (o.channels) cnt
      from (
    select  to_date('01.'|| lpad(level, 2, '0') || '.2015', 'dd.mm.yyyy') mm
        from dual
      connect by level <= 12 ) m
      cross join channel c
      left join opros o on o.date_opros < last_day(m.mm)+1 
                       and ',' || o.channels || ',' like '%,' || c.id_channel || ',%'
      group by m.mm, c.name_channel, c.id_channel
      order by m.mm, c.id_channel ;
      
      
-- ������� ������� 2 


-- ��������� ������, �. ����������� � ������. � �����, ������ ���� ������ ����������� ������� ���, 
-- ������� ���������� id ����������� � �� ������ ������ � ������ ������ '������'

select * 
       from opros o
       join channel c on ',' || o.channels || ',' like '%,' || c.id_channel || ',%'
    ;

-- ������ ���� ������������� �� �������� ���������� ����� � ���. 
-- ������ trunc(o.date_opros, 'mm') mm ����������� � ������ ���, ����� ������ ������

select trunc(o.date_opros, 'mm') mm,
       c.name_channel,
       c.id_channel,
       count(*) cnt 
       from opros o
       join channel c on ',' || o.channels || ',' like '%,' || c.id_channel || ',%'
       group by trunc(o.date_opros, 'mm'),c.name_channel, c.Id_Channel
       order by 1,  3
    ;


/* �����  
������ partition by t.name_channel order by mm ����� �� �������, 
��� ������ ������ �������� � ����������� �� �������.
������ rows between  unbounded preceding and current row ����� ������� 
����� ������� � ������ ���� �������

*/ 
-- 
select to_char(t.mm, 'Month yyyy') mm,
       t.name_channel,
       sum(cnt) over (partition by t.name_channel order by mm rows between
                unbounded preceding and current row ) cnt_all
       from (      

      select trunc(o.date_opros, 'mm') mm,
             c.name_channel,
             c.id_channel,
             count(*) cnt 
             from opros o
             join channel c on ',' || o.channels || ',' like '%,' || c.id_channel || ',%'
             group by trunc(o.date_opros, 'mm'),c.name_channel, c.Id_Channel
             order by 1,  3 ) t
      order by t.mm, t.id_channel
    ;


-- ������: �� ������ ������ ������ ������� ���������� � ������� � ������������ ���������� ���������� ������� 
-- ���������� � ����. ��������� ������� �� nom_operator


select * from channel;
select * from opros;

-- trunc(o.date_opros) dd  ������ ���� ������� ��� ������� (�������)
-- count (1)   ���������� ������� �� ������� ��������� �� ������ ���� 
-- trunc(t.dd, 'mm') ������� ���� � ���������� ������ ����� 
-- to_char(trunc(t.dd, 'mm'), 'Month yyyy') mm ���������� � ��������� ���� (����� � ���)
-- round (avg(cnt), 2)  avg_opros ���������� ����������� �������� �� ���� ������ ����� �������


select to_char(trunc(t.dd, 'mm'), 'Month yyyy') mm,
       round (avg(cnt), 2)  avg_opros,
       max (cnt) max_opros       
   from (
      select     o.nom_operator,
                 trunc(o.date_opros) dd,
                 count (1)  cnt
           from opros o 
           group by o.nom_operator, trunc(o.date_opros)
                   ) t
        group by trunc (t.dd, 'mm') ; 






------------------------ 


 

select distinct to_char(ord.date_d,'mm.yyyy'),
       pr.name_v,
       sum(ord.quantity_n) over (partition by to_char(ord.date_d,'mm.yyyy'),pr.name_v) as quantity  
       from tb_product pr
       join tb_order ord on pr.id=ord.product_id;



with moth_result as
( select res.o_date, max(res.max_quantity) as c_id     
  from (select st.first_name_v,
               to_char(ord.date_d,'yyyy.mm') as o_date,
               cl.id,
               count(ord.quantity_n) over (partition by to_char(ord.date_d,'mm.yyyy'),cl.id) as max_quantity 
               from tb_order ord
               join tb_client cl on ord.client_id=cl.id
               join tb_stuff st on cl.stuff_id=st.id
               )res
               group by res.o_date
               )
   
   
   select res.*,
          res_2.*,
          st.first_name_v
    from moth_result res
   join (
        select to_char(ord.date_d,'yyyy.mm') as o_date,
               cl.id as c_id,
               cl.stuff_id,
               count(ord.quantity_n) 
               from tb_order ord
               join tb_client cl on ord.client_id=cl.id
               group by to_char(ord.date_d,'yyyy.mm'),cl.id,cl.stuff_id) res_2 on res_2.o_date = res.o_date and
                        res_2.c_id = res.c_id
               join tb_stuff st on st.id=res_2.stuff_id







with t1 as (
select distinct res.mmyy,
        max(res.kol_zakazov) over (partition by res.mmyy) as maxzakaz
from
(select distinct to_char(o.date_d,'yyyy-mm') as mmyy,
    o.client_id as client,
    count(o.id) 
          over  (partition by o.client_id,to_char(o.date_d,'yyyy-mm')) as kol_zakazov
    from tb_order o 
    
   ) res
  )
    
select distinct t1.mmyy,
       t2.stuffname,
       t2.zakazy
       from t1 
       join (
       select to_char(ord.date_d,'yyyy-mm') as dat2,
       st.first_name_v as stuffname,
       count(ord.id)  over  (partition by to_char(ord.date_d,'yyyy-mm'),ord.client_id) as zakazy
       from tb_order ord
       join tb_client cl on ord.client_id=cl.id
       left join tb_stuff st on st.id=cl.stuff_id ) t2 on t2.dat2= t1.mmyy and  t1.maxzakaz=t2.zakazy



 





-- ������� ��������� ��� �������� ���� 
select * from  tb_client;

select * from  tb_product;

select * from tb_order_type;

select * from  tb_order;

select * from  tb_stuff;
-- id ���� 4 � ������� position_id

select * from  tb_position;

select * from  tb_country;

select * from  tb_gender;

select * from  tb_nationality;

select * from tb_address;

select * from  tb_address_type;



-- ������� ��������� ���  ���� projects
select * from  tb_project_employees;

select * from  tb_projects_for_empl;

select * from tb_order_type;

select * from  tb_department_empl;

select * from  tb_empl_project_associate;
