--------------------------------------------------------------------------
--                      ������� ������ 


--------------------------------------------------------------------------
/* VIEW ������������ ����� ����������� �������, ������� ��������� ��
����������. ��� ��������� � ������� �������, ������������ ���� ���
��������� ������. */

/*
������������� ������ ���������� ������ � ������ ������� select, �� � ��� ����� ������ ���������� ����� �� ��������
*/

create or replace view stuff_and_client_for_all as
select st.first_name_v as st_name,
st.position_id as st_position,
cl.first_name_v as cl_name,
cl.id as cl_id 
from tb_stuff st
join tb_client cl on cl.stuff_id = st.id


select * from stuff_and_client_for_all ;

select * 
       from stuff_and_client_for_all cl_new
       join tb_address ad on ad.client_id = cl_new.cl_id 
       
       

---------------------------------
/* 
������������� ����� ������� ��� �������������� ������� ����� �������� ����� FORCE.
��� ����� ������ � �������, ����� ��� ���������� � ��� ��� ��� �������� ����, � API, � ���� 
�������� ����� ������ �� ��������� ������, �����
*/


create or replace FORCE view stuff_and_client_for_all as
select st.first_name_v as st_name,
st.position_id as st_position,
cl.first_name_v as cl_name,
cl.id as cl_id 
from tb_stuff st
join tb_client cl on cl.stuff_id = st.id


---------------------------------
/* INSERT -       commit ������������ ����� ������� insert 
   INSERT ALL -   commit ����� ���� �������, � ����� 100 ������� ����� ���� �� ���������,
                  ��-�� ��������� 101  ������ */

/*  insert all
������: ��� ������ ���������� � id = 10 �������� �������,
����������� ����� � �������������� ���������� ��
���� �� (������� � ��������� ������). 

����� � ���, ��� ��������� ���� ����� �� ���������. � ����� �������� id ������� � ������� �������. 
*/

insert all

into tb_client values (102, '�����', '������', null,
to_date('31.01.2000','dd.mm.yyyy'))

into tb_address values (1, 1, 102, '������', '������', 1, 2)

into tb_additional_informational values (1001, 102,
'+999111222333', '1211@gmail.com', null)

select * from dual;

/* ������ ����������  INSERT SELECT  (�  ������ ���� ������ ��� ���� � ������ �������)

������:
���������� ��������, ����� ��� ���������� �������
����� �������, ���������� �������� � �������� � �� ����
������������ �������� (�������� ������������ ��
�������������� ��������� ��������� ������ ������� �������
�������� � ���������� ���� ��� ��� ��� ���������� �������).

*/

-- �������: ������� ����� ������� � ������� ����� ��������� ����: id, ��� ������, ���� ��������.
create table tb_client_new (
id number,
cl_name_v varchar2(200),
dirth_date_d date
);

-- ������ ������� ���� ������� �� ������ ������� ��������:

insert into tb_client_new
       select cl.id, cl.second_name_v || ' ' || cl.first_name_v || ' ' || cl.surname_v,
       cl.birth_date_d from tb_client cl ; 
       
/* 
������: 
��� ������� ���� ������� � ����� ������� ��������
��������� ����, ��� ������� ���������� ������ ����� �������.
���������� ����� �������� �� ������� �������� ��� ���������
������.
*/

insert into tb_client_new
       select cl.id, cl.second_name_v || ' ' || cl.first_name_v || ' '
              || cl.surname_v, cl.birth_date_d
 from tb_client cl
      where not exists (select * from tb_client_new cl1 where cl.id =
              cl1.id)
-- ��� 
      where cl.id not in (
            select cl.id from tb_client_new cl) 
      



SELECT ... FOR UPDATE -- ��������� ��� ���� ������ �� ����������. 
                      -- ����� ������� �� ������������� ���������� ������� ��� ���� �������� DML
                      
-- �����: ��� ������� ��������  FOR UPDATE � �� ���� ��� �� DML,
-- �� ����� ������� ����� �����, ���� � ��������� ������� ��������   FOR UPDATE
select * from tb_client for update 



                    
SELECT ... FOR UPDATE  [NOWAIT]  --  ���� ��� �� ����� �����, �� ��������, � ����������� � �������, ������ 
                                 -- ��� ������� ������ ��� ����� �����������  

-- �����: ����� ��� ����� ��� ��������� ����������/������, ��� ����� ��������� ����� ����� ��������� ���. 
select * from tb_client for update NOWAIT; 




/*
��������� �������� ������� ���� ������� �����������. � ��� �������� FK ����� ������� on delete CASCADE;
� ����� ������, ��� �������� �������� �������� � ������, ��� ������� � FK on delete CASCADE.
������ ��� ��� �������� ��������, �������� ��� ����� ��.
--------------
� �����: ���� � ������� ��������� FK, �� ���� �� ������ ���������  on delete CASCADE, ����� �� ������� ������. 
*/
 

alter table tb_address
add constraint FK_TB_CLIENT_TB_STUFF
foreign key (client_id)
references tb_client (STUFF_ID)
on delete CASCADE;

 


--------------------------------------------------------------------------
--                      ��������  ������ 


--------------------------------------------------------------------------

 

