--------------------------------------------------------------------------
--                      ������� ������ 


--------------------------------------------------------------------------
������� �������� ���������� � ���������������� �������.
�������� ������, ������� ������ ������������ ����� �������������� ������� ���


with t as (
select to_date ('01.01.2009', 'dd.mm.yyyy') beg_date,
       to_date ('10.01.2009', 'dd.mm.yyyy') end_date
 from dual
union all
select to_date ('03.01.2009', 'dd.mm.yyyy') beg_date,
       to_date ('05.01.2009', 'dd.mm.yyyy') end_date
 from dual
union all
select to_date ('10.01.2009', 'dd.mm.yyyy') beg_date,
       to_date ('12.01.2009', 'dd.mm.yyyy') end_date
 from dual
union all
select to_date ('13.01.2009', 'dd.mm.yyyy') beg_date,
       to_date ('20.01.2009', 'dd.mm.yyyy') end_date
 from dual
union all
select to_date ('01.02.2009', 'dd.mm.yyyy') beg_date,
       to_date ('10.02.2009', 'dd.mm.yyyy') end_date
 from dual
)
--
select min (beg_date) as beg_date,
       max (end_date) as end_date
from (
      select beg_date,
             end_date,
             sum(strt_grp) over (order by beg_date, end_date ) grp_num
      from (
             select  beg_date,
                     end_date,
                     case when beg_date > 1 + max(end_date)
                        over (order by beg_date, end_date rows between unbounded preceding and 1 preceding) then 1 end strt_grp
             from t
           )
     )
group by grp_num
order by beg_date;


  
--------------------------------------------------------------------------
-- � � � � � � � �       � � � � � � 



--------------------------------------------------------------------------


������� �������� ���������� � ���������������� �������.
�������� ������, ������� ������ ������������ ����� ������������� �����������
��� ������� ������������ � ����������� �����,
����� ��� ���������.

with log as
 (select 'U1' username,
         to_date('08.08.2022', 'dd.mm.yyyy') + 1 / 24 logon_time,
         to_date('08.08.2022', 'dd.mm.yyyy') + 10 / 24 logoff_time
    from dual
  union all
  select 'U1' username,
         to_date('08.08.2022', 'dd.mm.yyyy') + 6 / 24 logon_time,
         to_date('08.08.2022', 'dd.mm.yyyy') + 14 / 24 logoff_time
    from dual
  union all
  select 'U1' username,
         to_date('08.08.2022', 'dd.mm.yyyy') + 4 / 24 logon_time,
         to_date('08.08.2022', 'dd.mm.yyyy') + 12 / 24 logoff_time
    from dual
  union all
  select 'U1' username,
         to_date('08.08.2022', 'dd.mm.yyyy') + 8 / 24 logon_time,
         to_date('08.08.2022', 'dd.mm.yyyy') + 17 / 24 logoff_time
    from dual
  union all
  select 'U1' username,
         to_date('08.08.2022', 'dd.mm.yyyy') + 16 / 24 logon_time,
         to_date('08.08.2022', 'dd.mm.yyyy') + 18 / 24 logoff_time
    from dual
  union all
  select 'U1' username,
         to_date('08.08.2022', 'dd.mm.yyyy') + 9 / 24 logon_time,
         to_date('08.08.2022', 'dd.mm.yyyy') + 16 / 24 logoff_time
    from dual
  union all
  select 'U2' username,
         to_date('08.08.2022', 'dd.mm.yyyy') + 1 / 24 logon_time,
         to_date('08.08.2022', 'dd.mm.yyyy') + 3 / 24 logoff_time
    from dual
  union all
  select 'U2' username,
         to_date('08.08.2022', 'dd.mm.yyyy') + 2 / 24 logon_time,
         to_date('08.08.2022', 'dd.mm.yyyy') + 12 / 24 logoff_time
    from dual
  union all
  select 'U2' username,
         to_date('08.08.2022', 'dd.mm.yyyy') + 11 / 24 logon_time,
         to_date('08.08.2022', 'dd.mm.yyyy') + 13 / 24 logoff_time
    from dual
  union all
  select 'U2' username,
         to_date('08.08.2022', 'dd.mm.yyyy') + 10 / 24 logon_time,
         to_date('08.08.2022', 'dd.mm.yyyy') + 14 / 24 logoff_time
    from dual), -- ��� ����� ������� � ���� ��� ������� 1 

-- ������� 1 
                    
/* ������� ����� tb log1, � ������� ������� ������ ������� res. � res
��� logon_time ������ 1, ��� logoff_time ������ -1. ��� ��� � as time_t ������
��� ��������  */

log1 as (
select l1.username, l1.logon_time as time_t, 1 as res from log l1 
union all
select l1.username, l1.logoff_time as time_t, -1 as res from log l1
),   

/* ����� ������� ����� tb log2, � ������� �� log1 ������� as l  � ��� l ������� ���, ���� ������� sum. 
� ������� sum  �������� �������� �� ����� �����, � ������� ���������� �� �������.
���� ����� ������� log_count */

log2 as (
select l.*,
       sum(l.res) over (partition by l.username order by l.time_t, l.res ) as log_count
 from log1 l 
)

/* �����    ������� �� ����������� ������, ��� � U1/U2 ���������� max(log_count)t
 */

select log2.* 
       from log2
       where log2.username = 'U1'
       and log2.log_count = (
                             select max(log2.log_count)
                                    from log2
                                    where log2.username = 'U1'
                             )
union all
select log2.* 
       from log2
       where log2.username = 'U2'
       and log2.log_count = (
                             select max(log2.log_count)
                                    from log2
                                    where log2.username = 'U2'
                             )
      
       


-- ������� 2
select distinct first_value(n) over (partition by n order by c desc) username,
                first_value(in_) over (partition by n order by c desc) time_connect,
                first_value(c) over (partition by n order by c desc) num_connect
       from (select l.username n,
                    to_char(l.logon_time, ' hh24:mi') in_,
                    ((select count(*)
                            from log l1
                            where l1.username = l.username
                            and l1.logon_time <= l.logon_time) -
                    (select count(*)
                            from log l2
                            where l2.logoff_time <=  l.logon_time
                            and l2.username = l.username)) c
                    from log l)

 
                     
                    
 




















