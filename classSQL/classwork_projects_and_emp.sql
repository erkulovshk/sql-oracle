--�������� ������� �����������

create table tb_project_employees
(
  id         number not null,
  name_v     varchar2(100) not null,
  salary     number,
  position_v varchar2(20),
  state_v    varchar2(20),
  upper_id   number,
  dep_id     number
);
 
  
--�������� ������� ��������
create table tb_projects_for_empl (
       id number,
       name varchar2(100)
);

--�������� ������� �������������
create table tb_department_empl (
       id number,
       name varchar2(100)
);
--�������� ������� ������������ ���������� � ������� (����� ��������� ����� �������� ����������)
create table tb_empl_project_associate (
       id number,
       empl_id number,
       project_id number
);

insert into tb_project_employees values (13,'Win', 11111,'clerc', '', 10, null);
insert into tb_project_employees values (2,'Ulan', 7200.123,'manager', 'working', 4, 2);
insert into tb_project_employees values (3,'Ivan', 51000.5654,'clerc', 'onHoliday', 10, 2);
insert into tb_project_employees values (4,'Kojomkul', 68000.7789,'boss', 'working', null, 2);
insert into tb_project_employees values (14,'Pool', 22222,'clerc', '', 10, null);
insert into tb_project_employees values (8,'Sultan', 12343,'clerc', 'working', 2, 3);
insert into tb_project_employees values (9,'Risbek', 54221,'clerc', 'onHoliday', 7, 1);
insert into tb_project_employees values (10,'Nursultan', 12555,'manager', 'working', 4, 3);
insert into tb_project_employees values (11,'Alex', 5533,'clerc', 'working', 10, 1);
insert into tb_project_employees values (12,'Tom', 5213,'clerc', 'fired', 10, 1);

 


insert into tb_projects_for_empl values (1, 'Increasing the number of sales');
insert into tb_projects_for_empl values (2, 'Attraction of clients');
insert into tb_projects_for_empl values (3, 'Expansion of the list of goods');
insert into tb_projects_for_empl values (4, 'Improving the quality of service');



insert into tb_department_empl values (1, 'Retail business department');
insert into tb_department_empl values (2, 'Sales promotion department');
insert into tb_department_empl values (3, 'Customer service department');




insert into tb_empl_project_associate values (1, 2, 1);
insert into tb_empl_project_associate values (2, 2, 2);
insert into tb_empl_project_associate values (3, 3, 3);
insert into tb_empl_project_associate values (4, 4, 2);
insert into tb_empl_project_associate values (5, 8, 1);
insert into tb_empl_project_associate values (6, 9, 3);
insert into tb_empl_project_associate values (7, 5, 2);
insert into tb_empl_project_associate values (8, 10, 1);

 


select count (*) as count_emp
       from tb_project_employees 

-- ������� ���-�� �����������, ������ ���� -  ������� ����� ���-�� �������

-- ��� ��� count ��� �������� ���, �� � ���� ��� ������ ����������� � ��� 
-- ��������� ���������� (��������, ������) � �������� ����� as � ������ ��� ���� 


select count (id) as �������_id -- ���� ����, �� ������� ������� ��������� ���-�� ������� � ������� id
       from tb_project_employees 



select count (1) as ������_������� -- ���� ���� '1', �� ������� �������� ���, ��� ������ �������, �.�. � �����
       from tb_project_employees;  -- ������, ������� ����� ����� �� ���-�� ������� � ������� 'id'


select count (upper_id) as ���_��_������_�������  -- ����� ����� 12, ������ ��� � upper_id � ���� ���� ������
       from tb_project_employees;   -- ����� null, ������� ���� ���� ������������ � ���� ��������

select -- (*) as count_emp
       sum(salary)    -- ����� �� ����������  sum, ����� �������� ����� ����� �� ���� ����������� 
      from tb_project_employees;

select -- (*) as count_emp
       e.name_v,
       e.salary
       sum(e.salary)     
      from tb_project_employees e;
-- ��� ���� �� ������, ������� �� ��������, � ������ ���������� ��������� ����� ����� 

select -- (*) as count_emp
       e.name_v,
       e.salary,
       sum(e.salary),
       count( e.name_v)
      from tb_project_employees e
    group by   e.name_v, e.salary;
-- ����� ��������� ������ ����, ���� ������������ group by 
-- �� ����� �� �������� �������� ������� ��� �� ����� ��
-- Win ����������, ��� ��� � �� ���� 4�����  ��� � ����������� ��, 
-- ������� count � ���� ����� 4  

-- ��������� �������� ����� �� ��� ������ �������/��������� � ���-�� �����������

select e.position_v,
       sum(e.salary),
       count(e.name_v)
      from tb_project_employees e
           -- join  
    where  e.position_v != '�lerc'
    group by  e.position_v;
--   ��� ��� �� ��������� �� ������ ������� 


select e.position_v,
       sum(e.salary),
       count(e.name_v),
       max(e.salary),
       min(e.salary),
       avg(e.salary)
      from tb_project_employees e
           -- join  
    where  e.position_v != '�lerc'
    group by  e.position_v;
-- ������� ������� � ������ �� �� ������ �� ���, 
-- ���� ���-�� ����������� ������ ��� 
-- max min avg ������ ������� ������ 
-- � ��� ��� ���������� �� �������/��������� 




select e.position_v from tb_project_employees e

-- ���� �������� ��� ��������� ����� �����������, �� ��� ���� ����������� � ���������,
-- �� ��� �� ������. � ����� ����� ������������ ����������� 




-- ��� ���� ��������� ����������� � ����������� ������ ���� ��� ��, ��� ����� 
-- � ���� ����������� ��������� ���. � ����� ������ ��� �������/��������� 
select e.position_v 
    from tb_project_employees e
group by e.position_v;

-- �� ��� ����, ����� ����� ����� distinct ��������. �������� ���� �����.  
-- ������� ���������� - ���� ������� ��� distinct
select distinct e.position_v 
    from tb_project_employees e;

-- ���������� �������� ������ order by 
-- �� ������� � ����� �����
select e.position_v,
       sum(e.salary),
       count(e.name_v),
       max(e.salary),
       min(e.salary),
       avg(e.salary)
      from tb_project_employees e
    where  e.position_v != '�lerc'
    group by  e.position_v
    order by e.position_v desc; -- order by.... des� ��������� �� �������� � �������� 
    -- ���� �� ����� � ������, ����� � ����� �������� � �.�. 

select e.position_v,
       sum(e.salary),
       count(e.name_v),
       max(e.salary),
       min(e.salary),
       avg(e.salary)
      from tb_project_employees e
    where  e.position_v != '�lerc'
    group by  e.position_v
    order by e.position_v  ; -- order by.... ��� des� ��������� �� �������� � ��������
    

-- ���������� �� ���������� ��������, � ������� ���� ���� ��������� 
-- 100 ��� ��������, �� �� ����� ��� ��������� ������� ���� �� ������, 
-- � ��� ���� ������ ���� ������� ������� ����������� ���� 

select *
   from tb_project_employees e
order by e.id, e.position_v desc; 
/*  ID �� �������� � ��������  �����������, � ������� �� ����� �������������, 
��� ��� � ��� ��� ������������� ID,
������� ����� �� ������� ������������ �������/����� ������ ��� ���������� �� 
�������� */
 








/*
������� �� ������������ ����� ����� (������� �� �����������, ����� ������ �� �����): 
1. ������� tb_project_employees. �������� ������ ���������� � �� ��������� � �������:
   Bekjan(clerk)
   ������  � ������� ������������ || � ����������� as (��), ����� ������� �������
*/

select e.name_v  || ' (' || e.position_v || ' )' as name_plus_positione
       from tb_project_employees e;

/*
2. ������� tb_project_employees. �������� ���� �����������, ������� ������ �� �����������.
� ������� join � ���.
*/

select e.name_v,
       e.position_v,
       e.state_v
       from tb_project_employees e
      where e.upper_id is null ; -- c ������� IS NULL
      
select e2.name_v,
       e2.position_v,
       e1.name_v
       from tb_project_employees e1
       right join tb_project_employees e2 on e1.id = e2.upper_id
       where e1.name_v is null;     
/*
� ����� ���� � ����� �������� NULL ����� � upper_id, � � ������ (Risbek) id ����� �� ������, �.�.
� ���� ��������� upper_id ����������, �������� ���� � �������, �������� ��� ���������� �������, 
� �������� upper_id � Risbeka ������. 
� ����� ������� ���� �������� ����� join, ����� ��� �������, ��� ����� � ��� id ���������� ��
��������������� ����������. 
*/

/*
��������� ���� ����������� � ���������� manager, � ������ Sales promotion department
 � ����������� � ������� Attraction of clients
*/

select e.name_v,
       e.position_v
  from tb_project_employees e
  join tb_empl_project_associate ass
 where e.dep_id != 2 and ass.id ;

-- Sales promotion department  2 
-- tb_projects_for_empl 2 

select rowid, e.* from tb_project_employees e
-- rowid ��� ������� �������, ����� ������� �� �����
-- ������ ������ ������ � �������

/*
select  * from tb_project_employees;
select * from tb_projects_for_empl;
select * from tb_department_empl;
select * from tb_empl_project_associate;
  

DELETE FROM tb_project_employees
WHERE name_v = 'Win'
salary

update tb_client
set nationality_v = 'Kyrgyzstan'
where id = 3

ALTER TABLE tb_addres
  MODIFY house_v   varchar2 (50);

DELETE FROM tb_client
WHERE nationality_v = 'Russia'
AND id > 2;

DROP TABLE tb_client;
*/
