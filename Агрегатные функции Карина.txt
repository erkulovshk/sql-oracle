CREATE SEQUENCE stuff_seq;

CREATE TABLE stuff
( id NUMBER,
  fio VARCHAR2(300),
  position_v VARCHAR2(50),
  salary NUMBER,
  CONSTRAINT pk_stuff PRIMARY KEY(id)
);
INSERT INTO stuff VALUES(stuff_seq.nextval,'Aleksandr','manager',22000);
INSERT INTO stuff VALUES(stuff_seq.nextval,'Illarion','boss',50000);
INSERT INTO stuff VALUES(stuff_seq.nextval,'Marfa','clerk',13000);
INSERT INTO stuff VALUES(stuff_seq.nextval,'Sofia','manager',18000);
INSERT INTO stuff VALUES(stuff_seq.nextval,'Kristina','clerk',10000);
INSERT INTO stuff VALUES(stuff_seq.nextval,'Valeriya','manager',26000);
INSERT INTO stuff VALUES(stuff_seq.nextval,'Maksim','clerk',9000);
INSERT INTO stuff VALUES(stuff_seq.nextval,'Fill','clerk',7000);

ALTER TABLE tb_client ADD stuff_id NUMBER;

UPDATE tb_client SET stuff_id=1
WHERE tb_client.id=1;
UPDATE tb_client SET stuff_id=1
WHERE tb_client.id=2;
UPDATE tb_client SET stuff_id=1
WHERE tb_client.id=3;
UPDATE tb_client SET stuff_id=3
WHERE tb_client.id=4;
UPDATE tb_client SET stuff_id=4
WHERE tb_client.id=5;
UPDATE tb_client SET stuff_id=5
WHERE tb_client.id=6;
UPDATE tb_client SET stuff_id=6
WHERE tb_client.id=7;
UPDATE tb_client SET stuff_id=7
WHERE tb_client.id=8;
UPDATE tb_client SET stuff_id=8
WHERE tb_client.id=9;
UPDATE tb_client SET stuff_id=8
WHERE tb_client.id=10;

ALTER TABLE tb_client ADD CONSTRAINT fk_id FOREIGN KEY(stuff_id)
REFERENCES stuff(id);

SELECT SUM(salary) FROM stuff;

SELECT SUM(salary) sum_salary_manager 
FROM stuff
WHERE position_v='manager';

SELECT COUNT(DISTINCT cl.stuff_id) stuff_with_cl
FROM tb_client cl;

SELECT AVG(salary) avg_salary_clerk
FROM stuff
WHERE position_v='clerk';

SELECT MAX(salary) max_salary_without_cl
FROM stuff
LEFT JOIN tb_client cl ON stuff.id=cl.stuff_id
WHERE stuff_id is NULL;

SELECT MIN(stuff.salary) min_salary_where_age_cl_more_40
FROM stuff
JOIN tb_client cl ON stuff.id=cl.stuff_id
WHERE (MONTHS_BETWEEN(sysdate,cl.birth_date)/12)>40;
